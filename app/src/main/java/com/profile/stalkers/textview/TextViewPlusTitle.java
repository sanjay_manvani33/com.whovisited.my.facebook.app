package com.profile.stalkers.textview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;

import com.profile.stalkers.R;

/**
 * Created by main on 5/19/2017.
 */

public class TextViewPlusTitle extends AppCompatTextView{
    private  static  final String TAG = "TextView";
    public TextViewPlusTitle(Context context) {
        super(context);
    }

    public TextViewPlusTitle(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context,attrs);
    }

    public TextViewPlusTitle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context,attrs);
    }
    private void setCustomFont(Context ctx,AttributeSet attrs){
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewPlus);
        String customFont = a.getString(R.styleable.TextViewPlus_customFont);
        setCustomFont(ctx,customFont);
        a.recycle();
    }
    public boolean setCustomFont(Context ctx,String assest){
        Typeface tf = null;
        try{
               tf = Typeface.createFromAsset(ctx.getAssets(),"KARLA-REGULAR.TTF");
        }catch(Exception e){
            Log.e(TAG,"Could not get typeface :"+e.getMessage());
            return false;
        }
        setTypeface(tf);
        return false;
    }
}

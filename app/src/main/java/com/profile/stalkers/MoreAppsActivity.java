package com.profile.stalkers;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.profile.stalkers.adapters.MoreAppsAdapter;
import com.profile.stalkers.model.MoreAppsDataModel;
import com.profile.stalkers.utils.AnalyticsApplication;
import com.profile.stalkers.utils.AsyncRequest;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.profile.stalkers.model.dataStrings.app_id;
import static com.profile.stalkers.model.dataStrings.app_name;
import static com.profile.stalkers.model.dataStrings.image_path;
import static com.profile.stalkers.model.dataStrings.name2;
import static com.profile.stalkers.model.dataStrings.play_url;
import static com.profile.stalkers.model.dataStrings.ratings;
import static com.profile.stalkers.model.dataStrings.something_wrong;

public class MoreAppsActivity extends AppCompatActivity implements  AsyncRequest.OnAsyncRequestComplete {

    String name = "MoreAppsActivityy";
    ListView mListView;
    AdView mAdView;
    AdRequest adRequest;
    MoreAppsAdapter moreAppsAdapter;
    ArrayList<MoreAppsDataModel> moreAppsDataModels;
    ImageView mImageView;
    TextView mTextViewMore;
    public native String moreurl();
    private Tracker mTracker;

    static {
        System.loadLibrary("keys");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_more_apps);

        getSupportActionBar().hide();

       // adRequest = new AdRequest.Builder().build();
        mTextViewMore= (TextView) findViewById(R.id.text_more);
        mAdView = (AdView) findViewById(R.id.adView_id);

        mListView = (ListView) findViewById(R.id.more_apps_list);
        mImageView= (ImageView) findViewById(R.id.main_menu);


        mTextViewMore.setText(mTextViewMore.getText().toString());
        mTextViewMore.setText(mTextViewMore.getText().toString());

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        //mAdView.loadAd(adRequest);
        Mint.initAndStartSession(this.getApplication(), "17510f4c");

        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        AsyncRequest getPosts =
                new AsyncRequest(MoreAppsActivity.this, AsyncRequest.POSTURLCONNECTION);
        getPosts.execute(moreurl());

    }

    @Override
    public void asyncResponse(String response, String label) {
        if(response!=null) {
            try {
                JSONObject jObject = new JSONObject(response);

                JSONArray mJsonArray = jObject.getJSONArray("apps");
                moreAppsDataModels = new ArrayList<>();

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                    MoreAppsDataModel modelname = new MoreAppsDataModel();

                    modelname.setApp_id(mJsonObject.getString(app_id));
                    modelname.setName(mJsonObject.getString(name2));
                    modelname.setPlay_url(mJsonObject.getString(play_url));
                    modelname.setImage_path(mJsonObject.getString(image_path));
                    modelname.setApp_name(mJsonObject.getString(app_name));
                    modelname.setRatings(mJsonObject.getString(ratings));

                    moreAppsDataModels.add(modelname);
                }

                if (moreAppsDataModels != null) {
                    if (moreAppsDataModels.size() > 0) {
                        moreAppsAdapter = new MoreAppsAdapter(MoreAppsActivity.this, moreAppsDataModels);
                        mListView.setAdapter(moreAppsAdapter);
                    } else {
                        Toast.makeText(getApplicationContext(), something_wrong, Toast.LENGTH_SHORT).show();
                    }
                }

            } catch (JSONException e) {

            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
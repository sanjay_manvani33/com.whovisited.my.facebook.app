package com.profile.stalkers;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.profile.stalkers.utils.AnalyticsApplication;
import com.profile.stalkers.utils.SharePreference;
import com.profile.stalkers.utils.SharePreferencePermanent;
import com.profile.stalkers.utils.TAGS;
import com.profile.stalkers.utils.TinyDB;
import com.profile.stalkers.utils.Utils;

import org.json.JSONException;

import static com.profile.stalkers.MainActivity.GRAPH_PATH;
import static com.profile.stalkers.MainActivity.SUCCESS;
import static com.profile.stalkers.model.dataStrings.Portuguese;
import static com.profile.stalkers.model.dataStrings.email_id;
import static com.profile.stalkers.model.dataStrings.english;
import static com.profile.stalkers.model.dataStrings.french;
import static com.profile.stalkers.model.dataStrings.german;
import static com.profile.stalkers.model.dataStrings.spanish;

public class MySettingActivity extends AppCompatActivity implements View.OnClickListener {

    RadioButton mImageViewFrench;
    RadioButton mImageViewSpanish;
    RadioButton mImageViewGermany;
    RadioButton mImageViewEnglish;
    RadioButton mImageViewPortuguese;
    TextView mTextViewApp,mTextViewUserName,mTextViewEmail_id;
    SharePreference mSharePreference;
    SharePreferencePermanent mSharePreferencePermanent;
    String languageSelectedLocal = "";
    Intent intent;
    AdView mAdView;
    AdRequest adRequest;
    ImageView mdialog_setting_radio_default_english1,mimageViewProfilePicture,mimg_logout;
    TextView mTextViewSet;
    ImageView mImageView;
    ToggleButton dialog_setting_notification;
    private Tracker mTracker;

    RelativeLayout mRelativeLayoutEnglish;
    RelativeLayout mRelativeLayoutSpanish;
    RelativeLayout mRelativeLayoutPortuguese;
    RelativeLayout mRelativeLayoutGerman;
    RelativeLayout mRelativeLayoutFrench;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mysetting);

        getSupportActionBar().hide();

        mTextViewSet= (TextView) findViewById(R.id.text_settings);
        mTextViewUserName = (TextView)findViewById(R.id.settings_user_name);
        mTextViewEmail_id = (TextView)findViewById(R.id.settings_user_emailid);
        mTextViewApp= (TextView) findViewById(R.id.text_app_name_ver);

        mRelativeLayoutEnglish = (RelativeLayout)findViewById(R.id.setting_relatvie_english);
        mRelativeLayoutSpanish = (RelativeLayout)findViewById(R.id.setting_relatvie_spanish);
        mRelativeLayoutFrench = (RelativeLayout)findViewById(R.id.setting_relatvie_french);
        mRelativeLayoutGerman = (RelativeLayout)findViewById(R.id.setting_relatvie_german);
        mRelativeLayoutPortuguese = (RelativeLayout)findViewById(R.id.setting_relatvie_portuguese);

        dialog_setting_notification = (ToggleButton)findViewById(R.id.dialog_setting_notification);

        mimg_logout = (ImageView)findViewById(R.id.img_logout);
        mImageView= (ImageView) findViewById(R.id.main_menu);
        mimageViewProfilePicture = (ImageView)findViewById(R.id.img_profile_picture);

        mSharePreference = new SharePreference(MySettingActivity.this);
        mSharePreferencePermanent = new SharePreferencePermanent(MySettingActivity.this);

        Glide.with(MySettingActivity.this)
                .load(mSharePreference.getImage())
                .placeholder(R.drawable.ic_blur)
                .into(mimageViewProfilePicture);

        mTextViewUserName.setText(mSharePreference.getmStringUserName());
        if(mSharePreference.getmStringUserEmail() == null){
            mTextViewEmail_id.setText(getString(R.string.not_available)+email_id);
            mTextViewEmail_id.setVisibility(View.GONE);
        }else{
            if(mSharePreference.getmStringUserEmail().trim().length()>0)
                mTextViewEmail_id.setText(mSharePreference.getmStringUserEmail());
            else
                mTextViewEmail_id.setVisibility(View.GONE);
        }
        mTextViewEmail_id.setText(mSharePreference.getmStringUserEmail());
        mTextViewSet.setText(mTextViewSet.getText().toString().toUpperCase());

        mTextViewApp.setText(getResources().getText(R.string.app_name)+" v"+getAppVersion());
        /*text_settings*/

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();



        String name = "MySettingActivityy";
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        mdialog_setting_radio_default_english1 = (ImageView)findViewById(R.id.dialog_setting_radio_default_english1);


        adRequest = new AdRequest.Builder().build();
       // mAdView = (AdView) findViewById(R.id.adView_id);
        //mAdView.loadAd(adRequest);


        mSharePreference = new SharePreference(getApplicationContext());
        mSharePreferencePermanent = new SharePreferencePermanent(getApplicationContext());

        dialog_setting_notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    dialog_setting_notification.setBackgroundResource(R.drawable.ic_on_notification);
                    mSharePreference.setNotificationEnabled(false);
                }else{
                    dialog_setting_notification.setBackgroundResource(R.drawable.ic_off_notification);
                    mSharePreference.setNotificationEnabled(true);

                }
            }
        });
        if (mSharePreference.isNotificationEnabled()){
            dialog_setting_notification.setBackgroundResource(R.drawable.ic_on_notification);
        }else{
            dialog_setting_notification.setBackgroundResource(R.drawable.ic_off_notification);
        }

        mdialog_setting_radio_default_english1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!languageSelectedLocal.equalsIgnoreCase(mSharePreferencePermanent.getAppLanguage()))
                {
                    mSharePreferencePermanent.setAppLanguage(languageSelectedLocal);
                    Utils.changeAppLanguage(getApplicationContext(), languageSelectedLocal);

                    intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
            }
        });
        mImageViewEnglish= (RadioButton) findViewById(R.id.dialog_setting_radio_default_english);
        mImageViewSpanish= (RadioButton) findViewById(R.id.dialog_setting_radio_spanish);
        mImageViewGermany= (RadioButton) findViewById(R.id.dialog_setting_radio_german);
        mImageViewFrench=  (RadioButton) findViewById(R.id.dialog_setting_radio_french);
        mImageViewPortuguese= (RadioButton) findViewById(R.id.dialog_setting_radio_portugese);

        mImageViewSpanish.setOnClickListener(this);
        mImageViewEnglish.setOnClickListener(this);
        mImageViewGermany.setOnClickListener(this);
        mImageViewFrench.setOnClickListener(this);
        mImageViewPortuguese.setOnClickListener(this);
        mimg_logout.setOnClickListener(this);
        mImageView.setOnClickListener(this);
        mImageView.setOnClickListener(this);

        mRelativeLayoutEnglish.setOnClickListener(this);
        mRelativeLayoutPortuguese.setOnClickListener(this);
        mRelativeLayoutSpanish.setOnClickListener(this);
        mRelativeLayoutGerman.setOnClickListener(this);
        mRelativeLayoutFrench.setOnClickListener(this);


        if (mSharePreferencePermanent.getAppLanguage().equalsIgnoreCase(english)) {

            mImageViewEnglish.setChecked(true);
            mImageViewSpanish.setChecked(false);
            mImageViewFrench.setChecked(false);
            mImageViewGermany.setChecked(false);
            mImageViewPortuguese.setChecked(false);
            languageSelectedLocal = Utils.ENGLISH;
            mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_selected);

        } else if (mSharePreferencePermanent.getAppLanguage().equalsIgnoreCase(spanish)) {

            mImageViewEnglish.setChecked(false);
            mImageViewSpanish.setChecked(true);
            mImageViewFrench.setChecked(false);
            mImageViewGermany.setChecked(false);
            mImageViewPortuguese.setChecked(false);

            languageSelectedLocal = Utils.SPANISH;
            mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_selected);
            mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);

        } else if (mSharePreferencePermanent.getAppLanguage().equalsIgnoreCase(Portuguese)) {

            mImageViewEnglish.setChecked(false);
            mImageViewSpanish.setChecked(false);
            mImageViewFrench.setChecked(false);
            mImageViewGermany.setChecked(false);
            mImageViewPortuguese.setChecked(true);

            languageSelectedLocal = Utils.PORTUGUESE;
            mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
            mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_selected);

        } else if (mSharePreferencePermanent.getAppLanguage().equalsIgnoreCase(french)) {

            mImageViewEnglish.setChecked(false);
            mImageViewSpanish.setChecked(false);
            mImageViewFrench.setChecked(true);
            mImageViewGermany.setChecked(false);
            mImageViewPortuguese.setChecked(false);

            languageSelectedLocal = Utils.FRENCH;
            mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_selected);
            mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);

        } else if (mSharePreferencePermanent.getAppLanguage().equalsIgnoreCase(german)) {

            mImageViewEnglish.setChecked(false);
            mImageViewSpanish.setChecked(false);
            mImageViewFrench.setChecked(false);
            mImageViewGermany.setChecked(true);
            mImageViewPortuguese.setChecked(false);
            
            languageSelectedLocal = Utils.GERMAN;
            mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
            mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_selected);
        }
    }

    public String getAppVersion() {
        String versionCode = "1.0";
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.setting_relatvie_english:

                mImageViewEnglish.setChecked(true);
                mImageViewSpanish.setChecked(false);
                mImageViewFrench.setChecked(false);
                mImageViewGermany.setChecked(false);
                mImageViewPortuguese.setChecked(false);


                languageSelectedLocal = Utils.ENGLISH;

                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_selected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.setting_relatvie_spanish:

                mImageViewEnglish.setChecked(false);
                mImageViewSpanish.setChecked(true);
                mImageViewFrench.setChecked(false);
                mImageViewGermany.setChecked(false);
                mImageViewPortuguese.setChecked(false);


                languageSelectedLocal = Utils.SPANISH;

                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_selected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.setting_relatvie_german:

                mImageViewEnglish.setChecked(false);
                mImageViewSpanish.setChecked(false);
                mImageViewFrench.setChecked(false);
                mImageViewGermany.setChecked(true);
                mImageViewPortuguese.setChecked(false);


                languageSelectedLocal = Utils.GERMAN;

                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_selected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.setting_relatvie_french:

                mImageViewEnglish.setChecked(false);
                mImageViewSpanish.setChecked(false);
                mImageViewFrench.setChecked(true);
                mImageViewGermany.setChecked(false);
                mImageViewPortuguese.setChecked(false);

                languageSelectedLocal = Utils.FRENCH;

                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_selected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;

            case R.id.setting_relatvie_portuguese:

                mImageViewEnglish.setChecked(false);
                mImageViewSpanish.setChecked(false);
                mImageViewFrench.setChecked(false);
                mImageViewGermany.setChecked(false);
                mImageViewPortuguese.setChecked(true);
                languageSelectedLocal = Utils.PORTUGUESE;
                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_selected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;

            case R.id.dialog_setting_radio_default_english:

                mImageViewEnglish.setChecked(true);
                mImageViewSpanish.setChecked(false);
                mImageViewFrench.setChecked(false);
                mImageViewGermany.setChecked(false);
                mImageViewPortuguese.setChecked(false);


                languageSelectedLocal = Utils.ENGLISH;

                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_selected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.dialog_setting_radio_spanish:

                mImageViewEnglish.setChecked(false);
                mImageViewSpanish.setChecked(true);
                mImageViewFrench.setChecked(false);
                mImageViewGermany.setChecked(false);
                mImageViewPortuguese.setChecked(false);


                languageSelectedLocal = Utils.SPANISH;

                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_selected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.dialog_setting_radio_german:

                mImageViewEnglish.setChecked(false);
                mImageViewSpanish.setChecked(false);
                mImageViewFrench.setChecked(false);
                mImageViewGermany.setChecked(true);
                mImageViewPortuguese.setChecked(false);


                languageSelectedLocal = Utils.GERMAN;

                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_selected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;
            case R.id.dialog_setting_radio_french:

                mImageViewEnglish.setChecked(false);
                mImageViewSpanish.setChecked(false);
                mImageViewFrench.setChecked(true);
                mImageViewGermany.setChecked(false);
                mImageViewPortuguese.setChecked(false);

                languageSelectedLocal = Utils.FRENCH;

                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_selected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;

            case R.id.dialog_setting_radio_portugese:

                mImageViewEnglish.setChecked(false);
                mImageViewSpanish.setChecked(false);
                mImageViewFrench.setChecked(false);
                mImageViewGermany.setChecked(false);
                mImageViewPortuguese.setChecked(true);
                languageSelectedLocal = Utils.PORTUGUESE;
                mImageViewPortuguese.setBackgroundResource(R.drawable.ic_lang_selected);
                mImageViewGermany.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewFrench.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewSpanish.setBackgroundResource(R.drawable.ic_lang_unselected);
                mImageViewEnglish.setBackgroundResource(R.drawable.ic_lang_unselected);
                Toast.makeText(getApplicationContext(), languageSelectedLocal + " Selected", Toast.LENGTH_SHORT).show();
                break;


            case R.id.img_logout:

               // MainActivity activity1 = new MainActivity();
               // activity1.logout(MySettingActivity.this);
                logout(MySettingActivity.this);
                break;

            case R.id.main_menu:
                finish();
                break;
        }
    }



    public void logout(Activity mActivity) {
        final Dialog mDialog = new Dialog(mActivity, R.style.PauseDialog);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        mDialog.setContentView(R.layout.dialog_logout);
        ImageView imageViewok = (ImageView) mDialog.findViewById(R.id.img_ok_logout);
        ImageView imageViewcancel= (ImageView) mDialog.findViewById(R.id.img_ok_cancel);
        imageViewok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TinyDB mTinyDB = new TinyDB(getApplicationContext());
                mTinyDB.clear();
                callFacebookLogout(MySettingActivity.this);
                if(mSharePreference != null)
                {
                    mSharePreference.clearData();
                }


                mDialog.dismiss();
            }
        });
        imageViewcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public void callFacebookLogout(Context context) {

        GraphRequest.Callback callback = new GraphRequest.Callback() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onCompleted(GraphResponse response) {

                try {

                    if(TAGS.mStalkerUsers!=null)
                        TAGS.mStalkerUsers.clear();
                    if(TAGS.mViewersMainScreen!=null)
                        TAGS.mViewersMainScreen.clear();
                    if(TAGS.mViewersNew!=null)
                        TAGS.mViewersNew.clear();

                    if (response.getError() != null) {
                        if(mSharePreference != null){
                            mSharePreference.clearData();
                        }


                        Intent mIntent = new Intent(MySettingActivity.this, LoginPagerScreen.class);
                        startActivity(mIntent);
                        finishAffinity();
                    } else if (response !=null && response.getJSONObject().getBoolean(SUCCESS)) {
                        LoginManager.getInstance().logOut();
                        if(mSharePreference != null){
                            mSharePreference.clearData();
                        }


                        Intent mIntent = new Intent(MySettingActivity.this, LoginPagerScreen.class);
                        startActivity(mIntent);
                        finishAffinity();
                    }
                } catch (JSONException ex) { /* no op */ }
                TAGS.userList = true;
            }
        };
        GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(),
                GRAPH_PATH, new Bundle(), HttpMethod.DELETE, callback);
        request.executeAsync();


    }

}
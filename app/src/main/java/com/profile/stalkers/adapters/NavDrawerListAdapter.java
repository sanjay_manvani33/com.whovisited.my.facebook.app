package com.profile.stalkers.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.profile.stalkers.AdmireActivity;
import com.profile.stalkers.MainActivity;
import com.profile.stalkers.MoreAppsActivity;
import com.profile.stalkers.MySettingActivity;
import com.profile.stalkers.model.NavDrawerItem;
import com.profile.stalkers.R;
import com.profile.stalkers.tips.TipScreen;
import com.profile.stalkers.utils.SharePreference;
import com.profile.stalkers.utils.TAGS;
import com.profile.stalkers.utils.Utils;

import java.util.ArrayList;

import static com.profile.stalkers.model.dataStrings.email_id;


@SuppressLint("ResourceAsColor")
public class NavDrawerListAdapter extends BaseAdapter {

    private Activity activity;
    private ArrayList<NavDrawerItem> navDrawerItems;
    SharePreference mSharePreference;

    public NavDrawerListAdapter(ArrayList<NavDrawerItem> navDrawerItems, Activity activity) {
        mSharePreference = new SharePreference(activity);
        this.navDrawerItems = navDrawerItems;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public NavDrawerItem getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MyViewHolder mViewHolder;
        if (convertView == null) {
            mViewHolder = new MyViewHolder();
            LayoutInflater mInflater = (LayoutInflater)
                    activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.layout_drawer_list_item, null);
            mViewHolder.mViewTop =  convertView.findViewById(R.id.top);
            mViewHolder.mImageView = (ImageView) convertView.findViewById(R.id.navigation_imageview_user);
            mViewHolder.mdrawer_back = (ImageView)convertView.findViewById(R.id.drawer_back);
            mViewHolder.mImageViewIcon = (ImageView) convertView.findViewById(R.id.drawer_icon);
            mViewHolder.mTextViewMain = (TextView) convertView.findViewById(R.id.titlemain);
            mViewHolder.mTextViewLogout = (ImageView) convertView.findViewById(R.id.txt_logout);
            mViewHolder.mTextViewUserName = (TextView) convertView.findViewById(R.id.navigation_textview_user_name);
            mViewHolder.mLinearLayoutMain = (RelativeLayout) convertView.findViewById(R.id.layout_main);
            mViewHolder.logout_layout = (RelativeLayout) convertView.findViewById(R.id.logout_layout);
            mViewHolder.mLinearLayoutChild = (LinearLayout) convertView.findViewById(R.id.layout_child);
            mViewHolder.mtxt_logout = (ImageView) convertView.findViewById(R.id.txt_logout);
            convertView.setTag(mViewHolder);

        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }
        if(position==0)
        {
            mViewHolder.mViewTop.setVisibility(View.VISIBLE);
        }
        else
        {
            mViewHolder.mViewTop.setVisibility(View.GONE);
        }
        if (position > 5) {
            mViewHolder.mLinearLayoutMain.setVisibility(View.GONE);
            mViewHolder.mLinearLayoutChild.setVisibility(View.GONE);
            mViewHolder.mTextViewUserName.setVisibility(View.GONE);
            mViewHolder.mTextViewLogout.setVisibility(View.VISIBLE);
            mViewHolder.logout_layout.setVisibility(View.VISIBLE);
        }else if(position == 0){
            mViewHolder.mLinearLayoutMain.setVisibility(View.VISIBLE);
            mViewHolder.mTextViewUserName.setVisibility(View.VISIBLE);
            mViewHolder.mTextViewLogout.setVisibility(View.GONE);
            mViewHolder.logout_layout.setVisibility(View.GONE);
        }
        else {
            mViewHolder.mLinearLayoutMain.setVisibility(View.GONE);
            mViewHolder.mLinearLayoutChild.setVisibility(View.VISIBLE);
            mViewHolder.mTextViewUserName.setVisibility(View.GONE);
            mViewHolder.mTextViewLogout.setVisibility(View.GONE);
            mViewHolder.logout_layout.setVisibility(View.GONE);
        }
        mViewHolder.mTextViewMain.setText(navDrawerItems.get(position).getTitle());

        mViewHolder.mTextViewMain.setText(mViewHolder.mTextViewMain.getText().toString());
        mViewHolder.mTextViewUserName.setText(mSharePreference.getmStringUserName());

        mViewHolder.mLinearLayoutChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(activity.getString(R.string.suggestion).equals(mViewHolder.mTextViewMain.getText().toString())){
                    MainActivity activity1 = (MainActivity) activity;

                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email_id});
                    intent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.suggestion));
                    if (intent.resolveActivity(activity1.getPackageManager()) != null) {
                        activity1.startActivity(intent);
                    }
                    activity1.mDrawerLayout.closeDrawers();
                }
                if(activity.getString(R.string.report_issue).equals(mViewHolder.mTextViewMain.getText().toString())){
                    MainActivity activity1 = (MainActivity) activity;

                    activity1.mDrawerLayout.closeDrawers();

                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email_id});
                    intent.putExtra(Intent.EXTRA_SUBJECT, "Report an issue");
                    intent.putExtra(Intent.EXTRA_TEXT, "Device name  :  " + mSharePreference.getDeviceId() + "\n" +
                            "OS Version   :  " + mSharePreference.getOS() + "\n" +
                            "App Link     :  " + mSharePreference.getAppUrl() + "\n");
                    if (intent.resolveActivity(activity1.getPackageManager()) != null) {
                        activity1.startActivity(intent);
                    }

                }
                if(activity.getString(R.string.moreapps).equals(mViewHolder.mTextViewMain.getText().toString())){
                    MainActivity activity1 = (MainActivity) activity;
                    activity1.mDrawerLayout.closeDrawers();
                    activity1.activityDestroyed = true;
                    Intent mIntent = new Intent(activity, MoreAppsActivity.class);
                    activity.startActivity(mIntent);

                }
                if(activity.getString(R.string.facebook_tips).equals(mViewHolder.mTextViewMain.getText().toString())){
                    MainActivity activity1 = (MainActivity) activity;
                    activity1.mDrawerLayout.closeDrawer(Gravity.LEFT);
                    activity1.activityDestroyed = true;
                    Intent  mIntent = new Intent(activity, TipScreen.class);
                    activity. startActivity(mIntent);
                }
                if(activity.getString(R.string.settings).equals(mViewHolder.mTextViewMain.getText().toString())){
                    MainActivity activity1 = (MainActivity) activity;
                    activity1.mDrawerLayout.closeDrawers();
                    activity1.activityDestroyed = true;
                    Intent  mIntent = new Intent(activity, MySettingActivity.class);
                    activity.startActivity(mIntent);
                }
                if(activity.getString(R.string.admire).equals(mViewHolder.mTextViewMain.getText().toString())){
                    MainActivity activity1 = (MainActivity) activity;
                    activity1.activityDestroyed = true;
                    if (activity1.no_user_dialog.getVisibility() == View.VISIBLE) {
                        Utils.SetDiolog(activity, activity1.getString(R.string.No_Users));
                    } else {
                        if(TAGS.mViewersMainScreen.size()>0){
                            Intent mIntent = new Intent(activity, AdmireActivity.class);
                            activity.startActivity(mIntent);
                            activity1.mDrawerLayout.closeDrawers();
                        }
                        activity1.mDrawerLayout.closeDrawers();

                    }
                }

            }
        });



        mViewHolder.mdrawer_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity1 = (MainActivity) activity;
                activity1.mDrawerLayout.closeDrawers();
            }
        });

        mViewHolder.mtxt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity activity1 = (MainActivity) activity;
                activity1.mDrawerLayout.closeDrawers();
                activity1.logout(activity1);
            }
        });
        mViewHolder.mImageViewIcon.setImageResource(navDrawerItems.get(position).getIcon());

        if (!mSharePreference.getmStringUserName().equals(""))
        {
                    Glide.with(activity)
                    .load(mSharePreference.getImage())
                    .placeholder(R.drawable.ic_blur)
                    .into(mViewHolder.mImageView);
        }
        return convertView;
    }

    private class MyViewHolder {
        TextView mTextViewUserName;
        ImageView mImageView,mTextViewLogout,mdrawer_back;
        ImageView mImageViewIcon,mtxt_logout;
        RelativeLayout mLinearLayoutMain,logout_layout;
        LinearLayout mLinearLayoutChild;
        TextView mTextViewMain;
        View mViewTop;
    }

}

package com.profile.stalkers.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.profile.stalkers.R;
import com.profile.stalkers.utils.SharePreference;
import com.profile.stalkers.model.Viewers;

import java.util.ArrayList;

public class DataAdapterStalker extends RecyclerView.Adapter<DataAdapterStalker.ViewHolder> {

    LayoutInflater inflater;
    Context context;
    Activity activity;
    ArrayList<Viewers>myList=new ArrayList<Viewers>();


    SharePreference mSharePreference;

    public DataAdapterStalker(Activity mActivity, ArrayList<Viewers> mListItems) {
        this.context = mActivity;

        mSharePreference=new SharePreference(mActivity);

        this.myList = mListItems;
        this.activity= mActivity;

        inflater = LayoutInflater.from(activity);

    }
 
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_grid_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        if(myList.size()>0)
        {


            int index = i+1;

            if(!myList.get(i).isShow())
            {
                viewHolder.mTextViewItemName.setText(index+". "+activity.getResources().getString(R.string.unlock).toString().toUpperCase());
                viewHolder.mImageView.setImageResource(R.drawable.img_purchase_lock);
            }
            else
            {
                String str = myList.get(i).getName();
                String[] splitStr = str.split("\\s+");
//                viewHolder.mTextViewItemName.setText(index+". "+splitStr[0]);

                viewHolder.mTextViewItemName.setText(index+". "+splitStr[0].toUpperCase());
                Glide.with(activity)
                        .load(myList.get(i).getImage())
                        .placeholder(R.drawable.ic_blur)
                        .into(viewHolder.mImageView);
            }
        }
    }
 
    @Override
    public int getItemCount() {
        return myList.size();
    }
 
    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView mTextViewItemName;

        ImageView mImageView;


        public ViewHolder(View view) {
            super(view);
            mTextViewItemName=(TextView) view.findViewById(R.id.textview);

            mImageView=(ImageView) view.findViewById(R.id.imageview);



        }
    }
 
}
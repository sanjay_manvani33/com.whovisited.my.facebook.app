package com.profile.stalkers.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.profile.stalkers.model.MoreAppsDataModel;
import com.profile.stalkers.R;

import java.util.ArrayList;

public class MoreAppsAdapter extends BaseAdapter {


    LayoutInflater mLayoutInflater;


    ArrayList<MoreAppsDataModel> models;

    Activity mActivity;

    public MoreAppsAdapter(Activity context, ArrayList<MoreAppsDataModel> models) {
        mLayoutInflater = context.getLayoutInflater();
        mActivity = context;
        this.models = models;

    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.layout_moreapps_item, null);
            holder = new ViewHolder();

            holder.mImageView = (ImageView) convertView.findViewById(R.id.moreapps_image_app);
            holder.mTextViewAppName = (TextView) convertView.findViewById(R.id.moreapps_text_appname);
            holder.mTextViewRate = (TextView) convertView.findViewById(R.id.moreapps_text_rating);
            holder.mTextViewInstall = (TextView) convertView.findViewById(R.id.moreapps_text_install_button);
            holder.mRelative_moreapp_detail = (RelativeLayout) convertView.findViewById(R.id.relative_appDetail);

            holder.mRelative_moreapp_detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    try {
                        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + models.get(pos).getPlay_url())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Emobi")));

                    }
                }
            });

            holder.mTextViewInstall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    try {
                        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + models.get(pos).getPlay_url())));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Emobi")));

                    }
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextViewInstall.setTag(position);
        holder.mRelative_moreapp_detail.setTag(position);
        Glide.with(mActivity)
                .load(models.get(position).getImage_path())
                .into(holder.mImageView);
        holder.mTextViewAppName.setText(models.get(position).getName().toString());
        holder.mTextViewRate.setText("RATING | "+models.get(position).getRatings());
        Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.hyperspace);
        animation.setDuration(500);
        convertView.startAnimation(animation);
        return convertView;
    }

    class ViewHolder {

        ImageView mImageView;
        TextView mTextViewAppName;
        TextView mTextViewRate;
        TextView mTextViewInstall;
        RelativeLayout mRelative_moreapp_detail;
    }

}
package com.profile.stalkers.tips;

public class ListModel {

    private static final String[] MODEL = {
        "Zero",
        "One",
        "Two",
        "Three",
        "Four",
        "Five",
        "Six",
        "Seven",
        "Eight",
        "Nine",
        "Ten",
        "Eleven",
        "Twelve",
        "Thirteen",
        "Fourteen",
        "Fifteen",
        "Sixteen",
        "Seventeen",
        "Eighteen",
        "Nineteen",
        "Twenty",
        "Twentyone",
        "Twentytwo",
        "Twentythree",
        "Twentyfour",
        "Twentyfive",
        "Twentysix",
        "Twentyseven",
        "Twentyeight",
        "Twentynine",
        "Thirty",
        "Thirtyone",
        "Thirtytwo",
        "Thirtythree",
        "Thirtyfour",
        "Thirtyfive",
        "Thirtysix",
        "Thirtyseven",
        "Thirtyeight",
        "Thirtynine",
        "Forty",
        "Fortyone",
        "Fortytwo",
        "Fortythree",
        "Fortyfour",
        "Fortyfive",
        "Fortysix",
        "Fortyseven",
        "Fortyeight",
        "Fortynine",
        "Fifty",
        "Fiftyone",
        "Fiftytwo",
        "Fiftythree",
        "Fiftyfour",
        "Fiftyfive",
        "Fiftysix",
        "Fiftyseven",
        "Fiftyeight",
        "Fiftynine",
        "Sixty",
        "Sixtyone",
        "Sixtytwo",
        "Sixtythree",
        "Sixtyfour",
        "Sixtyfive",
        "Sixtysix",
        "Sixtyseven",
        "Sixtyeight",
        "Sixtynine",
        "Seventy",
        "Seventyone",
        "Seventytwo",
        "Seventythree",
        "Seventyfour",
        "Seventyfive",
        "Seventysix",
        "Seventyseven",
        "Seventyeight",
        "Seventynine",
        "Eighty",
        "Eightyone",
        "Eightytwo",
        "Eightythree",
        "Eightyfour",
        "Eightyfive",
        "Eightysix",
        "Eightyseven",
        "Eightyeight",
        "Eightynine",
        "Ninety",
        "Ninetyone",
        "Ninetytwo",
        "Ninetythree",
        "Ninetyfour",
        "Ninetyfive",
        "Ninetysix",
        "Ninetyseven",
        "Ninetyeight",
        "Ninetynine"
    };

    public static String[] getModel() {
        return MODEL;
    }


     String name = "";
    String nameid = "";
    String count = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameid() {
        return nameid;
    }

    public void setNameid(String nameid) {
        this.nameid = nameid;
    }


    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public ListModel() { }
}
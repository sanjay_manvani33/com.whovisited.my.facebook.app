package com.profile.stalkers.tips;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.profile.stalkers.MainActivity;
import com.profile.stalkers.R;
import com.profile.stalkers.model.GetTipsInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import java.util.Random;

import me.relex.circleindicator.CircleIndicator;

import static com.profile.stalkers.model.dataStrings.RESPONSE_CODE;
import static com.profile.stalkers.model.dataStrings.TIPS_URL;
import static com.profile.stalkers.model.dataStrings.TRUE1;
import static com.profile.stalkers.model.dataStrings.clicked_item;
import static com.profile.stalkers.model.dataStrings.count_new;
import static com.profile.stalkers.model.dataStrings.master;
import static com.profile.stalkers.model.dataStrings.please_wait_tips;


public class Single_View_back extends AppCompatActivity  implements View.OnClickListener{

    private String url = TIPS_URL;

    TextView head_text;
    TextView image_tips, bottom_text, top_text;

    private boolean init = false;
    ViewPager viewPager;
    ViewPagerAdapter adapter;

    ScrollView mScrollView;

    ArrayList<String> items;

    JSONObject json;

    ImageView image_of_items;
    ImageView mImageView_Menu;

    private String TAG = MainActivity.class.getSimpleName();

    private ProgressDialog pDialog;

    GetTipsInfo getTips;

    LinearLayout pagerLayout;
    // URL to get contacts JSON

    int count;
    int total_Tips_images;

    ArrayList<String> image_item = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_view_back);

        Intent mIntent = getIntent();
        String item = mIntent.getStringExtra(clicked_item);
        String id = mIntent.getStringExtra(master);
        count = mIntent.getIntExtra(count_new, 0);


        head_text      = (TextView) findViewById(R.id.title_items);
        image_tips     = (TextView) findViewById(R.id.image_tips);
        bottom_text    = (TextView) findViewById(R.id.bottom_text);
        top_text       = (TextView) findViewById(R.id.top_text);
        viewPager      = (ViewPager) findViewById(R.id.pager);

        mScrollView    = (ScrollView) findViewById(R.id.mScrollView);

        image_of_items = (ImageView) findViewById(R.id.img);
        mImageView_Menu = (ImageView) findViewById(R.id.main_menu);

        pagerLayout = (LinearLayout) findViewById(R.id.pagerLayout);
        items = new ArrayList<>();

        head_text.setText(item);

        mImageView_Menu.setOnClickListener(this);

        getSupportActionBar().hide();

        url = url + id;

        dispalymatrics();

        new CallRegisterService().execute();

    }
    public void dispalymatrics(){

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;

        RelativeLayout.LayoutParams mLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (width + 200));
        mLayoutParams.addRule(RelativeLayout.BELOW, R.id.indicator_circle);
        viewPager.setLayoutParams(mLayoutParams);

    }

    // Boom Menu

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        // Use a param to record whether the boom button has been initialized
        // Because we don't need to init it again when onResume()
        if (init) return;
        init = true;

        //  initBoom();
    }

    private String[] Colors = {
            "#F44336",
            "#9C27B0",
            "#2196F3",
            "#03A9F4",
            "#00BCD4",
            "#009688",
            "#4CAF50",
            "#8BC34A",
            "#CDDC39",
            "#FFEB3B",
            "#FFC107",
            "#E91E63",
            "#FF9800",
            "#FF5722",
            "#795548",
            "#9E9E9E",
            "#607D8B"};

    public int getRandomColor() {
        Random random = new Random();
        int p = random.nextInt(Colors.length);
        return Color.parseColor(Colors[p]);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.main_menu:
                //method of login
                onBackPressed();
                break;

            default:
                break;
        }
    }

    ////////////////////////////////////////////////
    ///////// Get Data From Server//////////////////
    ////////////////////////////////////////////////

    public class CallRegisterService extends AsyncTask<Void, Void, String> {
        ArrayList<String> all_image_des = new ArrayList<>();
        String response = "";
        int n;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(Single_View_back.this);
            pDialog.setMessage(please_wait_tips);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... passing) {
            response = uploadService(url);
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);

            pDialog.dismiss();

            try {
                json = new JSONObject(response);
                if (json.getString(RESPONSE_CODE).equals(TRUE1)) {
                    Gson mGson = new GsonBuilder().create();
                    getTips = mGson.fromJson(response, GetTipsInfo.class);

                    if (getTips != null) {
                        total_Tips_images = getTips.getTipsDetail().size();

                        for (int i = 0; i < getTips.getTipsDetail().size(); i++) {
                            bottom_text.setText(getTips.getTipsDetail().get(i).getBottom_detail());
                            top_text.setText(getTips.getTipsDetail().get(i).getTop_detail());

                            items.add(getTips.getTipsDetail().get(i).getTip_image());
                            all_image_des.add(getTips.getTipsDetail().get(i).getImage_desc());


                        }
                        if ((items.equals("") && all_image_des.equals("")) || items.equals(""))
                        {
                            pagerLayout.setVisibility(View.GONE);

                        } else {
                            adapter = new ViewPagerAdapter(Single_View_back.this, items, all_image_des);
                            viewPager.setAdapter(adapter);
                            CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator_circle);

                            if (all_image_des.size() > 1)
                                indicator.setViewPager(viewPager);
                            else
                                indicator.setVisibility(View.GONE);
                        }


                        if (count == 0) {
                            pagerLayout.setVisibility(View.GONE);
                        }else {
                            pagerLayout.setVisibility(View.VISIBLE);

                        }

                    }

                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public String uploadService(String strings) {

        String result = "";

        java.net.URL url1 = null;
        try {
            url1 = new java.net.URL(strings);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();



            conn.setRequestMethod("POST");


// read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        catch (ProtocolException e) {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            result = null;
            e.printStackTrace();

        }
        return result;

    }




}

package com.profile.stalkers.tips;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.profile.stalkers.R;
import com.profile.stalkers.model.GetTipsInfo;
import com.profile.stalkers.utils.AsyncRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import me.relex.circleindicator.CircleIndicator;


public class TipsDetailsActivity extends AppCompatActivity implements  View.OnClickListener,AsyncRequest.OnAsyncRequestComplete{

    public String RESPONSE_CODE = "ResponseCode";
    public String TRUE = "1";


    public final String RESPONSECODE = "ResponseCode";
    public final String CLICKED_ITEM = "Clicked_Item";
    public final String MASTER = "master";
    public final String COUNT = "count";

    TextView head_text;
    private boolean init = false;
    ViewPager viewPager;
    ViewPagerAdapter adapter;
    ScrollView mScrollView;
    ArrayList<String> items;
    JSONObject json;
    int total_Tips_images;
    ImageView image_of_items;
    ImageView mImageViewMenu;

    static {
        System.loadLibrary("keys");
    }

    private ProgressDialog pDialog;
    TextView image_tips, bottom_text, top_text;
    GetTipsInfo getTips;
    LinearLayout pagerLayout;
    // URL to get contacts JSON
    private String url ;
    int count;
    CircleIndicator mCirclePageIndicator;
    ArrayList<String> image_item = new ArrayList<String>();
    LinearLayout mBottomLine;

    public native String getid();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        url=getid();
        setContentView(R.layout.activity_single_view_back);

        getSupportActionBar().hide();

        head_text = (TextView) findViewById(R.id.title_items);
        viewPager = (ViewPager) findViewById(R.id.pager);
        image_tips = (TextView) findViewById(R.id.image_tips);
        bottom_text = (TextView) findViewById(R.id.bottom_text);
        top_text = (TextView) findViewById(R.id.top_text);

        pagerLayout = (LinearLayout) findViewById(R.id.pagerLayout);
        mBottomLine= (LinearLayout) findViewById(R.id.bottom_line);

        mScrollView = (ScrollView) findViewById(R.id.mScrollView);

        image_of_items = (ImageView) findViewById(R.id.img);
        mImageViewMenu = (ImageView)findViewById(R.id.main_menu);


        mImageViewMenu.setOnClickListener(this);

        Intent mIntent1 = getIntent();
        String tipsTitle = mIntent1.getStringExtra(R.string.clicked_item+"");
        String  tipsPosition  = mIntent1.getStringExtra(R.string.master+"");

        url = url +  tipsPosition ;

        count = mIntent1.getIntExtra(R.string.count+"", 0);

        getSupportActionBar().hide();


        items = new ArrayList<>();

        head_text.setText(tipsTitle);

        mCirclePageIndicator = (CircleIndicator) findViewById(R.id.indicator_circle);
        mCirclePageIndicator.setViewPager(viewPager);

       AsyncRequest getPosts =
                new AsyncRequest(TipsDetailsActivity.this, AsyncRequest.POSTURLCONNECTION);
       getPosts.execute(getid()+ tipsPosition );
    }

    // Boom Menu

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        // Use a param to record whether the boom button has been initialized
        // Because we don't need to init it again when onResume()
        if (init) return;
        init = true;

    }


    private String[] Colors = {
            "#F44336",
            "#9C27B0",
            "#2196F3",
            "#03A9F4",
            "#00BCD4",
            "#009688",
            "#4CAF50",
            "#8BC34A",
            "#CDDC39",
            "#FFEB3B",
            "#FFC107",
            "#E91E63",
            "#FF9800",
            "#FF5722",
            "#795548",
            "#9E9E9E",
            "#607D8B"};

    public int getRandomColor() {
        Random random = new Random();
        int p = random.nextInt(Colors.length);
        return Color.parseColor(Colors[p]);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.main_menu:
                //method of login
                onBackPressed();
                break;

            default:
                break;
        }
    }

    @Override
    public void asyncResponse(String response, String label) {

        if(response!=null) {
            ArrayList<String> all_image_des = new ArrayList<>();

            Log.e("respnsle", response);

            try {
                json = new JSONObject(response);
                if (json.getString(RESPONSECODE).equals(TRUE)) {
                    Gson mGson = new GsonBuilder().create();
                    getTips = mGson.fromJson(response, GetTipsInfo.class);

                    if (getTips != null) {
                        total_Tips_images = getTips.getTipsDetail().size();

                        for (int i = 0; i < getTips.getTipsDetail().size(); i++) {
                            bottom_text.setText(getTips.getTipsDetail().get(i).getBottom_detail());
                            top_text.setText(getTips.getTipsDetail().get(i).getTop_detail());

                            items.add(getTips.getTipsDetail().get(i).getTip_image());
                            all_image_des.add(getTips.getTipsDetail().get(i).getImage_desc());


                        }
                        if (bottom_text.getText().toString().equals("")) {
                            bottom_text.setVisibility(View.GONE);
                            mBottomLine.setVisibility(View.GONE);
                        } else {
                            bottom_text.setVisibility(View.VISIBLE);
                            mBottomLine.setVisibility(View.VISIBLE);
                        }
                        if ((items.equals("") && all_image_des.equals("")) || items.equals("")) {
                            pagerLayout.setVisibility(View.GONE);

                        } else {
                            adapter = new ViewPagerAdapter(TipsDetailsActivity.this, items, all_image_des);
                            viewPager.setAdapter(adapter);
                            CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator_circle);

                            if (all_image_des.size() > 1)
                                indicator.setViewPager(viewPager);
                            else
                                indicator.setVisibility(View.GONE);
                        }


                        if (count == 0) {
                            pagerLayout.setVisibility(View.GONE);
                        } else {
                            pagerLayout.setVisibility(View.VISIBLE);

                        }

                    }

                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    ////////////////////////////////////////////////
    ///////// Get Data From Server//////////////////
    ////////////////////////////////////////////////








}

package com.profile.stalkers.tips;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.profile.stalkers.R;
import com.profile.stalkers.utils.AsyncRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class TipScreen extends AppCompatActivity implements View.OnClickListener,ListView.OnItemClickListener,AsyncRequest.OnAsyncRequestComplete{



    private ListView mList;

    ArrayList<ListModel> models;


    TipListAdapter mTipListAdapter;

    AdView mAdView;

    AdRequest adRequest;

    TextView mTextViewTitle;

    ImageView mImageView_Menu;
    public native String gettips();
    public native String gettipsconstant();
    public native String gettips1();
    public native String gettips2();
    public native String gettips3();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tip_screen);

        getSupportActionBar().hide();

        mTextViewTitle= (TextView) findViewById(R.id.text_tips);
        mList = (ListView) findViewById(R.id.listview);
        mImageView_Menu =(ImageView)findViewById(R.id.main_menu);

        mTextViewTitle.setText(mTextViewTitle.getText().toString().toUpperCase());
        mTextViewTitle.setText(mTextViewTitle.getText().toString());

        models = new ArrayList<>();

        mImageView_Menu.setOnClickListener(this);
        mList.setOnItemClickListener(this);

       //advertisment();

        AsyncRequest getPosts =
                new AsyncRequest(TipScreen.this, AsyncRequest.POSTURLCONNECTION);
        getPosts.execute(gettips());
    }
    public void advertisment(){
        adRequest = new AdRequest.Builder().build();
        mAdView = (AdView) findViewById(R.id.adView_id);
        mAdView.loadAd(adRequest);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.main_menu:

                onBackPressed();

                break;
        }
    }
    public void listview_click(int position){
        Intent intent = new Intent(TipScreen.this, TipsDetailsActivity.class);
        intent.putExtra(R.string.master+"", models.get(position).getNameid());
        intent.putExtra(R.string.clicked_item+"", models.get(position).getName());
        intent.putExtra(R.string.count+"", Integer.parseInt(models.get(position).getCount()));
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listview_click(position);
    }

    @Override
    public void asyncResponse(String response, String label) {

        if(response!=null) {
            try {
                JSONObject jObject = new JSONObject(response);

                JSONArray mJsonArray = jObject.getJSONArray(gettipsconstant());

                for (int i = 0; i < mJsonArray.length(); i++) {
                    JSONObject mJsonObject = mJsonArray.getJSONObject(i);
                    ListModel modelname = new ListModel();

                    modelname.setName(mJsonObject.getString(gettips1()));
                    modelname.setNameid(mJsonObject.getString(gettips2()));
                    modelname.setCount(mJsonObject.getString(gettips3()));

                    models.add(modelname);

                }
                mTipListAdapter = new TipListAdapter(TipScreen.this, models);
                mList.setAdapter(mTipListAdapter);

            } catch (JSONException e) {

            }
        }
    }
}

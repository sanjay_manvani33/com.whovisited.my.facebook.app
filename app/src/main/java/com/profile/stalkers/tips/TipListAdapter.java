package com.profile.stalkers.tips;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.profile.stalkers.R;

import java.util.ArrayList;

class TipListAdapter extends BaseAdapter {


    LayoutInflater mLayoutInflater;


    ArrayList<ListModel> models;

    Activity mActivity;

    public TipListAdapter(Activity context,ArrayList<ListModel> models) {
        mLayoutInflater = context.getLayoutInflater();
        mActivity=context;
        this.models = models;

    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.layout_tip_item, null);
            holder = new ViewHolder();

            holder.text = (TextView) convertView.findViewById(R.id.text);
            holder.textId = (TextView) convertView.findViewById(R.id.number);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(models.get(position).getName());
        holder.textId.setText((position+1)+"");

        Animation animation= AnimationUtils.loadAnimation(mActivity, R.anim.shake);
        animation.setDuration(500);
        convertView.startAnimation(animation);
        return convertView;
    }

     class ViewHolder
     {
         TextView text;
         TextView textId;
    }

}
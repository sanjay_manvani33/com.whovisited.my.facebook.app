package com.profile.stalkers.tips;

import android.app.Activity;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.profile.stalkers.R;

import java.util.ArrayList;


public class ViewPagerAdapter extends PagerAdapter {

    Activity _activity;
    LayoutInflater inflater;
    ArrayList<String> items;
    ArrayList<String> all_image_des;
    public ViewPagerAdapter(Activity activity, ArrayList<String> items, ArrayList<String> all_image_des ) {
        this._activity = activity;
        this.items = items;
        this.all_image_des = all_image_des;
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public int getCount() {
        return items.size();

    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {

        inflater = (LayoutInflater) _activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewLayout = inflater.inflate(R.layout.layout_single_view_in_pager, container, false);

        final ImageView image = (ImageView)viewLayout.findViewById(R.id.img);
        final ImageView image1 = (ImageView)viewLayout.findViewById(R.id.img1);

        TextView text  = (TextView)viewLayout.findViewById(R.id.image_tips);
        TextView text1  = (TextView)viewLayout.findViewById(R.id.image_tips1);

        LinearLayout withtips = (LinearLayout)viewLayout.findViewById(R.id.withtips);
        LinearLayout withouttips = (LinearLayout)viewLayout.findViewById(R.id.withouttips);


        if(all_image_des.get(position).equals(""))
        {

            Glide.with(_activity).load(items.get(position)).into(image1);


            text1.setBackgroundColor(ContextCompat.getColor(_activity, R.color.transparent1));
            withtips.setVisibility(View.GONE);
            withouttips.setVisibility(View.VISIBLE);

        }else{
            text1.setVisibility(View.VISIBLE);
            withtips.setVisibility(View.VISIBLE);
            withouttips.setVisibility(View.GONE);
            Glide.with(_activity).load(items.get(position)).into(image);

            text.setText(all_image_des.get(position));
        }




        if(items.get(position).equals(""))
        {
            withtips.setVisibility(View.GONE);
            withouttips.setVisibility(View.GONE);
        }



        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }

}
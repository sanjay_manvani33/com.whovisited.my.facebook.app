package com.profile.stalkers.purchase;

/**
 * Created by abcd on 12/22/2017. pradip
 */

public enum PurchaseItem {

    SKU_ITEM_00,
    SKU_ITEM_11,
    SKU_ITEM_12, SKU_ITEM_13, SKU_ITEM_PEOPLE_ALL, SKU_ITEM_PEOPLE_15, SKU_ITEM_PEOPLE_25, SKU_ITEM_REMOVE_AD,base64EncodedPublicKey,


    //Admire activity purchase
    STALKER_200,STALKER_100,STALKER_50,STALKER_25,STALKER_10,ADMIRE_200,ADMIRE_100,ADMIRE_50,ADMIRE_25,ADMIRE_10;

//    private String SKU_ITEM_00 = "buy_35_stalkers"; // get profile
//    private String SKU_ITEM_11 = "buy_50_stalkers"; // get more
//    private String SKU_ITEM_12 = "buy_100_stalkers"; // get more
//    private String SKU_ITEM_13 = "buy_200_stalkers";// get more
//
//    private String SKU_ITEM_PEOPLE = "strangers_15"; // get more
//    private String SKU_ITEM_PEOPLE_25 = "strangers_25"; // get more
//    private String SKU_ITEM_PEOPLE_50 = "strangers_50";

//    private String SKU_ITEM_REMOVE_AD = "buy_remove_ad";
//    private String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlwlLiaXuitw4WU+Zw//KP4W0ggXlKq7OzfrqrnG95r0fowpZ/qj4IIBT7sERK97oDUEhewo5vWn3y/3hx7L4jy9v/8MkNazqsTbHgwbwkDEfVs/lKNXG6b+GsIM5NU5jMiu4HKW10h34ehMynwCwtr2tUbjyXrlFbMd28J66P2PDOx/5AOlrMWquQvR9ac0ic2yAPqepCTNU36lWERE10WRHUfus41iiwle3Llno8tyRFyOTdHB60iLeL2sUA8mPMUI6VQSFTcH1NXBDUhHqRwFEiVoIBqI93itWHRAihHgQ5bGSQ0fNRX1cH16p5sh7X7LVBZAa9Qe1z6MP8DzklwIDAQAB";
//    private String base64EncodedPublicKey;


    //Admire Activity
//    private String ADMIRE_10 = "buy_10_admires";
//    private String ADMIRE_25 = "buy_25_admires";
//    private String ADMIRE_50 = "buy_50_admires";
//    private String ADMIRE_100 = "buy_100_admires";
//    private String ADMIRE_200 = "buy_200_admires";
//
//    private String STALKER_10 = "buy_10_profile_stalkers";
//    private String STALKER_25 = "buy_25_profile_stalkers";
//    private String STALKER_50 = "buy_50_profile_stalkers";
//    private String STALKER_100 = "buy_100_profile_stalkers";
//    private String STALKER_200 = "buy_200_profile_stalkers";


    public String toString() {
        switch (this) {
            case SKU_ITEM_00:
                return "buy_35_stalkers";
            case SKU_ITEM_11:
                return "buy_50_stalkers";
            case SKU_ITEM_12:
                return "buy_100_stalkers";
            case SKU_ITEM_13:
                return "buy_200_stalkers";
            case SKU_ITEM_PEOPLE_ALL:
                return "strangers_50";
            case SKU_ITEM_PEOPLE_15:
                return "strangers_15";
            case SKU_ITEM_PEOPLE_25:
                return "strangers_25";
            case SKU_ITEM_REMOVE_AD:
                return "buy_remove_ad";
            case base64EncodedPublicKey:
                return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlwlLiaXuitw4WU+Zw//KP4W0ggXlKq7OzfrqrnG95r0fowpZ/qj4IIBT7sERK97oDUEhewo5vWn3y/3hx7L4jy9v/8MkNazqsTbHgwbwkDEfVs/lKNXG6b+GsIM5NU5jMiu4HKW10h34ehMynwCwtr2tUbjyXrlFbMd28J66P2PDOx/5AOlrMWquQvR9ac0ic2yAPqepCTNU36lWERE10WRHUfus41iiwle3Llno8tyRFyOTdHB60iLeL2sUA8mPMUI6VQSFTcH1NXBDUhHqRwFEiVoIBqI93itWHRAihHgQ5bGSQ0fNRX1cH16p5sh7X7LVBZAa9Qe1z6MP8DzklwIDAQAB";
            case STALKER_200:
                return"buy_200_profile_stalkers";
            case STALKER_100:
                return "buy_100_profile_stalkers";
            case STALKER_50:
                return "buy_50_profile_stalkers";
            case STALKER_25:
                return "buy_25_profile_stalkers";
            case STALKER_10:
                return "buy_10_profile_stalkers";
            case ADMIRE_200:
                return "buy_200_admires";
            case ADMIRE_100:
                return "buy_100_admires";
            case ADMIRE_50:
                return "buy_50_admires";
            case ADMIRE_25:
                return "buy_25_admires";
            case ADMIRE_10:
                return "buy_10_admires";


        }
        return null;
    }
}
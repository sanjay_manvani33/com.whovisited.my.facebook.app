package com.profile.stalkers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.profile.stalkers.utils.AnalyticsApplication;
import com.profile.stalkers.utils.SharePreference;
import com.profile.stalkers.utils.SharePreferencePermanent;
import com.profile.stalkers.utils.Utils;
import com.splunk.mint.Mint;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Arrays;

import me.relex.circleindicator.CircleIndicator;

import static com.profile.stalkers.model.dataStrings.EMAIL;
import static com.profile.stalkers.model.dataStrings.NAME;
import static com.profile.stalkers.model.dataStrings.PLAY_STORE;
import static com.profile.stalkers.model.dataStrings.android1;
import static com.profile.stalkers.model.dataStrings.fields;


public class LoginPagerScreen extends Activity implements View.OnClickListener{

    Dialog mDialog;

    boolean isclosed = false;

    String device_id = "";
    String androidOS = "";
    StringBuilder builder;

    RelativeLayout mRelativeLayout_fb;
    public static final int[] mThumbIds = {
            R.drawable.img_login_pager1,
            R.drawable.img_login_pager2,
            R.drawable.img_login_pager3};

    public static final String[] mainDesc = new String[3];
    ViewPager mViewPager;
    SharePreference mSharePreference;
    SharePreferencePermanent mSharePreferencePermanent;
    LoginButton mLoginButton;
    CallbackManager callbackManager;
    ProgressBar mProgressDialogFB;

    static {
        System.loadLibrary("keys");
    }

    public native String keyid();
    public native String getimagel();
    public native String getimagel1();

    private Tracker mTracker;
    CircleIndicator mCirclePageIndicator;

    boolean isCallActivity = false;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_pager);

        isCallActivity = false;

        mSharePreference = new SharePreference(getApplicationContext());
        mSharePreferencePermanent = new SharePreferencePermanent(getApplicationContext());
        mSharePreferencePermanent.setAppLanguage(mSharePreferencePermanent.getAppLanguage());
        Utils.changeAppLanguage(getApplicationContext(), mSharePreferencePermanent.getAppLanguage());

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();


        Mint.initAndStartSession(this.getApplication(), "17510f4c");

        if (!Utils.isInternetConnected(getApplicationContext())) {
            ShowDialogInternet();
        }

        mViewPager = (ViewPager) findViewById(R.id.activity_main_viewPager);
        mCirclePageIndicator=(CircleIndicator)findViewById(R.id.indicator);
        mProgressDialogFB = (ProgressBar) findViewById(R.id.progressBarFB);
        mRelativeLayout_fb = (RelativeLayout) findViewById(R.id.relative_layout_login);

        mProgressDialogFB.setVisibility(View.GONE);

        mainDesc[0] = getResources().getString(R.string.login_msg_1);
        mainDesc[1] = getResources().getString(R.string.login_msg_2);
        mainDesc[2] = getResources().getString(R.string.login_msg_3);



        setPagerAdapter();
        deviceInfo();
        facebook_configue();

        mTracker.setScreenName("Image~" + NAME);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        mRelativeLayout_fb.setOnClickListener(this);


        mSharePreference = new SharePreference(this);
        mSharePreference.setOS(builder.toString());
        mSharePreference.setDeviceId(getDeviceName());


        final String appName = getPackageName();

        mSharePreference.setAppUrl(PLAY_STORE + appName);

        if(mSharePreference.getToken().equals("")) {}
        if (!mSharePreference.getmStringUserId().equals("")) {
            Intent mIntent = new Intent(LoginPagerScreen.this, MySplash.class);
            startActivity(mIntent);
            finish();
        }


    }
    public  void facebook_configue(){
        mLoginButton=new LoginButton(this);
        callbackManager = CallbackManager.Factory.create();

        mLoginButton.setLoginBehavior(LoginBehavior.WEB_ONLY);
        mLoginButton.setReadPermissions(Arrays.asList("email,public_profile"));
        mLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                LoginPagerScreen.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    }
                });

                updateUI();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(final FacebookException exception) {
            }
        });
        new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    final Profile oldProfile,
                    final Profile currentProfile) {
                updateUI();
            }
        };
        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
            }
        };
    }
    public void  setPagerAdapter(){
        mViewPager.setAdapter(new MyAdapter());
        mCirclePageIndicator.setViewPager(mViewPager);
    }
    public void deviceInfo(){
        device_id = Secure.getString(getContentResolver(), Secure.ANDROID_ID);
        androidOS = Build.VERSION.RELEASE;

        isclosed = false;


        builder = new StringBuilder();
        builder.append(android1).append(Build.VERSION.RELEASE);

        Field[] fields = Build.VERSION_CODES.class.getFields();
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append(",").append(fieldName);
            }
        }
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public void relative_layout_fb_click(){
        if (!Utils.isInternetConnected(getApplicationContext())) {
            Utils.SetDiolog(LoginPagerScreen.this, getResources().getString(R.string.no_connectivity_msg));
        } else {
            if(AccessToken.getCurrentAccessToken()!=null) {
                if(!isCallActivity) {
                    isCallActivity=true;
                    mSharePreference.setImage(getimagel() + mSharePreference.getmStringUserId() + getimagel1());
                    Intent mIntent2 = new Intent(LoginPagerScreen.this, MainActivity.class);
                    startActivity(mIntent2);
                    finish();
                }
            }else{
                mLoginButton.performClick();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.relative_layout_login:
                //method of login

                relative_layout_fb_click();
                break;

            default:
                break;
        }
    }

    private class MyAdapter extends PagerAdapter {
       @Override
       public Object instantiateItem(ViewGroup container, int position) {
           View page =
                   getLayoutInflater().inflate(R.layout.layout_view_pager, container, false);

           ImageView iv = (ImageView) page.findViewById(R.id.view_pager_imageview);

           TextView mTextViewDesc = (TextView) page.findViewById(R.id.text_desc);
           iv.setImageResource(mThumbIds[position]);
           Utils.SetFont(mTextViewDesc, LoginPagerScreen.this);


           mTextViewDesc.setText(mainDesc[position]);

           container.addView(page);

           return (page);
       }

       @Override
       public void destroyItem(ViewGroup container, int position,
                               Object object) {
           container.removeView((View) object);
       }

       @Override
       public int getCount() {
           return mThumbIds.length;
       }


       @Override
       public boolean isViewFromObject(View view, Object object) {
           return (view == object);
       }
   }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);


        callbackManager.onActivityResult(requestCode, resultCode, data);

    }


private void updateUI() {
    final Profile profile = Profile.getCurrentProfile();
    if (profile != null) {
        mSharePreference.setmStringFacebookToken(AccessToken.getCurrentAccessToken().getToken().toString());
        GraphRequest request = GraphRequest.newMeRequest(
                AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback()
                {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response)
                    {
                        LoginPagerScreen.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mProgressDialogFB.setVisibility(View.GONE);
                            }
                        });
                        try {
                            if(object!=null)
                            {
                                if(!object.getString(EMAIL).toString().equals(""))
                                {
                                    mSharePreference.setmStringUserEmail(object.getString(EMAIL));
                                    mSharePreference.setmStringUserId(profile.getId());
                                    mSharePreference.setmStringUserName(profile.getName());
                                    if(!isCallActivity) {
                                        isCallActivity=true;
                                        mSharePreference.setImage(getimagel() + mSharePreference.getmStringUserId() + getimagel1());
                                        Intent mIntent2 = new Intent(LoginPagerScreen.this, MainActivity.class);
                                        startActivity(mIntent2);
                                        finish();
                                    }
                                }
                                else
                                {
                                    mSharePreference.setmStringUserEmail("");
                                    mSharePreference.setmStringUserId(profile.getId());
                                    mSharePreference.setmStringUserName(profile.getName());
                                    if(!isCallActivity) {
                                        isCallActivity = true;
                                        mSharePreference.setImage(getimagel() + mSharePreference.getmStringUserId() + getimagel1());
                                        Intent mIntent2 = new Intent(LoginPagerScreen.this, MainActivity.class);
                                        startActivity(mIntent2);
                                        finish();
                                    }
                                }
                            }

                        } catch (JSONException e) {

                            mSharePreference.setmStringUserEmail("");
                            mSharePreference.setmStringUserId(profile.getId());
                            mSharePreference.setmStringUserName(profile.getName());
                            mSharePreference.setImage(getimagel() + mSharePreference.getmStringUserId() + getimagel1());
                            if(!isCallActivity) {
                                isCallActivity = true;
                                Intent mIntent2 = new Intent(LoginPagerScreen.this, MainActivity.class);
                                startActivity(mIntent2);
                                finish();
                            }

                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString(fields, EMAIL);
        request.setParameters(parameters);
        request.executeAsync();

    } else {
    }
}
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mProgressDialogFB.getVisibility() == View.VISIBLE)
            mProgressDialogFB.setVisibility(View.GONE);
    }
    public void ShowDialogInternet() {

        mDialog = new Dialog(LoginPagerScreen.this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.dialog_internet);

        mDialog.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mDialog.dismiss();
                finish();
            }
        });
        mDialog.show();
    }
}

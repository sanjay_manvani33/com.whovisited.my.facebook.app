package com.profile.stalkers;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.profile.stalkers.utils.AsyncRequest;
import com.profile.stalkers.utils.SharePreference;
import com.profile.stalkers.utils.Utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.profile.stalkers.model.dataStrings.MARKET_URL;
import static com.profile.stalkers.model.dataStrings.PLAY_STORE;
import static com.profile.stalkers.model.dataStrings.d_text;
import static com.profile.stalkers.model.dataStrings.h_text;
import static com.profile.stalkers.model.dataStrings.please_wait;
import static com.profile.stalkers.model.dataStrings.version_new;

public class MySplash extends Activity  implements AsyncRequest.OnAsyncRequestComplete{

    SharePreference mSharePreference;
    ProgressDialog mProgressDialog;
    ArrayList<NameValuePair> params;
    String version = "";
    Thread splashTread;
    protected boolean _active = true;
    protected int _splashTime = 3000;
    String versionapp = "";
    PackageInfo info = null;
    Dialog mDialog;

    static {
        System.loadLibrary("keys");
    }

    public String localtime(String utcTime) {
        String formattedDate = "";
        if (utcTime != null) {
            if (utcTime.length() > 0) {
                long unixSeconds = (Double.valueOf(utcTime)).longValue();

                Date date = new Date(unixSeconds * 1000L); // 1000 is to convert seconds to milliseconds
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // the format of your date
                formattedDate = sdf.format(date);
            }
        }
        return formattedDate;
    }

    public String printDifference(Date startDate, Date endDate) {

        String time = "";

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        if (elapsedSeconds < 0)
            elapsedSeconds = 1;

        if (elapsedDays > 0) {
            time = elapsedDays + " d";
        } else if (elapsedHours > 0) {
            time = elapsedHours + " h";
        } else if (elapsedMinutes > 0)
            time = elapsedMinutes + " m";
        else
            time = elapsedSeconds + " s";
        return time;

    }

    public native String getVersionService();

    public native String getVersion();

    public native String getparam1();

    public native String getparam1Value();

    public native String getparam2();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_mysplash);

        mSharePreference =  new SharePreference(this);
        mProgressDialog  =  new ProgressDialog(MySplash.this);

        mProgressDialog.setMessage(please_wait);


       getAppInfo();
        waitTimer();

    }
    public void getAppInfo(){
        PackageManager manager = getPackageManager();

        try {
            info = manager.getPackageInfo(
                    getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionapp = info.versionName;
    }

     public void waitTimer(){
         splashTread = new Thread() {
             @Override
             public void run() {
                 try {
                     int waited = 0;
                     while (_active && (waited < _splashTime)) {
                         sleep(100);
                         if (_active) {
                             waited += 100;
                         }
                     }
                 } catch (InterruptedException e) {
                     e.toString();
                 } finally {

                     MySplash.this.runOnUiThread(new Runnable() {
                         @Override
                         public void run() {

                             params = getParams();
                             AsyncRequest getPosts =
                                     new AsyncRequest(MySplash.this, AsyncRequest.POSTURLCONNECTION, params);
                             getPosts.execute(getVersionService());
                             getPosts.pDialog.dismiss();
                         }
                     });

                 }
             }
         };
         long unixTime = System.currentTimeMillis() / 1000L;

         if (mSharePreference.getTime() == 0) {
             mSharePreference.setTime(unixTime);
         }
         String time = "";

         //milliseconds
         String dateTime = localtime(String.valueOf(mSharePreference.getTime()));

         SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         String currenttime = sdf.format(new Date());

         try {

             Date date1 = sdf.parse(dateTime);
             Date date2 = sdf.parse(currenttime);
             time = printDifference(date1, date2);

         } catch (ParseException e) {
             e.printStackTrace();
         }
         String[] spitStr = time.split(" ");


         int hours = Integer.parseInt(spitStr[0]);
         if (hours >= 23 && spitStr[1].equals(h_text) && !mSharePreference.isTime24()) {
             mSharePreference.setTime24(true);
         } else if (spitStr[1].equals(d_text) && !mSharePreference.isTime24()) {
             mSharePreference.setTime24(true);
         }
         if (!Utils.isInternetConnected(getApplicationContext()))
         {
             ShowDialogInternet();
         }
         else
         {
             splashTread.start();

         }
     }

    @Override
    public void asyncResponse(String response, String label) {


        if (response == null) {
            Intent i = new Intent(MySplash.this, MainActivity.class);
            startActivity(i);
            finish();
        } else {
            try {
                JSONObject jObject = new JSONObject(response);
                version = jObject.getString(version_new);

            } catch (JSONException e) {
                Intent i = new Intent(MySplash.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        }
        if (!version.equals("")) {
            float versionnumber = Float.valueOf(versionapp);
            float versionnumberserver = Float.valueOf(version);
            if (versionnumber >= versionnumberserver) {
                Intent i = new Intent();
                i.setClass(MySplash.this, MainActivity.class);
                startActivity(i);
                finish();
            } else {
                if (!(MySplash.this).isFinishing()) {
                    MySplash.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ShowDialogUpdateApp();
                        }
                    });
                }
            }
        } else {
            Intent i = new Intent();
            i.setClass(MySplash.this, MainActivity.class);
            startActivity(i);
            finish();
        }
    }


    public void ShowDialogUpdateApp() {

        final Dialog mDialogExit = new Dialog(MySplash.this);
        mDialogExit.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogExit.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialogExit.setContentView(R.layout.dialog_update);


        mDialogExit.findViewById(R.id.button_update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String appName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_URL + appName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(PLAY_STORE + appName)));

                }
                if(mDialogExit != null){


                    mDialogExit.dismiss();
                }

                else{
                }

                finish();
            }
        });

        mDialogExit.show();
    }

    public void ShowDialogInternet() {

        mDialog = new Dialog(MySplash.this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setCancelable(false);
        mDialog.setContentView(R.layout.dialog_internet);


        mDialog.findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mDialog != null)
                mDialog.dismiss();
                finish();


            }
        });
    }

    private ArrayList<NameValuePair> getParams() {
        // define and ArrayList whose elements are of type NameValuePair
        ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(getparam1(), getparam1Value()));
        params.add(new BasicNameValuePair(getparam2(), getPackageName()));
        return params;
    }

}
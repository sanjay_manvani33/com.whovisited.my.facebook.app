package com.profile.stalkers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.profile.stalkers.adapters.DataAdapterAdmire;
import com.profile.stalkers.adapters.DataAdapterStalker;
import com.profile.stalkers.model.Viewers;
import com.profile.stalkers.model.dataStrings;
import com.profile.stalkers.purchase.IabHelper;
import com.profile.stalkers.purchase.IabResult;
import com.profile.stalkers.purchase.Inventory;
import com.profile.stalkers.purchase.Purchase;
import com.profile.stalkers.purchase.PurchaseItem;
import com.profile.stalkers.utils.AnalyticsApplication;
import com.profile.stalkers.utils.GridSpacingItemDecoration;
import com.profile.stalkers.utils.RecyclerItemClickListener;
import com.profile.stalkers.utils.SharePreference;
import com.profile.stalkers.utils.SharePreferencePermanent;
import com.profile.stalkers.utils.TAGS;
import com.profile.stalkers.utils.Utils;
import com.splunk.mint.Mint;

import java.util.ArrayList;
import java.util.List;

public class AdmireActivity extends AppCompatActivity implements  View.OnClickListener {

    List<String> fbIds;


    SharePreference mSharePreference;
    SharePreferencePermanent mSharePreferencePermanentpurchasefriend;
    SharePreferencePermanent mSharePreferencePermanentpurchasestalkers;
    SharePreferencePermanent mSharePreferencePermanentadd;

    IabHelper mHelper;

    String name = "AdmireActivityy";


    List<Integer> list20;
    List<Integer> list50;
    List<Integer> list100;
    List<Integer> list200;


    private static final int RC_REQUEST = 10001;
    boolean isInAppBilling = false;
    private boolean setup_successed = false;
    private String payload = "";

    Button mButtonAdmire;
    Button mButtonStalkers;

    ImageView mImageViewMenu;

    TextView mTextViewTitle;

    float x = 0;
    float y = 0;


    Dialog mDialogPopup;
    Dialog mDialog;

    LinearLayout mLinearLayoutButtons;
    LinearLayout mLinearLayoutCart;

    RelativeLayout mRelativeLayoutMore;


    AdView mAdView;

    AdRequest adRequest;

    RecyclerView mRecyclerViewAdmire;
    RecyclerView mRecyclerViewStalkers;

    DataAdapterAdmire mDataAdapterAdmire;
    DataAdapterStalker mDataAdapterStalker;

    CounterClass mtimerAds;

    InterstitialAd interstitial;

    boolean activityDestroyed = false;
    public    boolean isOpenScreen = true;

    public native String image4();

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.main_menu:
                //method of login
                finish();
                break;

            case R.id.cart:

                if (mRecyclerViewAdmire.isShown()) {
                    dialog_admire_purchase();
                } else {
                    dialog_admire_stalkers_purchase();
                }

                break;

            case R.id.button_whoview:

               click_on_admire();

                break;

            case R.id.button_people:

                click_on_stranger();

                break;
            default:
                break;
        }
    }

    public void click_on_admire(){
        mTextViewTitle.setText(getResources().getString(R.string.profileadmiresmall));


        mButtonAdmire.setBackgroundResource(R.drawable.img_selected);
        mButtonAdmire.setTextColor(getResources().getColor(R.color.white));
        mButtonStalkers.setBackgroundResource(R.drawable.img_unselected);
        mButtonStalkers.setTextColor(getResources().getColor(R.color.blue_theme_color));

        mRecyclerViewAdmire.setVisibility(View.VISIBLE);
        mRecyclerViewStalkers.setVisibility(View.GONE);
    }

    public void click_on_stranger(){
        mTextViewTitle.setText(getResources().getString(R.string.profilestalkersmall));
        mButtonStalkers.setBackgroundResource(R.drawable.img_selected);
        mButtonStalkers.setTextColor(getResources().getColor(R.color.white));
        mButtonAdmire.setBackgroundResource(R.drawable.img_unselected);
        mButtonAdmire.setTextColor(getResources().getColor(R.color.blue_theme_color));

        mRecyclerViewAdmire.setVisibility(View.GONE);
        mRecyclerViewStalkers.setVisibility(View.VISIBLE);
    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {

        }


        @Override
        public void onTick(long millisUntilFinished) {

            if (!mSharePreferencePermanentadd.isAds() && !activityDestroyed && isOpenScreen) {
//                interstitial.loadAd(adRequest);
//                interstitial.show();
            }
        }
    }

    private Tracker mTracker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Mint.initAndStartSession(this.getApplication(), "17510f4c");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_admire);

        getSupportActionBar().hide();

        mRelativeLayoutMore = (RelativeLayout) findViewById(R.id.loadmore);

        mLinearLayoutButtons = (LinearLayout) findViewById(R.id.linear_menu);
        mLinearLayoutCart = (LinearLayout) findViewById(R.id.cart);

        mButtonAdmire = (Button) findViewById(R.id.button_whoview);
        mButtonStalkers = (Button) findViewById(R.id.button_people);

        mTextViewTitle = (TextView) findViewById(R.id.text_tips);

        mRecyclerViewAdmire = (RecyclerView) findViewById(R.id.card_recycler_view_users);
        mRecyclerViewStalkers = (RecyclerView) findViewById(R.id.card_recycler_view_stranger);

        mImageViewMenu = (ImageView) findViewById(R.id.main_menu);

        mTextViewTitle.setText(getResources().getString(R.string.profileadmiresmall));

        activityDestroyed = false;
        isOpenScreen = true;

       // advertisement();
      //  timerads();

        x = 0;
        y = 0;
        int spanCount = 1; // 3 columns
        int spacing = 8; // 50px
        boolean includeEdge = true;

        mSharePreference = new SharePreference(this);
        mSharePreferencePermanentadd = new SharePreferencePermanent(this);
        mSharePreferencePermanentpurchasefriend = new SharePreferencePermanent(this);
        mSharePreferencePermanentpurchasestalkers = new SharePreferencePermanent(this);

        fbIds = new ArrayList<String>();



        SetDataList();

        mDataAdapterAdmire = new DataAdapterAdmire(this, TAGS.mViewersNew);
        mDataAdapterStalker = new DataAdapterStalker(this, TAGS.mStalkerUsers);

        initIABSetup();

        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        RecyclerView.LayoutManager layoutManagerStrangers = new GridLayoutManager(getApplicationContext(), 1);

        mRecyclerViewAdmire.setLayoutManager(layoutManager);

        mRecyclerViewAdmire.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

        mRecyclerViewStalkers.setLayoutManager(layoutManagerStrangers);
        mRecyclerViewStalkers.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));


        mRecyclerViewAdmire.setAdapter(mDataAdapterAdmire);
        mRecyclerViewStalkers.setAdapter(mDataAdapterStalker);

        if (!mSharePreference.isTime24()) {
            int size = 10;
            int size_Stalker = 10;
            if(TAGS.mViewersNew.size()<10)
                size=TAGS.mViewersNew.size();
            for (int i = 0; i < size; i++) {
                TAGS.mViewersNew.get(i).setShow(true);

            }
            if(TAGS.mStalkerUsers.size()<10){
                size_Stalker = TAGS.mStalkerUsers.size();
            }
            for(int i = 0;i <size_Stalker;i++){
                TAGS.mStalkerUsers.get(i).setShow(true);
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDataAdapterAdmire.notifyDataSetChanged();
                    mDataAdapterStalker.notifyDataSetChanged();
                }
            });
        }
        if (mSharePreferencePermanentpurchasefriend.isAdmire10()) {
            int size1 = 10;
            if(TAGS.mViewersNew.size()<10)
                size1=TAGS.mViewersNew.size();

            for (int i = 0; i < size1; i++) {
                TAGS.mViewersNew.get(i).setShow(true);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDataAdapterAdmire.notifyDataSetChanged();
                }
            });
        }
        if (mSharePreferencePermanentpurchasestalkers.isStalkers10()) {

            int size2 = 10;
            if(TAGS.mViewersNew.size()<10)
                size2=TAGS.mViewersNew.size();

            for (int i = 0; i < size2; i++) {
                TAGS.mStalkerUsers.get(i).setShow(true);
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDataAdapterStalker.notifyDataSetChanged();
                }
            });
        }


        ShowDialogPop();


        mButtonAdmire.setBackgroundResource(R.drawable.img_selected);
        mButtonStalkers.setBackgroundResource(R.drawable.img_unselected);
        mButtonStalkers.setTextColor(getResources().getColor(R.color.blue_theme_color));


        mRecyclerViewAdmire.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub

                x = event.getX();
                y = event.getY() - 50;

                return false;
            }
        });


        mRecyclerViewAdmire.addOnItemTouchListener(new RecyclerItemClickListener(AdmireActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        friendsListClick(position);

                    }
                })
        );

        mRecyclerViewStalkers.addOnItemTouchListener(
                new RecyclerItemClickListener(AdmireActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        StalkersListClick(position);
                    }
                })
        );


        mImageViewMenu.setOnClickListener(this);
        mLinearLayoutCart.setOnClickListener(this);
        mButtonAdmire.setOnClickListener(this);
        mButtonStalkers.setOnClickListener(this);
    }

    public void SetDataList(){
        TAGS.mViewersNew.clear();
        if (TAGS.mViewersMainScreen.size() > 20 && TAGS.mViewersMainScreen.size() <= 50) {
            list50 = Utils.getlist(20, TAGS.mViewersMainScreen.size() - 1);
        } else if (TAGS.mViewersMainScreen.size() > 50 && TAGS.mViewersMainScreen.size() < 100) {
            list50 = Utils.getlist(20, 49);
            list100 = Utils.getlist(50, TAGS.mViewersMainScreen.size() - 1);
        } else if (TAGS.mViewersMainScreen.size() >= 100 && TAGS.mViewersMainScreen.size() <= 200) {
            list50 = Utils.getlist(20, 49);
            list100 = Utils.getlist(50, 99);
            list200 = Utils.getlist(100, TAGS.mViewersMainScreen.size() - 1);
        }

        if (TAGS.mViewersMainScreen.size() > 20)
            list20 = Utils.getlist(0, 19);
        else
            list20 = Utils.getlist(0, TAGS.mViewersMainScreen.size() - 1);

        int size_MainScreen;
        size_MainScreen = 20;

        if (TAGS.mViewersMainScreen.size() <20) {

            size_MainScreen = TAGS.mViewersMainScreen.size();
        }

        for(int j=0;j<size_MainScreen;j++){
//        for(int j=0;j<list20.size();j++){
                TAGS.mViewersMainScreen.get(list20.get(j)).setViewid(j);
                TAGS.mViewersNew.add(TAGS.mViewersMainScreen.get(list20.get(j)));
        }

        if (TAGS.mStalkerUsers.size() > 20 && TAGS.mStalkerUsers.size() <= 50 && list50!=null) {
            for (int j = 0; j < list50.size(); j++) {
                TAGS.mViewersMainScreen.get(list50.get(j)).setViewid(j + 20);
                TAGS.mViewersNew.add(TAGS.mViewersMainScreen.get(list50.get(j)));
            }
        }

        if (TAGS.mStalkerUsers.size() > 50 && list50!=null) {
            for (int j = 0; j < list50.size(); j++) {
                TAGS.mViewersMainScreen.get(list50.get(j)).setViewid(j + 20);
                TAGS.mViewersNew.add(TAGS.mViewersMainScreen.get(list50.get(j)));
            }
        }
//        if (TAGS.mStalkerUsers.size() > 100) {
//            for (int j = 0; j < list100.size(); j++) {
//                TAGS.mViewersMainScreen.get(list100.get(j)).setViewid(j + 50);
//                TAGS.mViewersNew.add(TAGS.mViewersMainScreen.get(list100.get(j)));
//            }
//
//        }

        if (TAGS.mStalkerUsers.size() >= 100 && TAGS.mStalkerUsers.size() <= 200 && list100!=null) {

            for (int j = 0; j < list100.size(); j++) {
                TAGS.mViewersMainScreen.get(list100.get(j)).setViewid(j + 50);
                TAGS.mViewersNew.add(TAGS.mViewersMainScreen.get(list100.get(j)));
            }
            for (int j = 0; j < list200.size(); j++) {
                TAGS.mViewersMainScreen.get(list200.get(j)).setViewid(j + 100);
                TAGS.mViewersNew.add(TAGS.mViewersMainScreen.get(list200.get(j)));
            }
        }
        int size_stalkerUsers, size_viewerNew;
        size_stalkerUsers = 10;
        size_viewerNew = 10;
        if (TAGS.mStalkerUsers.size() < 10) {
            size_stalkerUsers = TAGS.mStalkerUsers.size();
        }
        if (TAGS.mViewersNew.size() < 10) {
            size_viewerNew = TAGS.mViewersNew.size();
        }
        if (mSharePreference.isTime24()) {
            for (int i = 0; i < size_stalkerUsers; i++) {
                TAGS.mStalkerUsers.get(i).setShow(false);
            }
            for (int i = 0; i < size_viewerNew; i++) {
                TAGS.mViewersNew.get(i).setShow(false);
            }
        }
    }

    public void friendsListClick(int position){

        if (position < 10) {
            if (TAGS.mViewersNew.get(position).isShow()) {
                dialog_user(R.style.thirdanimation, TAGS.mViewersNew.get(position));
            } else {
                dialog_admire_purchase();
            }
        } else if (position >= 10 && position < 25) {
            if (!mSharePreferencePermanentpurchasefriend.isAdmire25()) {
                dialog_admire_purchase();
            } else {
                dialog_user(R.style.thirdanimation, TAGS.mViewersNew.get(position));
            }
        } else if (position >= 25 && position < 50) {
            if (!mSharePreferencePermanentpurchasefriend.isAdmire50()) {
                dialog_admire_purchase();
            } else {
                dialog_user(R.style.thirdanimation, TAGS.mViewersNew.get(position));
            }
        } else if (position >= 50 && position < 100) {
            if (!mSharePreferencePermanentpurchasefriend.isAdmire100()) {
                dialog_admire_purchase();
            } else {
                dialog_user(R.style.thirdanimation, TAGS.mViewersNew.get(position));
            }
        } else {
            if (!mSharePreferencePermanentpurchasefriend.isAdmire200()) {
                dialog_admire_purchase();
            } else {
                dialog_user(R.style.thirdanimation, TAGS.mViewersNew.get(position));
            }
        }

    }

    public void StalkersListClick(int position){
        if (position < 10) {
            if (TAGS.mStalkerUsers.get(position).isShow()) {
                dialog_user(R.style.thirdanimation, TAGS.mStalkerUsers.get(position));
            } else {
                dialog_admire_stalkers_purchase();
            }
        } else if (position >= 10 && position < 25) {
            if (!mSharePreferencePermanentpurchasestalkers.isStalkers25()) {
                dialog_admire_stalkers_purchase();
            } else {
                dialog_user(R.style.thirdanimation, TAGS.mStalkerUsers.get(position));
            }
        } else if (position >= 25 && position < 50) {
            if (!mSharePreferencePermanentpurchasestalkers.isStalkers50()) {
                dialog_admire_stalkers_purchase();
            } else {
                dialog_user(R.style.thirdanimation, TAGS.mStalkerUsers.get(position));
            }
        } else if (position >= 50 && position < 100) {
            if (!mSharePreferencePermanentpurchasestalkers.isStalkers100()) {
                dialog_admire_stalkers_purchase();
            } else {
                dialog_user(R.style.thirdanimation, TAGS.mStalkerUsers.get(position));
            }
        } else {
            if (!mSharePreferencePermanentpurchasestalkers.isStalkers200()) {
                dialog_admire_stalkers_purchase();
            } else {
                dialog_user(R.style.thirdanimation, TAGS.mStalkerUsers.get(position));
            }
        }
    }

    public void advertisement(){
        adRequest = new AdRequest.Builder().build();
        mAdView = (AdView) findViewById(R.id.adView_id);
        mAdView.loadAd(adRequest);
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId(getResources().getString(R.string.adUnitId_intersitial));
    }

    public void timerads(){
        mtimerAds = new CounterClass(100000000, 20000);
        mtimerAds.start();
    }

    private void dialog_user(int stylename, Viewers mViewers) {

        mDialog = new Dialog(AdmireActivity.this, stylename);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mDialog.setContentView(R.layout.dialog_user);
        LinearLayout mLayout = (LinearLayout) mDialog.findViewById(R.id.layout);
        LinearLayout mLayoutMutual = (LinearLayout) mDialog.findViewById(R.id.layout_mutual);

        ImageView mImageView = (ImageView) mDialog.findViewById(R.id.image);
        ImageView mImageView1 = (ImageView) mDialog.findViewById(R.id.image1);
        TextView mTextView = (TextView) mDialog.findViewById(R.id.text);
        TextView mTextViewMutual = (TextView) mDialog.findViewById(R.id.text1);
        TextView mTextViewStranger = (TextView) mDialog.findViewById(R.id.text_mutual);


        if (mViewers.isShowmutual()) {
            mLayout.setVisibility(View.GONE);
            mLayoutMutual.setVisibility(View.VISIBLE);
            mTextViewMutual.setText(mViewers.getMulualname() + " is Mutual Friend");
            mTextViewMutual.setText(mTextViewMutual.getText().toString().toUpperCase());

            String str =mViewers.getName();
            String[] splitStr = str.split("\\s+");
//            viewHolder.mTextViewItemName.setText(index+". "+splitStr[0]);
            mTextViewStranger.setText(splitStr[0]);
            mTextViewStranger.setText(mTextViewStranger.getText().toString().toUpperCase());
            Glide.with(AdmireActivity.this).
                    load(mViewers.getImage() + image4()).
                    placeholder(R.drawable.ic_blur).
                    into(mImageView1);
        } else {
            mLayout.setVisibility(View.VISIBLE);
            mLayoutMutual.setVisibility(View.GONE);
            Glide.with(AdmireActivity.this).
                    load(mViewers.getImage() + image4()).
                    placeholder(R.drawable.ic_blur).
                    into(mImageView);
        }

        String str =mViewers.getName();
        String[] splitStr = str.split("\\s+");

        mTextView.setText(splitStr[0]);
        mTextView.setText(mTextView.getText().toString().toUpperCase());
        mDialog.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        isOpenScreen = false;
        activityDestroyed = true;
       // mtimerAds.cancel();
        interstitial = null;
    }

 
    public void initIABSetup() {

        mHelper = new IabHelper(this, String.valueOf(PurchaseItem.base64EncodedPublicKey));
        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {

                    setup_successed = false;
                    return;
                }
                setup_successed = true;
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }


    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {

        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            if (result.isFailure() || !setup_successed) {
                return;
            }
            List<Purchase> purchases = inventory.getAllPurchases();// new
            if (purchases != null && purchases.size() > 0) {
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.ADMIRE_10))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.ADMIRE_10)));
                    if (!mSharePreferencePermanentpurchasefriend.isAdmire10()) {
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.ADMIRE_25))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.ADMIRE_25)));
                    if (!mSharePreferencePermanentpurchasefriend.isAdmire25()) {
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire25(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.ADMIRE_50))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.ADMIRE_50)));
                    if (!mSharePreferencePermanentpurchasefriend.isAdmire50()) {
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire25(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire50(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.ADMIRE_100))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.ADMIRE_100)));
                    if (!mSharePreferencePermanentpurchasefriend.isAdmire100()) {
                        mSharePreferencePermanentpurchasefriend.setAdmire100(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire25(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire50(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.ADMIRE_200))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.ADMIRE_200)));
                    if (!mSharePreference.isAdmire200()) {
                        mSharePreferencePermanentpurchasefriend.setAdmire200(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire100(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire25(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire50(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.STALKER_10))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.STALKER_10)));
                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers10()) {
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.STALKER_25))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.STALKER_25)));
                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers25()) {
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers25(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.STALKER_50))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.STALKER_50)));
                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers50()) {
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers25(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers50(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.STALKER_100))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.STALKER_100)));
                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers100()) {
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers100(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers25(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers50(true);
                    }
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.STALKER_200))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.STALKER_200)));
                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers200()) {
                        mSharePreferencePermanentpurchasestalkers.setStalkers200(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers100(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers25(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers50(true);
                    }
                }
            }
            if (purchases.size() > 0) {
                isInAppBilling = true;
            }
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (mHelper == null)
                return;

            if (result.isFailure()) {
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_auth_failed));
                return;
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.ADMIRE_10))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        for (int i = 0; i < 10; i++) {
                            TAGS.mViewersNew.get(i).setShow(true);
                        }
                        mDataAdapterAdmire.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.ADMIRE_25))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire25(true);
                        for (int i = 0; i < 25; i++) {
                            TAGS.mViewersNew.get(i).setShow(true);
                        }
                        mDataAdapterAdmire.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.ADMIRE_50))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire25(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire50(true);
                        for (int i = 0; i < 50; i++) {
                            TAGS.mViewersNew.get(i).setShow(true);
                        }
                        mDataAdapterAdmire.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.ADMIRE_100))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasefriend.setAdmire100(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire25(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire50(true);
                        for (int i = 0; i < 100; i++) {
                            TAGS.mViewersNew.get(i).setShow(true);
                        }
                        mDataAdapterAdmire.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.ADMIRE_200))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasefriend.setAdmire200(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire100(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire10(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire25(true);
                        mSharePreferencePermanentpurchasefriend.setAdmire50(true);
                        for (int i = 0; i < TAGS.mViewersNew.size(); i++) {
                            TAGS.mViewersNew.get(i).setShow(true);
                        }
                        mDataAdapterAdmire.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.STALKER_10))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        for (int i = 0; i < 10; i++) {
                            TAGS.mStalkerUsers.get(i).setShow(true);
                        }
                        mDataAdapterStalker.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.STALKER_25))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers25(true);
                        for (int i = 0; i < 25; i++) {
                            TAGS.mStalkerUsers.get(i).setShow(true);
                        }
                        mDataAdapterStalker.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.STALKER_50))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers25(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers50(true);
                        for (int i = 0; i < 50; i++) {
                            if(TAGS.mStalkerUsers!=null && TAGS.mStalkerUsers.size()>i)
                                TAGS.mStalkerUsers.get(i).setShow(true);
                        }
                        mDataAdapterStalker.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.STALKER_100))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasestalkers.setStalkers100(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers25(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers50(true);
                        for (int i = 0; i < 100; i++) {
                            TAGS.mStalkerUsers.get(i).setShow(true);
                        }
                        mDataAdapterStalker.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.STALKER_200))) {
                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSharePreferencePermanentpurchasestalkers.setStalkers10(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers25(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers50(true);
                        mSharePreferencePermanentpurchasestalkers.setStalkers200(true);
                        for (int i = 0; i < TAGS.mStalkerUsers.size(); i++) {
                            TAGS.mStalkerUsers.get(i).setShow(true);
                        }
                        mDataAdapterStalker.notifyDataSetChanged();
                    }
                });
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD))) {
                mSharePreferencePermanentadd.setAds(true);

                AdmireActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdView.setVisibility(View.GONE);
                    }
                });

            }
        }
    };

    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

		/*
         * TODO: verify that the developer payload of the purchase is correct.
		 * It will be the same one that you sent when initiating the purchase.
		 *
		 * WARNING: Locally generating a random string when starting a purchase
		 * and verifying it here might seem like a good approach, but this will
		 * fail in the case where the user purchases an item on one device and
		 * then uses your app on a different device, because on the other device
		 * you will not have access to the random string you originally
		 * generated.
		 *
		 * So a good developer payload has these characteristics:
		 *
		 * 1. If two different users purchase an item, the payload is different
		 * between them, so that one user's purchase can't be replayed to
		 * another user.
		 *
		 * 2. The payload must be such that you can verify it even when the app
		 * wasn't the one who initiated the purchase flow (so that items
		 * purchased by the user on one device work on other devices owned by
		 * the user).
		 *
		 * Using your own server to store and verify developer payloads across
		 * app installations is recommended.
		 */

        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            mHelper.queryInventoryAsync(mGotInventoryListener);
        }

    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        isOpenScreen = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityDestroyed = false;
        isOpenScreen = true;
        if (mSharePreferencePermanentadd.isAds()) {
            activityDestroyed = true;
        }
    }

    public void dialog_admire_purchase() {
        final Dialog mDialog = new Dialog(AdmireActivity.this);

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_admire_purchase);

        final ImageView mImageView_Close = (ImageView) mDialog.findViewById(R.id.purchase_dialog_admire_close);
        final TextView mTextViewTitle = (TextView) mDialog.findViewById(R.id.text_title);
        final LinearLayout mLinearLayout10 = (LinearLayout) mDialog.findViewById(R.id.layout_10);
        final LinearLayout mLinearLayout25 = (LinearLayout) mDialog.findViewById(R.id.layout_25);
        final LinearLayout mLinearLayout50 = (LinearLayout) mDialog.findViewById(R.id.layout_50);
        final LinearLayout mLinearLayout100 = (LinearLayout) mDialog.findViewById(R.id.layout_100);
        final LinearLayout mLinearLayout200 = (LinearLayout) mDialog.findViewById(R.id.layout_200);
        final LinearLayout mTextViewAds = (LinearLayout) mDialog.findViewById(R.id.layout_ads);
         View  mView_200_admire = (View)mDialog.findViewById(R.id.view_200_admire);
         View mView_100 = (View)mDialog.findViewById(R.id.view_100);
         View mView_50 = (View)mDialog.findViewById(R.id.view_50);
        View mView_25 = (View)mDialog.findViewById(R.id.view_25);
        View  mView_rate = (View)mDialog.findViewById(R.id.view_rate);
        View mView_ads = (View)mDialog.findViewById(R.id.view_ads);

        TextView mTextViewRate = (TextView)mDialog.findViewById(R.id.text_rate);
        TextView mTextView35 = (TextView)mDialog.findViewById(R.id.text_35);
        TextView mTextView50 = (TextView)mDialog.findViewById(R.id.text_50);
        TextView mTextView100 = (TextView)mDialog.findViewById(R.id.text_100);
        TextView mTextView200 = (TextView)mDialog.findViewById(R.id.text_200);

        mTextViewRate.setText(getPurchaseText(R.string.admire_10,10));
        mTextView35.setText(getPurchaseText(R.string.admire_25,25));
        mTextView50.setText(getPurchaseText(R.string.admire_50,50));
        mTextView100.setText(getPurchaseText(R.string.admire_100,100));
        mTextView200.setText(getPurchaseText(R.string.admire_200,200));

        mTextViewTitle.setText(getResources().getString(R.string.admire_store));

        if(TAGS.mViewersNew.size()>=11 && TAGS.mViewersNew.size()<= 24){
            mLinearLayout10.setVisibility(View.GONE);
            if(mSharePreference.isTime24()){
                mLinearLayout10.setVisibility(View.VISIBLE);
            }
                mTextView200.setVisibility(View.GONE);
                mTextView100.setVisibility(View.GONE);
                mTextView50.setVisibility(View.GONE);
                mView_200_admire.setVisibility(View.GONE);
                mView_100.setVisibility(View.GONE);
                mView_50.setVisibility(View.GONE);

            }

        if(TAGS.mViewersNew.size()>=25 && TAGS.mViewersNew.size()<= 50){
            mLinearLayout10.setVisibility(View.GONE);
            if(mSharePreference.isTime24()){
                mLinearLayout10.setVisibility(View.VISIBLE);
            }
            mTextView200.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);
            mView_200_admire.setVisibility(View.GONE);
            mView_100.setVisibility(View.GONE);

        }

        if(TAGS.mViewersNew.size()>=51 && TAGS.mViewersNew.size()<= 100){
            mLinearLayout10.setVisibility(View.GONE);
            mView_rate.setVisibility(View.GONE);
            if(mSharePreference.isTime24()){
                mLinearLayout10.setVisibility(View.VISIBLE);
                mView_rate.setVisibility(View.VISIBLE);
            }
            mTextView200.setVisibility(View.GONE);
            mView_200_admire.setVisibility(View.GONE);
        }
        if(TAGS.mViewersNew.size()>=101 && TAGS.mViewersNew.size()<= 200){
            mLinearLayout10.setVisibility(View.GONE);
            mView_rate.setVisibility(View.GONE);
            if(mSharePreference.isTime24()){
                mLinearLayout10.setVisibility(View.VISIBLE);
                mView_rate.setVisibility(View.VISIBLE);
            }
        }


        if(TAGS.mViewersNew.size()<= 10){
            mTextView200.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
            mLinearLayout10.setVisibility(View.GONE);
            mView_200_admire.setVisibility(View.GONE);
            mView_100.setVisibility(View.GONE);
            mView_50.setVisibility(View.GONE);
            mView_rate.setVisibility(View.GONE);
            if(mSharePreference.isTime24()){
                mLinearLayout10.setVisibility(View.VISIBLE);
            }
            mLinearLayout25.setVisibility(View.GONE);
        }


        if (mSharePreferencePermanentpurchasefriend.isAdmire10()) {
            mLinearLayout10.setVisibility(View.GONE);
            mView_rate.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasefriend.isAdmire25()) {
            mLinearLayout10.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
            mView_rate.setVisibility(View.GONE);
            mView_25.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasefriend.isAdmire50()) {
            mLinearLayout10.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);

            mView_rate.setVisibility(View.GONE);
            mView_25.setVisibility(View.GONE);
            mView_50.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasefriend.isAdmire100()) {
            mLinearLayout10.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);

            mView_rate.setVisibility(View.GONE);
            mView_25.setVisibility(View.GONE);
            mView_50.setVisibility(View.GONE);
            mView_100.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasefriend.isAdmire200()) {
            mLinearLayout10.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);
            mTextView200.setVisibility(View.GONE);

            mView_rate.setVisibility(View.GONE);
            mView_25.setVisibility(View.GONE);
            mView_50.setVisibility(View.GONE);
            mView_100.setVisibility(View.GONE);
        }
        mImageView_Close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mLinearLayout10.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasefriend.isAdmire10()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.ADMIRE_10), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//
//                }

                inAppPurchase(String.valueOf(PurchaseItem.ADMIRE_10),mSharePreferencePermanentadd.isAdmire10());
                mDialog.dismiss();
            }
        });

        mLinearLayout25.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasefriend.isAdmire25()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.ADMIRE_25), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//
//                }

                inAppPurchase(String.valueOf(PurchaseItem.ADMIRE_25),mSharePreferencePermanentadd.isAdmire25());
                mDialog.dismiss();
            }
        });
        mTextView50.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasefriend.isAdmire50()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.ADMIRE_50), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//                }

                inAppPurchase(String.valueOf(PurchaseItem.ADMIRE_50),mSharePreferencePermanentadd.isAdmire50());
                mDialog.dismiss();
            }
        });

        mTextView100.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasefriend.isAdmire100()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.ADMIRE_100), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//                }

                inAppPurchase(String.valueOf(PurchaseItem.ADMIRE_100),mSharePreferencePermanentadd.isAdmire100());
                mDialog.dismiss();
            }
        });

        mTextView200.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//
//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasefriend.isAdmire200()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.ADMIRE_200), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//                }

                inAppPurchase(String.valueOf(PurchaseItem.ADMIRE_200),mSharePreferencePermanentadd.isAdmire200());
                mDialog.dismiss();
            }
        });
        mTextViewAds.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//                if (setup_successed) {
//                    if (!mSharePreferencePermanentadd.isAds()) {
//                        mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD), RC_REQUEST,
//                                mPurchaseFinishedListener, payload);
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//                }

                inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD),mSharePreferencePermanentadd.isAds());
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }


    public String getPurchaseText(int res, int size)
    {
        String text = "";

        if(TAGS.mViewersNew.size()<size)
            text = String.format(getResources().getString(res), TAGS.mViewersNew.size());
        else
            text = String.format(getResources().getString(res), size);

        return text;
    }

    public void dialog_admire_stalkers_purchase() {
        final Dialog mDialog = new Dialog(AdmireActivity.this);

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_admire_purchase);

        final ImageView mImageView_Close = (ImageView) mDialog.findViewById(R.id.purchase_dialog_admire_close);
        final TextView mTextViewTitle = (TextView) mDialog.findViewById(R.id.text_title);
        final LinearLayout mLinearLayout10 = (LinearLayout) mDialog.findViewById(R.id.layout_10);
        final LinearLayout mLinearLayout25 = (LinearLayout) mDialog.findViewById(R.id.layout_25);
        final LinearLayout mLinearLayout50 = (LinearLayout) mDialog.findViewById(R.id.layout_50);
        final LinearLayout mLinearLayout100 = (LinearLayout) mDialog.findViewById(R.id.layout_100);
        final LinearLayout mLinearLayout200 = (LinearLayout) mDialog.findViewById(R.id.layout_200);
        final LinearLayout mTextViewAds = (LinearLayout) mDialog.findViewById(R.id.layout_ads);
        View  mView_200_admire = (View)mDialog.findViewById(R.id.view_200_admire);
        View mView_100 = (View)mDialog.findViewById(R.id.view_100);
        View mView_50 = (View)mDialog.findViewById(R.id.view_50);
        View mView_rate = (View)mDialog.findViewById(R.id.view_rate);
        View mView_ads = (View)mDialog.findViewById(R.id.view_ads);
        View mView_25 = (View)mDialog.findViewById(R.id.view_25);



        TextView mTextViewRate = (TextView)mDialog.findViewById(R.id.text_rate);
        TextView mTextView35 = (TextView)mDialog.findViewById(R.id.text_35);
        TextView mTextView50 = (TextView)mDialog.findViewById(R.id.text_50);
        TextView mTextView100 = (TextView)mDialog.findViewById(R.id.text_100);
        TextView mTextView200 = (TextView)mDialog.findViewById(R.id.text_200);

        mTextViewRate.setText(getPurchaseTextStalker(R.string.admire_10,10));
        mTextView35.setText(getPurchaseTextStalker(R.string.admire_25,25));
        mTextView50.setText(getPurchaseTextStalker(R.string.admire_50,50));
        mTextView100.setText(getPurchaseTextStalker(R.string.admire_100,100));
        mTextView200.setText(getPurchaseTextStalker(R.string.admire_200,200));

        mTextViewTitle.setText(getResources().getString(R.string.stalker_store));


        if(TAGS.mStalkerUsers.size()>=11 && TAGS.mStalkerUsers.size()<= 24){
            mTextView200.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
            mView_200_admire.setVisibility(View.GONE);
            mView_100.setVisibility(View.GONE);
            mView_50.setVisibility(View.GONE);

        }
        if(TAGS.mStalkerUsers.size()>=25 && TAGS.mStalkerUsers.size()<= 50){

            mTextView200.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);
            mView_200_admire.setVisibility(View.GONE);
            mView_100.setVisibility(View.GONE);

        }

        if(TAGS.mStalkerUsers.size()>=51 && TAGS.mStalkerUsers.size()<= 100){
            mTextView200.setVisibility(View.GONE);
            mView_200_admire.setVisibility(View.GONE);
        }

        if(TAGS.mStalkerUsers.size()>=101 && TAGS.mStalkerUsers.size()<= 200){

        }

        if(TAGS.mStalkerUsers.size()<= 10){
            mTextView200.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
            mView_200_admire.setVisibility(View.GONE);
            mView_100.setVisibility(View.GONE);
            mView_50.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
        }


        if (mSharePreferencePermanentadd.isAds()) {
            mTextViewAds.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasestalkers.isStalkers10()) {
            mLinearLayout10.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasestalkers.isStalkers25()) {
            mLinearLayout10.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasestalkers.isStalkers50()) {
            mLinearLayout10.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasestalkers.isStalkers100()) {
            mLinearLayout10.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasestalkers.isStalkers200()) {
            mLinearLayout10.setVisibility(View.GONE);
            mLinearLayout25.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
            mTextView100.setVisibility(View.GONE);
            mTextView200.setVisibility(View.GONE);
        }

        mImageView_Close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });

        mLinearLayout10.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers10()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.STALKER_10), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//
//                }

                inAppPurchase(String.valueOf(PurchaseItem.STALKER_10),mSharePreferencePermanentadd.isStalkers10());
                mDialog.dismiss();
            }
        });

        mLinearLayout25.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers25()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.STALKER_25), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//
//                }

                inAppPurchase(String.valueOf(PurchaseItem.STALKER_25),mSharePreferencePermanentadd.isStalkers25());
                mDialog.dismiss();
            }
        });
        mTextView50.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers50()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.STALKER_50), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//                }

                inAppPurchase(String.valueOf(PurchaseItem.STALKER_50),mSharePreferencePermanentadd.isStalkers50());
                mDialog.dismiss();
            }
        });

        mTextView100.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers100()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.STALKER_100), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this,  getResources().getString(R.string.purchase_can_not));
//                }

                inAppPurchase(String.valueOf(PurchaseItem.STALKER_100),mSharePreferencePermanentadd.isStalkers100());
                mDialog.dismiss();
            }
        });

        mTextView200.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

//                if (setup_successed) {
//                    if (!mSharePreferencePermanentpurchasestalkers.isStalkers200()) {
//                        if (mHelper != null) {
//                            mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.STALKER_200), RC_REQUEST,
//                                    mPurchaseFinishedListener, payload);
//                        } else {
//                            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
//                        }
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//                }
                inAppPurchase(String.valueOf(PurchaseItem.STALKER_200),mSharePreferencePermanentadd.isStalkers200());
                mDialog.dismiss();
            }
        });
        mTextViewAds.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//                if (setup_successed) {
//                    if (!mSharePreferencePermanentadd.isAds()) {
//                        mHelper.launchPurchaseFlow(AdmireActivity.this, String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD), RC_REQUEST,
//                                mPurchaseFinishedListener, payload);
//                    } else {
//                        Utils.SetDiolog(AdmireActivity.this,getResources().getString(R.string.purchase_done_already));
//                    }
//                } else {
//                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));
//                }
                 inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD),mSharePreferencePermanentadd.isAds());
                mDialog.dismiss();
            }
        });

        mDialog.show();
    }

    public String getPurchaseTextStalker(int res, int size)
    {
        String text = "";

        if(TAGS.mStalkerUsers.size()<size)
            text = String.format(getResources().getString(res), TAGS.mStalkerUsers.size());
        else
            text = String.format(getResources().getString(res), size);

        return text;
    }

    public void ShowDialogPop() {

        mDialogPopup = new Dialog(AdmireActivity.this, R.style.PauseDialog);
        mDialogPopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialogPopup.setCanceledOnTouchOutside(false);
        mDialogPopup.setCancelable(false);
        mDialogPopup.setContentView(R.layout.dialog_welcome);

        ImageView mImageViewCancel = (ImageView) mDialogPopup.findViewById(R.id.done);
        mImageViewCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                mDialogPopup.dismiss();

            }
        });
    }

    public void inAppPurchase(String purchaseItem, boolean isBought) {
        if (setup_successed) {
            if (!isBought) {
                if (mHelper != null) {
                    mHelper.launchPurchaseFlow(AdmireActivity.this, purchaseItem, RC_REQUEST,
                            mPurchaseFinishedListener, payload);
                } else {
                    Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_error));
                }
            } else {
                Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_done_already));
            }
        } else {
            Utils.SetDiolog(AdmireActivity.this, getResources().getString(R.string.purchase_can_not));

        }
    }
}
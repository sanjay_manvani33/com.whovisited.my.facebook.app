/*
 * Android-PercentProgressBar
 * Copyright (c) 2015  Natasa Misic
 *
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package com.profile.stalkers.lib;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

import com.profile.stalkers.R;


abstract class BaseProgressView extends View {
    protected int progress;
    protected String progresscenter;
    protected int maximum_progress = 100;

    protected Paint foregroundPaint, backgroundPaint, textPaint,centertextpaint;
    protected float backgroundStrokeWidth = 3f, strokeWidth = 5f;
    protected int PADDING = 20;
    protected int color = getResources().getColor(R.color.white),
    backgroundColor = getResources().getColor(R.color.transparent1);

    protected int textColor = getResources().getColor(R.color.white);
    protected int shadowColor = getResources().getColor(R.color.shader);
    protected int textSize = 50;
    protected Typeface typeface;
    protected String typeface_path = "Montserrat-Bold.ttf";
    protected int height, width;

    protected Context context;
    protected boolean isRoundEdge;
    protected boolean isShadowed;
    protected OnProgressTrackListener listener;

    public void setOnProgressTrackListener(OnProgressTrackListener listener) {
        this.listener = listener;
    }

    protected abstract void init(Context context);

    public BaseProgressView(Context context) {
        super(context);
        init(context);

    }

    public BaseProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTypedArray(context, attrs);
        init(context);

    }


    public BaseProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);

    }

    private void initTypedArray(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.Progressmmm, 0, 0);
        try {

            progresscenter = "0%";

            progress = (int) typedArray.getFloat(
                    R.styleable.Progressmmm_progress, progress);
            strokeWidth = typedArray.getDimension(
                    R.styleable.Progressmmm_stroke_width, strokeWidth);
            backgroundStrokeWidth = typedArray.getDimension(
                    R.styleable.Progressmmm_background_stroke_width,
                    backgroundStrokeWidth);
            color = typedArray.getInt(
                    R.styleable.Progressmmm_progress_color, color);
            backgroundColor = typedArray.getInt(
                    R.styleable.Progressmmm_background_color, backgroundColor);
            textColor = typedArray.getInt(
                    R.styleable.Progressmmm_text_color, textColor);
            textSize = typedArray.getInt(
                    R.styleable.Progressmmm_text_size_circular, textSize);

        } finally {
            typedArray.recycle();
        }
        this.setLayerType(LAYER_TYPE_SOFTWARE, null);

        typeface = Typeface.createFromAsset(context.getAssets(), typeface_path);

    }

    protected void initBackgroundColor() {
        backgroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        backgroundPaint.setColor(backgroundColor);
        backgroundPaint.setStyle(Paint.Style.STROKE);
        backgroundPaint.setStrokeWidth(backgroundStrokeWidth);
        if (isRoundEdge) {
            backgroundPaint.setStrokeCap(Paint.Cap.ROUND);
        }
        if (isShadowed) {

//            backgroundPaint.setShadowLayer(3.0f, 0.0f, 2.0f, shadowColor);
            backgroundPaint.setShadowLayer(7.0f, 0.0f, 5.0f, shadowColor);
        }
    }

    protected void initForegroundColor() {
        foregroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        foregroundPaint.setColor(color);
        foregroundPaint.setStyle(Paint.Style.STROKE);
        foregroundPaint.setStrokeWidth(strokeWidth);
        if (isRoundEdge) {
            foregroundPaint.setStrokeCap(Paint.Cap.ROUND);
        }

        if (isShadowed) {

//            foregroundPaint.setShadowLayer(3.0f, 0.0f, 2.0f, shadowColor);
            foregroundPaint.setShadowLayer(7.0f, 0.0f, 5.0f, shadowColor);
        }
    }

    protected void initTextColor() {
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(textColor);
        textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        textPaint.setStrokeWidth(1f);
        textPaint.setTextSize(textSize);
        typeface = Typeface.createFromAsset(context.getAssets(), typeface_path);
//        textPaint.setTypeface(typeface,Typeface.BOLD);

        centertextpaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        centertextpaint.setColor(textColor);
        centertextpaint.setStyle(Paint.Style.FILL_AND_STROKE);
        centertextpaint.setStrokeWidth(5f);
//        centertextpaint.setTextSize(35);
        centertextpaint.setTextSize(getResources().getInteger(R.integer.circle_text));
        typeface = Typeface.createFromAsset(context.getAssets(), typeface_path);
        centertextpaint.setTypeface(typeface);



    }

    public float getProgress() {
        return progress;
    }


    public void setProgress(int progress,String progresscenter) {
//        System.out.println("base status "+progresscenter);
        setProgressInView(progress,progresscenter);

    }

    private synchronized void setProgressInView(int progress,String progresscenter) {
        this.progress = (progress <= maximum_progress) ? progress : maximum_progress;
        this.progresscenter = progresscenter;
        invalidate();
        trackProgressInView(progress, progresscenter);
    }

    private void trackProgressInView(int progress,String progresscenter) {
        if (listener != null) {
//            System.out.println("base status listner "+progresscenter);
            listener.onProgressUpdate(progress,progresscenter);
            if (progress >= maximum_progress) {
                listener.onProgressFinish();
            }
        }
    }

    public void resetProgress() {
        setProgress(0,"0%");

    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
        init(context);
    }

    public void setTypeface(String typefacePath) {
        this.typeface_path = typefacePath;
        init(context);

    }

    public int getProgressColor() {
        return color;
    }

    public void setProgressColor(int color) {
        this.color = color;
        init(context);

    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        init(context);

    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
        init(context);

    }

    public void setProgressStrokeWidth(int strokeWidth) {
        this.strokeWidth = strokeWidth;
        init(context);
    }

    public void setBackgroundStrokeWidth(int strokeWidth) {
        this.backgroundStrokeWidth = strokeWidth;
        init(context);
    }

    public void setRoundEdge(boolean isRoundEdge) {
        this.isRoundEdge = isRoundEdge;
        init(context);
    }

    public void setShadow(boolean isShadowed) {
        this.isShadowed = isShadowed;
        init(context);

    }
}

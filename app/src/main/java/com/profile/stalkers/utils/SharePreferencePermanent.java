package com.profile.stalkers.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.profile.stalkers.model.Viewers;

import java.util.ArrayList;

public class SharePreferencePermanent {

    public static String SHAREPREFERENCE = "com.WhoVisited.My.Facebook.app.permanent";
    SharedPreferences mSharedPreferences;
    SharedPreferences.Editor mEditor;

    public SharePreferencePermanent(Context con) {
        mContext = con;
        mSharedPreferences = con.getSharedPreferences(SHAREPREFERENCE, 0);

    }

    Context mContext;

    public static String GENDER = "gender";
    public static String USERNAME = "username";
    public static String USEREMAIL = "useremail";
    public static String USERPASSWORD = "userpassword";
    public static String USERID = "userid";
    public static String COMMENTADDED = "comment";
    public static String RATINGCHANGED = "ratingchanged";
    public static String FACEBOOKTOKEN = "facebooktoken";
    public static String PURCHASE = "purchase";
    public static String PURCHASE_50 = "purchase50";
    public static String PURCHASE_100 = "purchase100";
    public static String PURCHASE_200 = "purchase200";
    public static String PURCHASE_REMOVEAD = "purchase_ad";
    public static String PURCHASE_PEOPLE = "purchase_people";
    public static String PURCHASE_PEOPLE_25 = "purchase_people_25";
    public static String PURCHASE_PEOPLE_50 = "purchase_people_50";

    public static String FBSHARING = "fb_sharing";
    public static String RATE = "ratestar";

    public static String FBSHARINGPEOPLE = "fb_sharing_people";
    public static String RATEPEOPLE = "ratestar_people";

    public static String NOTIFICATION_LOCAL = "notification_local";
    public static String NOTIFICATION_ENABLE = "app_notification_enable";
    public static String APP_LANGUAGE = "app_language_selected";

    public static String STALKERS_10="stalkers_10";
    public static String STALKERS_25="stalkers_25";
    public static String STALKERS_50="stalkers_50";
    public static String STALKERS_100="stalkers_100";
    public static String STALKERS_200="stalkers_200";

    public static String ADMIRE_10="admire_10";
    public static String ADMIRE_25="admire_25";
    public static String ADMIRE_50="admire_50";
    public static String ADMIRE_100="admire_100";
    public static String ADMIRE_200="admire_200";

    public String mStringUserName = "";
    public String mStringUserEmail = "";
    public String mStringUserPassword = "";
    public String mStringUserId = "";
    public String mStringFacebookToken = "";
    public boolean commentAdded = false;
    public boolean ratingChanged = false;
    public static String TIME="time";
    public static String TIME24="time24";
    public long getTime() {
        return mSharedPreferences.getLong(TIME, 0);
    }

    public void setTime(long time) {
        open_editor();
        mEditor.putLong(TIME, time).commit();
    }
    public boolean isTime24() {
        return mSharedPreferences.getBoolean(TIME24, false);
    }
    public void setTime24(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(TIME24, puchaseItem).commit();
    }
    public boolean isAdmire10() {
        return mSharedPreferences.getBoolean(ADMIRE_10, false);
    }
    public void setAdmire10(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(ADMIRE_10, puchaseItem).commit();
    }
    public boolean isAdmire25() {
        return mSharedPreferences.getBoolean(ADMIRE_25, false);
    }
    public void setAdmire25(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(ADMIRE_25, puchaseItem).commit();
    }
    public boolean isAdmire50() {
        return mSharedPreferences.getBoolean(ADMIRE_50, false);
    }
    public void setAdmire50(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(ADMIRE_50, puchaseItem).commit();
    }
    public boolean isAdmire100() {
        return mSharedPreferences.getBoolean(ADMIRE_100, false);
    }
    public void setAdmire100(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(ADMIRE_100, puchaseItem).commit();
    }
    public boolean isAdmire200() {
        return mSharedPreferences.getBoolean(ADMIRE_200, false);
    }
    public void setAdmire200(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(ADMIRE_200, puchaseItem).commit();
    }


    public boolean isStalkers10() {
        return mSharedPreferences.getBoolean(STALKERS_10, false);
    }
    public void setStalkers10(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(STALKERS_10, puchaseItem).commit();
    }
    public boolean isStalkers25() {
        return mSharedPreferences.getBoolean(STALKERS_25, false);
    }
    public void setStalkers25(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(STALKERS_25, puchaseItem).commit();
    }
    public boolean isStalkers50() {
        return mSharedPreferences.getBoolean(STALKERS_50, false);
    }
    public void setStalkers50(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(STALKERS_50, puchaseItem).commit();
    }
    public boolean isStalkers100() {
        return mSharedPreferences.getBoolean(STALKERS_100, false);
    }
    public void setStalkers100(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(STALKERS_100, puchaseItem).commit();
    }
    public boolean isStalkers200() {
        return mSharedPreferences.getBoolean(STALKERS_200, false);
    }
    public void setStalkers200(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(STALKERS_200, puchaseItem).commit();
    }

    public String getToken() {
        return mSharedPreferences.getString("TOKEN", "");
    }

    public void setToken(String mStringUserPassword) {
        open_editor();
        mEditor.putString("TOKEN", mStringUserPassword).commit();
        this.mStringUserPassword = mStringUserPassword;
    }
    public String getmStringGender() {
        return mSharedPreferences.getString(GENDER, "not available");
    }

    public void setmStringGender(String mStringGender) {
        open_editor();
        mEditor.putString(GENDER, mStringGender).commit();
    }
    public boolean isServiceCalled() {
        return mSharedPreferences.getBoolean("SERVICECALLED", false);
    }

    public void setServiceCall(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean("SERVICECALLED", puchaseItem).commit();

    }

    public boolean isPuchaseItem35() {
        return mSharedPreferences.getBoolean(PURCHASE, false);
    }

    public void setPuchaseItem35(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(PURCHASE, puchaseItem).commit();
        this.puchaseItem = puchaseItem;
    }

    public boolean puchaseItem = false;

    public String getmStringFacebookToken() {
        return mSharedPreferences.getString(FACEBOOKTOKEN, "");
    }

    public void setmStringFacebookToken(String mStringFacebookToken) {
        open_editor();
        mEditor.putString(FACEBOOKTOKEN, mStringFacebookToken).commit();
        this.mStringFacebookToken = mStringFacebookToken;
    }

    public boolean isRatingChanged() {
        return mSharedPreferences.getBoolean(RATINGCHANGED, false);
    }

    public void setRatingChanged(boolean ratingChanged) {
        open_editor();
        mEditor.putBoolean(RATINGCHANGED, ratingChanged).commit();
        this.ratingChanged = ratingChanged;
    }

    public boolean isCommentAdded() {
        return mSharedPreferences.getBoolean(COMMENTADDED, false);
    }

    public void setCommentAdded(boolean commentAdded) {
        open_editor();
        mEditor.putBoolean(COMMENTADDED, commentAdded).commit();
        this.commentAdded = commentAdded;
    }

    public String getmStringUserId() {
        return mSharedPreferences.getString(USERID, "");
    }

    public void setmStringUserId(String mStringUserId) {
        open_editor();
        mEditor.putString(USERID, mStringUserId).commit();
        this.mStringUserId = mStringUserId;
    }



    public String getmStringUserName() {
        return mSharedPreferences.getString(USERNAME, "");
    }

    public void setmStringUserName(String mStringUserName) {
        open_editor();
        mEditor.putString(USERNAME, mStringUserName).commit();
        this.mStringUserName = mStringUserName;
    }

    public String getmStringUserEmail() {
        return mSharedPreferences.getString(USEREMAIL, "not available");
    }

    public void setmStringUserEmail(String mStringUserEmail) {
        open_editor();
        mEditor.putString(USEREMAIL, mStringUserEmail).commit();
        this.mStringUserEmail = mStringUserEmail;
    }

    public String getImage() {
        return mSharedPreferences.getString("IMAGE", "not available");
    }

    public void setImage(String mStringUserEmail) {
        open_editor();
        mEditor.putString("IMAGE", mStringUserEmail).commit();
        this.mStringUserEmail = mStringUserEmail;
    }
    public String getDeviceId() {
        return mSharedPreferences.getString(USERPASSWORD, "");
    }

    public void setDeviceId(String mStringUserPassword) {
        open_editor();
        mEditor.putString(USERPASSWORD, mStringUserPassword).commit();
        this.mStringUserPassword = mStringUserPassword;
    }

    public String getOS() {
        return mSharedPreferences.getString("OSVER", "");
    }

    public void setOS(String mStringUserPassword) {
        open_editor();
        mEditor.putString("OSVER", mStringUserPassword).commit();
        this.mStringUserPassword = mStringUserPassword;
    }

    public String getAppUrl() {
        return mSharedPreferences.getString("APP", "");
    }

    public void setAppUrl(String mStringUserPassword) {
        open_editor();
        mEditor.putString("APP", mStringUserPassword).commit();
        this.mStringUserPassword = mStringUserPassword;
    }
    public void open_editor() {
        // TODO Auto-generated method stub
        mEditor = mSharedPreferences.edit();
    }

    public void clearData() {
        // TODO Auto-generated method stub
        mEditor = mSharedPreferences.edit();
        mEditor.clear();
        mEditor.commit();
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("unchecked")
    public ArrayList<Viewers> getcontacts() {
        open_editor();
        int size = mSharedPreferences.getInt("contact_size", 0);

        Log.e("size got", String.valueOf(size));
        ArrayList<Viewers> mContactInfo = new ArrayList<Viewers>();
        for (int i = 0; i < size; i++) {
            Viewers mContactInfo2 = new Viewers();
            mContactInfo2.setViewid(mSharedPreferences.getInt("id" + i, 0));
            mContactInfo2.setName(mSharedPreferences.getString("name" + i, ""));
            mContactInfo2.setId(mSharedPreferences.getString("number" + i, ""));
            mContactInfo2.setImage(mSharedPreferences.getString("image" + i, ""));
            mContactInfo.add(mContactInfo2);

        }
        return mContactInfo;

    }

    @SuppressLint("NewApi")
    @SuppressWarnings("unchecked")
    public boolean saveContacts(ArrayList<Viewers> mcontact) {
        open_editor();
        mEditor.putInt("contact_size", mcontact.size()); /* sKey is an array */
        Log.e("size save", String.valueOf(mcontact.size()));

        for (int i = 0; i < mcontact.size(); i++) {
            mEditor.remove("name" + i);
            mEditor.remove("number" + i);
            mEditor.remove("number" + i);
            mEditor.remove("id" + i);

            mEditor.putInt("id" + i, mcontact.get(i).getViewid());
            mEditor.putString("name" + i, mcontact.get(i).getName());
            mEditor.putString("number" + i, mcontact.get(i).getId());
            mEditor.putString("image" + i, mcontact.get(i).getImage());
        }

        return mEditor.commit();
    }

    public boolean isPuchaseItem50() {
        return mSharedPreferences.getBoolean(PURCHASE_50, false);
    }

    public void setPuchaseItem50(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(PURCHASE_50, puchaseItem).commit();
    }

    public boolean isPuchaseItem200() {
        return mSharedPreferences.getBoolean(PURCHASE_200, false);
    }

    public void setPuchaseItem200(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(PURCHASE_200, puchaseItem).commit();
    }
    public boolean isPuchaseItem100() {
        return mSharedPreferences.getBoolean(PURCHASE_100, false);
    }

    public void setPuchaseItem100(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(PURCHASE_100, puchaseItem).commit();
    }

    public boolean isPuchaseItemRemove() {
        return mSharedPreferences.getBoolean(PURCHASE_REMOVEAD, false);
    }

    public void setPuchaseItemRemove(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(PURCHASE_REMOVEAD, puchaseItem).commit();
    }

    public boolean isPuchasePeople() {
        return mSharedPreferences.getBoolean(PURCHASE_PEOPLE, false);
    }

    public void setPuchasePeople(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(PURCHASE_PEOPLE, puchaseItem).commit();
    }


    public boolean isPuchasePeople25() {
        return mSharedPreferences.getBoolean(PURCHASE_PEOPLE_25, false);
    }

    public void setPuchasePeople25(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(PURCHASE_PEOPLE_25, puchaseItem).commit();
    }

    public boolean isPuchasePeople50() {
        return mSharedPreferences.getBoolean(PURCHASE_PEOPLE_50, false);
    }

    public void setPuchasePeople50(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(PURCHASE_PEOPLE_50, puchaseItem).commit();
    }

    public boolean isAds() {
        return mSharedPreferences.getBoolean("ADS", false);
    }

    public void setAds(boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean("ADS", puchaseItem).commit();
    }

    public boolean isShareonFbPeople() {
        return mSharedPreferences.getBoolean(FBSHARINGPEOPLE, false);
    }

    public void setSharefbPeople() {
        open_editor();
        mEditor.putBoolean(FBSHARINGPEOPLE, true).commit();
    }

    public boolean isRatePeople() {
        return mSharedPreferences.getBoolean(RATEPEOPLE, false);
    }

    public void setRatePeople() {
        open_editor();
        mEditor.putBoolean(RATEPEOPLE, true).commit();
    }


    public void setNotificationLocal(boolean islogin) {
        // TODO Auto-generated method stub
        open_editor();
        mEditor.putBoolean(NOTIFICATION_LOCAL, islogin);
        mEditor.commit();
    }

    public Boolean isNotificationLocal() {
        // TODO Auto-generated method stub
        return mSharedPreferences.getBoolean(NOTIFICATION_LOCAL, false);
    }


    public void setNotificationEnabled(boolean isEnabled) {
        open_editor();
        mEditor.putBoolean(NOTIFICATION_ENABLE, isEnabled);
        mEditor.commit();
    }

    public Boolean isNotificationEnabled() {
        // TODO Auto-generated method stub
        return mSharedPreferences.getBoolean(NOTIFICATION_ENABLE, true);
    }


    public void setAppLanguage(String language) {
        open_editor();
        mEditor.putString(APP_LANGUAGE, language);
        mEditor.commit();
    }

    public String getAppLanguage() {
        // TODO Auto-generated method stub
        return mSharedPreferences.getString(APP_LANGUAGE, Utils.ENGLISH);
    }
    public boolean isShareonFb(String Username) {
        return mSharedPreferences.getBoolean(Username, false);
    }

    public void setSharefb(String Username,boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(Username, puchaseItem).commit();
    }

    public boolean isRate(String Username) {
        return mSharedPreferences.getBoolean(Username+"1", false);
    }

    public void setRate(String Username,boolean puchaseItem) {
        open_editor();
        mEditor.putBoolean(Username+"1", puchaseItem).commit();
    }
}
package com.profile.stalkers.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.TextView;

import com.profile.stalkers.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class Utils {


    public static AlertDialog mAlertDialog;
    public static String[] ITEMS = {"", "Suggestion ?", "Report an issue",
            "More apps", "Facebook Tips", "Settings"};
    public static final int[] ITEMS_ICONS = {
            R.drawable.ic_suggestion_white, R.drawable.ic_report_white,
            R.drawable.ic_more, R.drawable.ic_tips,
            R.drawable.ic_settings, R.drawable.ic_admire,
            R.drawable.icon};

    public static final String ENGLISH = "English";
    public static final String SPANISH = "Spanish";
    public static final String PORTUGUESE = "Portuguese";
    public static final String FRENCH = "French";
    public static final String GERMAN = "German";
    public static List<Integer> getlist(int start, int end) {

        List<Integer> mylist = new ArrayList<Integer>();
        for (int i = start; i <= end; i++)
            mylist.add(i);

        Collections.shuffle(mylist);
        return mylist;

    }

	public static void SetFont(TextView textView, Activity activity)
	{
		Typeface font = Typeface.createFromAsset(activity.getAssets(), "Roboto-Bold.ttf");
		textView.setTypeface(font);
	}

    public static boolean isInternetConnected(Context mContext) {
        boolean isInternet = false;
        try {
            ConnectivityManager connect = null;
            connect = (ConnectivityManager) mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connect != null) {
                NetworkInfo resultMobile = connect
                        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                NetworkInfo resultWifi = connect
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);


                if ((resultMobile != null && resultMobile.isConnected())
                        || (resultWifi != null && resultWifi.isConnected())) {
                    isInternet = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isInternet;
    }

    public static void SetDiolog(Activity activity, String message) {
        mAlertDialog = new AlertDialog.Builder(activity).create();
        mAlertDialog.setMessage(message);

        mAlertDialog.setButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        mAlertDialog.show();
    }

    public static void changeAppLanguage(Context mContext, String languageType) {
        Locale locale = null;
        String langtype = "en_US";
        if (languageType.equalsIgnoreCase(ENGLISH)) {

            locale = new Locale("en_US");
            langtype = "en_US";

        } else if (languageType.equalsIgnoreCase(SPANISH)) {

            locale = new Locale("es");
            langtype = "es";

        } else if (languageType.equalsIgnoreCase(PORTUGUESE)) {

            locale = new Locale("pt");
            langtype = "pt";

        } else if (languageType.equalsIgnoreCase(FRENCH)) {

            locale = new Locale("fr");
            langtype = "fr";

        } else if (languageType.equalsIgnoreCase(GERMAN)) {

            locale = new Locale("de");
            langtype = "de";

        }
        if (locale == null)
            locale = new Locale("en_US");

//        Locale.setDefault(locale);
//        Configuration config = new Configuration();
//        config.locale = locale;
//        mContext.getApplicationContext().getResources().updateConfiguration(config, null);

        LocaleHelper.setLocale(mContext,langtype);
    }
}

package com.profile.stalkers.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URLEncoder;
import java.util.List;


/**
 * Created by abcd on 10/10/2017. pradip
 */


public class AsyncRequest extends AsyncTask<String, Integer, String> {

    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String POSTURLCONNECTION = "POSTURLCONNECTION";
    OnAsyncRequestComplete caller;
    Activity context;
    String method = GET;
    List<NameValuePair> parameters = null;
    MultipartEntity multipartEntity = null;
    String activityLable = "";
    boolean isShow = true;
    public  ProgressDialog pDialog = null;

    // Three Constructors
    public AsyncRequest(Activity a, String m, List<NameValuePair> p) {

        caller = (OnAsyncRequestComplete) a;
        context = a;
        method = m;
        parameters = p;
    }

    public AsyncRequest(Activity a, String m) {
        caller = (OnAsyncRequestComplete) a;
        context = a;
        method = m;
    }

    public AsyncRequest(Activity a, String m, String type) {
        caller = (OnAsyncRequestComplete) a;
        context = a;
        method = m;
        activityLable = type;
    }


    public void isShowProgress(boolean b) {
        isShow = b;
    }

    public void onPreExecute() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading data.."); // typically you will define such
        // strings in a remote file.

        if (!Utils.isInternetConnected(context)) {
            //Utils.ShowNoInternetConnection(context);
            cancel(true);
        }

        if (isShow && !context.isFinishing())
            pDialog.show();
        else
            return;
    }


    public String doInBackground(String... urls) {
        // get url pointing to entry point of API
        String address = urls[0].toString();
        if (method == POST) {
            //   url = address;
            return post(address);
        }
        if (method == GET) {
            return get(address);
        }
        if (method == POSTURLCONNECTION) {
            return postHttpUrl(address);
        }

        return null;
    }

    public void onProgressUpdate(Integer... progress) {
        // you can implement some progressBar and update it in this record
        // setProgressPercent(progress[0]);
    }

    public void onPostExecute(String response) {
        if (pDialog != null && pDialog.isShowing() && !context.isFinishing()) {
            pDialog.dismiss();
        }
        if(pDialog != null){
            pDialog.dismiss();
        }
        // Log.w("onPost", "onPostExecute URL: " + url + "->Response: " + response);
        if(!context.isFinishing()) caller.asyncResponse(response, activityLable);
    }

    protected void onCancelled(String response) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
        if(response!=null)
            caller.asyncResponse(response, activityLable);
    }

    // Interface to be implemented by calling activity
    public interface OnAsyncRequestComplete {
        public void asyncResponse(String response, String label);
    }


    @SuppressWarnings("deprecation")
    private String get(String address) {
        try {

            if (parameters != null) {

                String query = "";
                String EQ = "=";
                String AMP = "&";
                for (NameValuePair param : parameters) {
                    query += param.getName() + EQ + URLEncoder.encode(param.getValue()) + AMP;
                }

                if (query != "") {
                    address += "?" + query;
                }
            }

            HttpClient client = new DefaultHttpClient();
            HttpGet get = new HttpGet(address);
            //   url = address;
            HttpResponse response = client.execute(get);
            return stringifyResponse(response);

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String post(String address) {
        try {

            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(address);

            if (multipartEntity != null) {
                post.setEntity(multipartEntity);
            }

            HttpResponse response = client.execute(post);
            return stringifyResponse(response);

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String postHttpUrl(String address) {
        String result = "";
        java.net.URL url1 = null;

        if (parameters != null) {

            String query = "";
            String EQ = "=";
            String AMP = "&";
            for (NameValuePair param : parameters) {
                query += param.getName() + EQ + URLEncoder.encode(param.getValue()) + AMP;
            }

            if (query != "") {
                address += "?" + query;
            }
        }

        try {
            url1 = new java.net.URL(address);
            HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
            conn.setRequestMethod("POST");

            InputStream in = new BufferedInputStream(conn.getInputStream());
            result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
            return result;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            result = null;
            e.printStackTrace();
        }
        return null;
    }



    private String stringifyResponse(HttpResponse response) {
        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer sb = new StringBuffer("");
            String line = "";
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            in.close();

            return sb.toString();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
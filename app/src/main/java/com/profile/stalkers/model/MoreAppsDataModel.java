package com.profile.stalkers.model;

import java.io.Serializable;

/**
 * Created by imac on 28/10/16.
 */

public class MoreAppsDataModel implements Serializable {

    String app_id = "";
    String name = "";
    String play_url = "";
    String image_path = "";
    String app_name = "";
    String ratings = "";


    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlay_url() {
        return play_url;
    }

    public void setPlay_url(String play_url) {
        this.play_url = play_url;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = "http://incredibleapps.net/applications/images_new_app/" + image_path;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public String getRatings() {
        return ratings;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }
}

package com.profile.stalkers.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by WayCreon_Nitin on 9/30/2016.
 */
public class GetTips implements Serializable {
    private String tip_detail_id;
    private String top_detail;
    private  String bottom_detail;
    private String tip_master_id;
    private String tip_photo_id;
    private String tip_image;
    private String image_desc;
    private String no_of_img;

ArrayList<GetTips> TipsDetail=  new ArrayList<>();

    public ArrayList<GetTips> getTipsDetail() {
        return TipsDetail;
    }

    public void setTipsDetail(ArrayList<GetTips> tipsDetail) {
        TipsDetail = tipsDetail;
    }

    public String getTip_detail_id() {
        return tip_detail_id;
    }

    public void setTip_detail_id(String tip_detail_id) {
        this.tip_detail_id = tip_detail_id;
    }

    public String getTop_detail() {
        return top_detail;
    }

    public void setTop_detail(String top_detail) {
        this.top_detail = top_detail;
    }

    public String getBottom_detail() {
        return bottom_detail;
    }

    public void setBottom_detail(String bottom_detail) {
        this.bottom_detail = bottom_detail;
    }

    public String getTip_master_id() {
        return tip_master_id;
    }

    public void setTip_master_id(String tip_master_id) {
        this.tip_master_id = tip_master_id;
    }

    public String getTip_photo_id() {
        return tip_photo_id;
    }

    public void setTip_photo_id(String tip_photo_id) {
        this.tip_photo_id = tip_photo_id;
    }

    public String getTip_image() {
        return
                "http://incredibleapps.net/facebooktips/tips_images/"+tip_image;
    }

    public void setTip_image(String tip_image) {
        this.tip_image = tip_image;
    }

    public String getImage_desc() {
        return image_desc;
    }

    public void setImage_desc(String image_desc) {
        this.image_desc = image_desc;
    }

    public String getNo_of_img() {
        return no_of_img;
    }

    public void setNo_of_img(String no_of_img) {
        this.no_of_img = no_of_img;
    }
}


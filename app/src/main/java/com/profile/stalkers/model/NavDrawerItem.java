package com.profile.stalkers.model;

import java.io.Serializable;

public class NavDrawerItem implements Serializable
{
	 	private String title;
	    private int color=0;
	    private int icon=0;

	 

	    public String getTitle(){
	        return this.title;
	    }
	     
	      
	    public int getColor() {
			return color;
		}

		public void setColor(int color) {
			this.color = color;
		}



	    public void setTitle(String title){
	        this.title = title;
	    }
	     
	   
	     



		public int getIcon() {
			return icon;
		}


		public void setIcon(int icon) {
			this.icon = icon;
		}
	    
	    
	    
}

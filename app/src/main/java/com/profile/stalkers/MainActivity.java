
package com.profile.stalkers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.profile.stalkers.adapters.DataAdapter;
import com.profile.stalkers.adapters.DataAdapterStrangers;
import com.profile.stalkers.adapters.NavDrawerListAdapter;
import com.profile.stalkers.model.NavDrawerItem;
import com.profile.stalkers.model.Viewers;
import com.profile.stalkers.purchase.IabHelper;
import com.profile.stalkers.purchase.IabResult;
import com.profile.stalkers.purchase.Inventory;
import com.profile.stalkers.purchase.Purchase;
import com.profile.stalkers.purchase.PurchaseItem;
import com.profile.stalkers.utils.AnalyticsApplication;
import com.profile.stalkers.utils.AsyncRequest;
import com.profile.stalkers.utils.GridSpacingItemDecoration;
import com.profile.stalkers.utils.RecyclerItemClickListener;
import com.profile.stalkers.utils.SharePreference;
import com.profile.stalkers.utils.SharePreferencePermanent;
import com.profile.stalkers.utils.TAGS;
import com.profile.stalkers.utils.TinyDB;
import com.profile.stalkers.utils.Utils;
import com.splunk.mint.Mint;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import me.itangqi.waveloadingview.WaveLoadingView;

import static com.profile.stalkers.model.dataStrings.EMail;
import static com.profile.stalkers.model.dataStrings.FACEBOOK_ARKVERSE;
import static com.profile.stalkers.model.dataStrings.FACEBOOK_KATANA_URL;
import static com.profile.stalkers.model.dataStrings.FACEBOOK_PROFILE;
import static com.profile.stalkers.model.dataStrings.MARKET_URL;
import static com.profile.stalkers.model.dataStrings.PLAY_STORE;
import static com.profile.stalkers.model.dataStrings.RATINGLABEL;
import static com.profile.stalkers.model.dataStrings.REGISTER;
import static com.profile.stalkers.model.dataStrings.TRUE;
import static com.profile.stalkers.model.dataStrings.email_id;
import static com.profile.stalkers.model.dataStrings.improvments;
import static com.profile.stalkers.model.dataStrings.name;
import static com.profile.stalkers.model.dataStrings.rate;

public class MainActivity extends AppCompatActivity implements AsyncRequest.OnAsyncRequestComplete,View.OnClickListener{

    public static final String LOCALTIME = "localtime";
    public static final String STORELIST = "storelist";
    public static final String LOCALTIME_STRAGNER = "localtime_stranger";
    public static final String STORELIST_STRANGER = "storelist_stranger";
    public static final int REFRESHLOADTIME = 30;

    int loadingCount = 0;
    int loadingCountStranger = 0;


    String regex = "-?\\d+(\\.\\d+)?";
    private String payload = "";
    String mainString = "";
    public static final String GRAPH_PATH = "me/permissions";
    public static final String SUCCESS = "success";

    public JSONObject student1;
    public JSONArray jsonArray;

    SharedPreferences sharedPreferences;

    ScrollView mScrollViewStranger;

    WebView mWebViewFacebook;
    WebView mWebViewFacebookStrangers;

    RelativeLayout main;
    ArrayList<Viewers> mViewers;
    ArrayList<Viewers> mViewersPeople;

    HashMap<String, String> mHashMap;

    AlertDialog alertDialog;
    SharedPreferences sp1;

    List<String> fbIds;
    List<String> nameList;

    SharePreference mSharePreference;
    SharePreferencePermanent mSharePreferencePermanent;
    SharePreferencePermanent mSharePreferencePermanentRate;
    SharePreferencePermanent mSharePreferencePermanentShare;
    SharePreferencePermanent mSharePreferencePermanentpurchasefriend;
    SharePreferencePermanent mSharePreferencePermanentadd;
    SharePreferencePermanent mSharePreferencePermanentpurchasestranger;

    boolean isTokenExpire = false;
    boolean isBoughtItem1 = false;
    boolean isBoughtItem2 = false;
    boolean isBoughtItem3 = false;
    boolean isBoughtRemoveAd = false;
    public static boolean isBoughtPeople = false;
    boolean isprocessed = false;
    boolean isDestroy = false;
    boolean isListFinished = false;
    boolean isfirstclick = false;
    boolean isInAppBilling = false;
    private boolean setup_successed = false;
    boolean webStranger = false;
    boolean webUsers = false;
    boolean clickShare = false;
    boolean callmethod = false;
    boolean callstrangers = false;
    boolean callinback = false;
    boolean buttonClicked = false;
    boolean isRunningStranger = false;
    boolean isRunning = false;
    boolean isFacebookCalling = false;
    boolean isFacebookCallingStrange = false;
 public   boolean activityDestroyed = false;
 public   boolean isOpenScreen = true;

    IabHelper mHelper;

    Timer mTimer;

    ImageView mImageViewRefresh;
    ImageView mImageViewMenu;

    UploadFeed mUploadFeed;
    UploadFeedKnow mUploadFeedKnow;

    WaveLoadingView circularProgress, circularProgressStranger;

    Button mButtonWhoView;
    Button mButtonWhoPeople;

    public DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ArrayList<NavDrawerItem> navDrawerItems;


    TextView mTextViewTitle;

    float x = 0;
    float y = 0;

    BroadcastReceiver br;

    Dialog dialogInternet;
    Dialog mDialogPopup;
    Dialog mDialog;

    ShareDialog shareDialog;
    CallbackManager callbackManager;

    LinearLayout mLinearLayoutButtons;
    LinearLayout mLinearLayoutNoInternet;
    LinearLayout mLinearLayoutCart;

    RelativeLayout mRelativeLayoutMore;

    AdView mAdView;
    AdRequest adRequest;

    RecyclerView mRecyclerView;
    RecyclerView mRecyclerViewStrangers;

    DataAdapter mDataAdapter;
    DataAdapterStrangers mDataAdapterStrangers;

    View progressFriend;
    public View progressStrangers, no_user_dialog;
    View view_mainss;

    TinyDB tinydb;

    int i = 0;
    public static int SHOWUSERS = 5;
    private static final int RC_REQUEST = 10001;
    int count = 0;
    private int userList = 200;

    public void addDrawerItems() {

        Utils.ITEMS = getResources().getStringArray(R.array.navigation_menu);

        for (int pos = 0; pos < Utils.ITEMS.length; pos++) {
            NavDrawerItem mItem = new NavDrawerItem();

            mItem.setTitle(Utils.ITEMS[pos]);
            mItem.setIcon(Utils.ITEMS_ICONS[pos]);
            navDrawerItems.add(mItem);
        }
    }

    @Override
    public void asyncResponse(String response, String label)
    {

        if(label.equalsIgnoreCase(REGISTER))
        {
            registerParsing(response);
        }
        else if(label.equalsIgnoreCase(RATINGLABEL)) {
            rateparsing(response);
        }
    }
    public void registerParsing(String response){
            if (mSharePreferencePermanentShare.isShareonFb(mSharePreference.getmStringUserName())) {
                mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
            }
            else{
                mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),false);
            }
             if (mSharePreferencePermanentRate.isRate(mSharePreference.getmStringUserName()))
             {
                 mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);
             } else {
                 mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),false);
             }

        WebData();
    }
    public void rateparsing(String response){
            if (mSharePreferencePermanentShare.isShareonFb(mSharePreference.getmStringUserName()))
            {
                if (clickShare)
                {
                    mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);

                } else {
                    mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateRating();
                        }
                    });
                }
            }

    }

    public void Click_On_Cart(){
        if (mRecyclerView.getVisibility() == View.VISIBLE) {dialog_purchase();}
        else {
            dialog_purchase_stranger();
        }
    }

    public void Click_On_Main_Menu(){
        if (!progressFriend.isShown() && !progressStrangers.isShown()) {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                if (mTextViewTitle.getText().toString().equals(getResources().getString(R.string.your_friends_small))) {
                }

                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
        } else {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            } else {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.cart:
               Click_On_Cart();
                break;
            case R.id.main_menu:
                    Click_On_Main_Menu();
                break;
            case R.id.button_whoview:
                    Click_On_Button_WhoView();
                break;
            case R.id.button_people:
              //  Click_On_People();
                cacheStrangerList();
                break;

            case R.id.imageview_refresh:
                Click_On_Refresh();
                break;
            default:
                break;
        }
    }

    public void Click_On_Button_WhoView(){
        if (progressStrangers.getVisibility() == View.GONE) {
          //  mTextViewTitle.setText(getResources().getString(R.string.your_friends_small));


            mButtonWhoView.setBackgroundResource(R.drawable.img_selected);
            mButtonWhoPeople.setBackgroundResource(R.drawable.img_unselected);
            mButtonWhoPeople.setTextColor(getResources().getColor(R.color.blue_theme_color));
            mButtonWhoView.setTextColor(getResources().getColor(R.color.white));
            mRecyclerView.setVisibility(View.VISIBLE);
            mRecyclerViewStrangers.setVisibility(View.GONE);

            mImageViewRefresh.setVisibility(View.VISIBLE);

            if (mViewers.size() == 0) {
                isFacebookCalling = false;
                mViewers.clear();
                mDataAdapter.notifyDataSetChanged();
                mWebViewFacebook.reload();
                mRelativeLayoutMore.setVisibility(View.GONE);
                mLinearLayoutButtons.setVisibility(View.GONE);
            }
            else{
                mRelativeLayoutMore.setVisibility(View.VISIBLE);
                mLinearLayoutButtons.setVisibility(View.VISIBLE);
            }
        } else {
            showAlert(getResources().getString(R.string.profile_visitors), getResources().getString(R.string.process_going));
        }
    }

    public void Click_On_People(){
        if (progressFriend.getVisibility() == View.GONE) {
           // mTextViewTitle.setText(getResources().getString(R.string.strangers_small));
            mButtonWhoPeople.setBackgroundResource(R.drawable.img_selected);
            mButtonWhoView.setBackgroundResource(R.drawable.img_unselected);
            mButtonWhoView.setTextColor(getResources().getColor(R.color.blue_theme_color));
            mButtonWhoPeople.setTextColor(getResources().getColor(R.color.white));

            if (mViewersPeople.size() == 0) {
                progressStrangers.setVisibility(View.VISIBLE);
                view_mainss.setVisibility(View.VISIBLE);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                mWebViewFacebookStrangers.loadUrl(getNativeKey4());
                isRunningStranger = true;
                mRelativeLayoutMore.setVisibility(View.GONE);
                mLinearLayoutButtons.setVisibility(View.GONE);
            } else {
                mRelativeLayoutMore.setVisibility(View.VISIBLE);
                mLinearLayoutButtons.setVisibility(View.VISIBLE);
            }
            progressFriend.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.GONE);
            mRecyclerViewStrangers.setVisibility(View.VISIBLE);
        } else {
            showAlert(getResources().getString(R.string.profile_visitors), getResources().getString(R.string.process_going));
        }
    }

    public void Click_On_Refresh(){
        runOnUiThread(new Runnable() {
            public void run() {
                refreshClick();

            }
        });
    }

//    class SlideMenuClickListener implements OnItemClickListener {
//
//        @Override
//        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//                                long arg3) {
////            displayView(arg2);
//        }
//
//    }
//
//    private void displayView(int position) {
//
//    }

    static {
        System.loadLibrary("keys");
    }

    public native String getNativeKey1();

    public native String getNativeKey2();

    public native String getNativeKey3();

    public native String getNativeKey4();

    public native String getNativeKey5();

    public native String getNativeKey6();

    public native String getNativeKey7();

    public native String getNativeKey8();

    public native String getNativeKey9();

    public native String getNativeKey10();

    public native String getNativeKey11();

    public native String getNativeKey12();

    public native String getimage();

    public native String getimage1();

    public native String geturl();

    public native String geturl1();

    public native String getstringvalue();

    public native String getstringvalue1();

    public native String getstringvalue2();

    public native String getstringvalue3();

    public native String getstringvalue4();

    public native String getstringvalue5();

    public native String getstringvalue6();

    public native String getstringvalue7();

    public native String getsplit();

    public native String getconstant();

    public native String register();

    public native String mainweb();

    public native String updateshare();

    public native String updaterate();

    public native String finalkey();

    public native String sign1();

    public native String sign2();

    public native String image3();

    public native String numbers();

    List<Integer> list20;
    List<Integer> list50;
    List<Integer> list100;
    List<Integer> list200;

    CounterClass mtimerAds;
    InterstitialAd interstitial;

    private Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        Mint.initAndStartSession(MainActivity.this, "17510f4c");


        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        tinydb = new TinyDB(this);

        view_mainss = (View) findViewById(R.id.view_mainss);
        mButtonWhoView = (Button) findViewById(R.id.button_whoview);
        mButtonWhoPeople = (Button) findViewById(R.id.button_people);

        mImageViewRefresh = (ImageView) findViewById(R.id.imageview_refresh);
        mImageViewMenu = (ImageView) findViewById(R.id.main_menu);

        mTextViewTitle = (TextView) findViewById(R.id.text_main);

        mRelativeLayoutMore = (RelativeLayout) findViewById(R.id.adView_1);
        main = (RelativeLayout) findViewById(R.id.main);

        mLinearLayoutButtons = (LinearLayout) findViewById(R.id.linear_menu);
        mLinearLayoutCart = (LinearLayout) findViewById(R.id.cart);
        mLinearLayoutNoInternet = (LinearLayout) findViewById(R.id.linear_dialog_view_internet);

        mRecyclerView = (RecyclerView) findViewById(R.id.card_recycler_view_users);
        mRecyclerViewStrangers = (RecyclerView) findViewById(R.id.card_recycler_view_stranger);

        progressFriend = findViewById(R.id.linear_dialog_view);
        progressStrangers = findViewById(R.id.linear_dialog_view_stranger);
        no_user_dialog = findViewById(R.id.linear_no_user_dialog);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        mScrollViewStranger = (ScrollView) findViewById(R.id.scroll_facebook_friend);

        mWebViewFacebook = (WebView) findViewById(R.id.webview_facebook);
        mWebViewFacebookStrangers = (WebView) findViewById(R.id.webview_facebook_friend);

        shareDialog = new ShareDialog(this);
        callbackManager = CallbackManager.Factory.create();
        shareDialog.registerCallback(callbackManager, callback);


        AnalyticsApplication application = (AnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        Log.i("analytics:", "MainScreen: " + name);
        mTracker.setScreenName("Image~" + name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());


        dialogInternet = new Dialog(MainActivity.this);

        loadingCount = 0;
        loadingCountStranger = 0;

        checkInternetConnection();
        x = 0;
        y = 0;


        Advertisment();

        if (mSharePreferencePermanentShare.isShareonFb(mSharePreference.getmStringUserName()))
            SHOWUSERS = 8;

        if (mSharePreferencePermanentRate.isRate(mSharePreference.getmStringUserName()))
            SHOWUSERS = 11;

        isfirstclick = false;
        isListFinished = false;
        buttonClicked = false;
        mTimer = new Timer();
        callinback = false;
        isBoughtItem1 = false;
        isBoughtItem2 = false;
        isBoughtItem3 = false;
        isBoughtRemoveAd = false;
        isBoughtPeople = false;
        isDestroy = false;
        isprocessed = false;
        callmethod = false;
        isTokenExpire = false;
        isFacebookCalling = false;
        isFacebookCallingStrange = false;
        webStranger = false;
        webUsers = false;
        isRunning = false;
        isRunningStranger = false;
        callstrangers = false;
        count = 0;
        clickShare = false;
        activityDestroyed = false;
        int spanCount = 1; // 3 columns
        int spacing = 8; // 50px
        boolean includeEdge = true;
        isOpenScreen = true;


        mViewers = new ArrayList<Viewers>();
        mViewersPeople = new ArrayList<Viewers>();
        mHashMap = new HashMap<String, String>();
        fbIds = new ArrayList<String>();
        nameList = new ArrayList<String>();
        navDrawerItems = new ArrayList<NavDrawerItem>();

        initIABSetup();

        mUploadFeed = new UploadFeed();
        mUploadFeedKnow = new UploadFeedKnow();


        sp1 = getSharedPreferences("rating", 0);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        addDrawerItems();

        mDrawerList.setAdapter(new NavDrawerListAdapter(navDrawerItems, MainActivity.this));

//        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());


        mDataAdapter = new DataAdapter(this, mViewers);
        mDataAdapterStrangers = new DataAdapterStrangers(this, mViewersPeople);


        if (!mSharePreference.isNotificationLocal()) {
            setNotification();
            mSharePreference.setNotificationLocal(true);
        }

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
        RecyclerView.LayoutManager layoutManagerStrangers = new GridLayoutManager(getApplicationContext(), 1);

        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

        mRecyclerViewStrangers.setLayoutManager(layoutManagerStrangers);
        mRecyclerViewStrangers.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));

        mRecyclerView.setAdapter(mDataAdapter);
        mRecyclerViewStrangers.setAdapter(mDataAdapterStrangers);

        ShowDialogPop();

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        mButtonWhoView.setBackgroundResource(R.drawable.img_selected);
        mButtonWhoPeople.setBackgroundResource(R.drawable.img_unselected);
        mButtonWhoPeople.setTextColor(getResources().getColor(R.color.blue_theme_color));

        webviewcode();

        mRecyclerView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                x = event.getX();
                y = event.getY() - 50;
                return false;
            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                       friendsListClick(position);
                    }
                })
        );

        mRecyclerViewStrangers.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        strangerListClick(position);
                    }
                })
        );

                     mLinearLayoutCart.setOnClickListener(this);
                     mImageViewMenu.setOnClickListener(this);
                     mButtonWhoView.setOnClickListener(this);
                     mButtonWhoPeople.setOnClickListener(this);
                     mImageViewRefresh.setOnClickListener(this);


        long time = tinydb.getLong(LOCALTIME,0);

        if(time>0) {
            Time t1 = new Time();
            t1.set(time);
            Time t2 = new Time();
            t2.set(System.currentTimeMillis());

            long diff = TimeUnit.MILLISECONDS.toMinutes(t2.toMillis(true)-t1.toMillis(true));
            if(diff>REFRESHLOADTIME)
                WebData();
            else
            {
                isRunning = true;
                mViewers = tinydb.getListObject(STORELIST,Viewers.class);
                TAGS.mViewersMainScreen= tinydb.getListObject(STORELIST, Viewers.class);
                TAGS.mStalkerUsers= tinydb.getListObject(STORELIST, Viewers.class);

                if(mViewers!=null && mViewers.size()>0) {
                    setShowHideUsers();
                    setList();
                }
                else
                {
                    tinydb.putLong(LOCALTIME, System.currentTimeMillis());
                    WebData();
                }
            }
        }
        else {

            tinydb.putLong(LOCALTIME, System.currentTimeMillis());
            WebData();
        }

    }

    public void cacheStrangerList()
    {
        long time = tinydb.getLong(LOCALTIME_STRAGNER,0);
        if(time>0) {
            Time t1 = new Time();
            t1.set(time);
            Time t2 = new Time();
            t2.set(System.currentTimeMillis());

            long diff = TimeUnit.MILLISECONDS.toMinutes(t2.toMillis(true)-t1.toMillis(true));
            if(diff>REFRESHLOADTIME)
                Click_On_People();
            else
            {
                isRunningStranger = true;
                if(mViewersPeople.size()==0) {
                    mViewersPeople = tinydb.getListObject(STORELIST_STRANGER, Viewers.class);

                }

                if( mViewersPeople!=null && mViewersPeople.size()>0) {
                    mDataAdapterStrangers = new DataAdapterStrangers(MainActivity.this, mViewersPeople);
                    mRecyclerViewStrangers.setAdapter(mDataAdapterStrangers);
                    setStrangerList();
                    Click_On_People();
                }
                else
                {
                    tinydb.putLong(LOCALTIME_STRAGNER, System.currentTimeMillis());
                    Click_On_People();
                }
            }
        }
        else {

            tinydb.putLong(LOCALTIME_STRAGNER, System.currentTimeMillis());
            Click_On_People();
        }
    }

    public void friendsListClick(int position){
        if (!isfirstclick && mViewers !=null && mViewers.size()>position) {
            isfirstclick = true;
            Viewers mViewers1 = mViewers.get(position);

            if (position > 5 && position < 9) {
                if (!mSharePreferencePermanentShare.isShareonFb(mSharePreference.mStringUserName)) {
                    dialog_purchase();
                } else {

                    dialog_user(R.style.thirdanimation, mViewers1);
                }
            } else if(position<=5) {

                dialog_user(R.style.thirdanimation, mViewers1);
            }
            else if (position > 8 && position < 12) {
                if (!mSharePreferencePermanentRate.isRate(mSharePreference.getmStringUserName())) {
                    dialog_purchase();
                } else {
                    dialog_user(R.style.thirdanimation, mViewers1);
                }
            }
            else
            {
                if (position <= MainActivity.SHOWUSERS)
                {
                    dialog_user(R.style.thirdanimation, mViewers1);
                }
                else {
                    dialog_purchase();
                }
            }

            Timer mTimer = new Timer();

            mTimer.schedule(new TimerTask() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    isfirstclick = false;
                }
            }, 3000);

        }
    }

    public void strangerListClick(int position){

        Viewers mViewers = mViewersPeople.get(position);

        if (position < 3) {

            dialog_user(R.style.thirdanimation, mViewers);
        } else if (position < 15) {
            if (mSharePreferencePermanentpurchasestranger.isPuchasePeople()) {
                dialog_user(R.style.thirdanimation, mViewers);
            } else {
                dialog_purchase_stranger();

            }
        } else if (position < 25) {
            if (mSharePreferencePermanentpurchasestranger.isPuchasePeople25()) {

                dialog_user(R.style.thirdanimation, mViewers);
            } else {
                dialog_purchase_stranger();

            }
        } else if (position >= 25) {
            if (mSharePreferencePermanentpurchasestranger.isPuchasePeople50()) {
                dialog_user(R.style.thirdanimation, mViewers);
            } else {
                dialog_purchase_stranger();

            }
        }
    }

    public void Advertisment(){
        adRequest = new AdRequest.Builder().build();
        mAdView = (AdView) findViewById(R.id.adView_id);
        interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId(getResources().getString(R.string.adUnitId_intersitial));

        mtimerAds = new CounterClass(100000000, 20000);

        mSharePreference = new SharePreference(this);
        mSharePreferencePermanent = new SharePreferencePermanent(this);
        mSharePreferencePermanentRate = new SharePreferencePermanent(this);
        mSharePreferencePermanentShare = new SharePreferencePermanent(this);
        mSharePreferencePermanentadd = new SharePreferencePermanent(this);
        mSharePreferencePermanentpurchasefriend = new SharePreferencePermanent(this);
        mSharePreferencePermanentpurchasestranger = new SharePreferencePermanent(this);
        if (mSharePreferencePermanentadd.isAds()) {
            activityDestroyed = true;
            mAdView.setVisibility(View.GONE);
        }
    }

    public class CounterClass extends CountDownTimer {
        public CounterClass(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {

        }


        @Override
        public void onTick(long millisUntilFinished) {

            if (!mSharePreferencePermanentadd.isAds() && !activityDestroyed && !isDestroy && isOpenScreen) {
//                interstitial.loadAd(adRequest);
//                interstitial.show();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isDestroy = true;
        isOpenScreen = false;
        mtimerAds.cancel();

        TAGS.userList = true;

        if (br != null) {
            unregisterReceiver(br);
        }

        if (mUploadFeed != null)
            mUploadFeed.cancel(true);

        if (mUploadFeedKnow != null)
            mUploadFeedKnow.cancel(true);


        progressFriend.setVisibility(View.GONE);
        view_mainss.setVisibility(View.GONE);
    }

    public void WebData() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {

            progressFriend.setVisibility(View.VISIBLE);
            fbIds.clear();
            nameList.clear();
            isFacebookCalling = false;
            isRunning = true;

            mWebViewFacebook.loadUrl(getNativeKey1());

        }

    }

    private void dialog_user(int stylename, Viewers mViewers) {

        mDialog = new Dialog(MainActivity.this, stylename);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        mDialog.setContentView(R.layout.dialog_user);
        ImageView mImageView = (ImageView) mDialog.findViewById(R.id.image);
        TextView mTextView = (TextView) mDialog.findViewById(R.id.text);

        Glide.with(MainActivity.this).
                load(getimage() + mViewers.getId() + getimage1() + image3()).
                placeholder(R.drawable.ic_blur).
                into(mImageView);

        String str =mViewers.getName();
        String[] splitStr = str.split("\\s+");
//            viewHolder.mTextViewItemName.setText(index+". "+splitStr[0]);
        mTextView.setText(splitStr[0]);
//        mTextView.setText(mViewers.getName());
      //  mTextView.setText(mTextView.getText().toString().toUpperCase());
        mDialog.show();
    }

    public void refreshClick() {
        runOnUiThread(new Runnable() {
            public void run() {
                if (!buttonClicked) {
                    buttonClicked = true;
                    Timer timertemp = new Timer();

                    timertemp.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            buttonClicked = false;
                        }
                    }, 10000);


                    if (mRecyclerView.getVisibility() == View.VISIBLE) {
                        if (mRecyclerView.getVisibility() == View.VISIBLE) {
                            if (isRunning) {
                                if (mUploadFeed.getStatus() == Status.RUNNING) {
                                    isDestroy = true;
                                    if (mUploadFeed != null)
                                        mUploadFeed.cancel(true);
                                }

                                Timer mTimernew = new Timer();

                                mTimernew.schedule(new TimerTask() {

                                    @Override
                                    public void run() {

                                        MainActivity.this.runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {
                                                loadingCount=0;
                                                refreshfriends();
                                            }
                                        });

                                    }
                                }, 500);
                            } else {
                                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.bk_process_msg));
                            }

                        } else {

                            loadingCount=0;
                            WebSettings webSettings = mWebViewFacebook.getSettings();


                            webSettings.setUserAgentString(mainweb());

                            webSettings.setBuiltInZoomControls(false);
                            webSettings.setLoadsImagesAutomatically(false);

                            webSettings.setJavaScriptEnabled(true);
                            webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
                            //webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);

                           // mWebViewFacebook.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                            mWebViewFacebook.setWebViewClient(new Callback()); // HERE IS THE MAIN CHANGE
                            mWebViewFacebook.addJavascriptInterface(new MyJavaScriptInterface(), getNativeKey3());

                            mWebViewFacebook.loadUrl(getNativeKey1());

                        }

                    } else if (mRecyclerViewStrangers.getVisibility() == View.VISIBLE || progressStrangers.getVisibility() == View.VISIBLE) {
                        if (mRecyclerViewStrangers.getVisibility() == View.VISIBLE) {
                            if (isRunningStranger) {
                                if (mUploadFeedKnow.getStatus() == Status.RUNNING) {
                                    isDestroy = true;
                                    if (mUploadFeedKnow != null)
                                        mUploadFeedKnow.cancel(true);
                                }


                                Timer mTimernew = new Timer();

                                mTimernew.schedule(new TimerTask() {

                                    @Override
                                    public void run() {


                                        MainActivity.this.runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {

                                                loadingCountStranger=0;

                                                refreshstranger();
                                            }
                                        });

                                    }
                                }, 500);
                            } else {
                                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.failed_login));
                            }
                        }

                    }
                } else {
                    Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.bk_process_msg));
                }
            }
        });
    }

    public void refreshstranger()
    {
        progressStrangers.setVisibility(View.VISIBLE);
        view_mainss.setVisibility(View.VISIBLE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        isRunningStranger = true;
        isFacebookCallingStrange = false;
        webStranger = false;
        callstrangers = false;
        mViewersPeople.clear();
        mDataAdapterStrangers.notifyDataSetChanged();
        mRecyclerViewStrangers.setVisibility(View.GONE);
        isDestroy = false;

        WebSettings webSettingsFriend = mWebViewFacebookStrangers.getSettings();
        webSettingsFriend.setUserAgentString(mainweb());
        webSettingsFriend.setBuiltInZoomControls(false);
        webSettingsFriend.setLoadsImagesAutomatically(false);
        webSettingsFriend.setJavaScriptEnabled(true);
        webSettingsFriend.setJavaScriptCanOpenWindowsAutomatically(true);

        // webSettingsFriend.setRenderPriority(WebSettings.RenderPriority.HIGH);

        // mWebViewFacebookStrangers.setLayerType(View.LAYER_TYPE_HARDWARE, null);

        mWebViewFacebookStrangers.setWebViewClient(new CallbackFriend()); // HERE IS THE MAIN CHANGE
        mWebViewFacebookStrangers.addJavascriptInterface(new MyJavaScriptInterfaceFriend(), getNativeKey3());


        mWebViewFacebookStrangers.loadUrl(getNativeKey4());
        mRelativeLayoutMore.setVisibility(View.GONE);
        mLinearLayoutButtons.setVisibility(View.GONE);
    }

    public void refreshfriends()
    {
        progressFriend.setVisibility(View.VISIBLE);
        view_mainss.setVisibility(View.VISIBLE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        isFacebookCalling = false;
        callmethod = false;
        mViewers.clear();
        mDataAdapter.notifyDataSetChanged();
        mRecyclerView.setVisibility(View.GONE);
        isListFinished = false;
        isDestroy = false;
        webUsers = false;
        WebSettings webSettings = mWebViewFacebook.getSettings();

        webSettings.setUserAgentString(mainweb());

        webSettings.setBuiltInZoomControls(false);
        webSettings.setLoadsImagesAutomatically(false);

        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        // webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);

        //  mWebViewFacebook.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mWebViewFacebook.setWebViewClient(new Callback()); // HERE IS THE MAIN CHANGE
        mWebViewFacebook.addJavascriptInterface(new MyJavaScriptInterface(), getNativeKey3());

        mWebViewFacebook.loadUrl(getNativeKey1());
        mRelativeLayoutMore.setVisibility(View.GONE);
        mLinearLayoutButtons.setVisibility(View.GONE);
    }

    public void webviewcode() {
        WebSettings webSettings = mWebViewFacebook.getSettings();


        webSettings.setUserAgentString(mainweb());

        webSettings.setBuiltInZoomControls(false);
        webSettings.setLoadsImagesAutomatically(false);

        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
       // webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);

      //  mWebViewFacebook.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mWebViewFacebook.setWebViewClient(new Callback()); // HERE IS THE MAIN CHANGE
        mWebViewFacebook.addJavascriptInterface(new MyJavaScriptInterface(), getNativeKey3());

        WebSettings webSettingsFriend = mWebViewFacebookStrangers.getSettings();


        webSettingsFriend.setUserAgentString(mainweb());

        webSettingsFriend.setBuiltInZoomControls(false);
        webSettingsFriend.setLoadsImagesAutomatically(false);

        webSettingsFriend.setJavaScriptEnabled(true);
        webSettingsFriend.setJavaScriptCanOpenWindowsAutomatically(true);
       // webSettingsFriend.setRenderPriority(WebSettings.RenderPriority.HIGH);

      //  mWebViewFacebookStrangers.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        mWebViewFacebookStrangers.setWebViewClient(new CallbackFriend()); // HERE IS THE MAIN CHANGE
        mWebViewFacebookStrangers.addJavascriptInterface(new MyJavaScriptInterfaceFriend(), getNativeKey3());

        circularProgress = (WaveLoadingView) progressFriend.findViewById(R.id.circular);

        circularProgress.setProgressValue(10);
        circularProgress.setCenterTitle("10%");

        circularProgressStranger = (WaveLoadingView) progressStrangers.findViewById(R.id.circular);

        circularProgressStranger.setProgressValue(10);
        circularProgressStranger.setCenterTitle("10%");



    }

    private class Callback extends WebViewClient { // HERE IS THE MAIN CHANGE.

        Timer mTimer = new Timer();

        int progress = 5;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            Log.e("url main ",url);

            if (!isFacebookCalling) {
                progress = 5;
                mTimer = new Timer();

                isFacebookCalling = true;

                circularProgress.setProgressValue(5);
                circularProgress.setCenterTitle("5%");
                mTimer.scheduleAtFixedRate(new TimerTask() {

                    @Override
                    public void run() {
                        // TODO Auto-generated method stub

                        MainActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                if (progress < 40 && !callmethod) {
                                    progress += 5;

                                    circularProgress.setProgressValue(progress);
                                    circularProgress.setCenterTitle(progress + "%");
                                } else {
                                    mTimer.cancel();
                                }
                            }
                        });


                    }
                }, 1000, 1500);

            }

        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mTimer.cancel();
            progress = 100;


//            circularProgress.setProgressValue(progress);
//            circularProgress.setCenterTitle(+progress + "%");
            if (!webUsers) {
                webUsers = true;

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isRunning) {
                            isRunning = false;
                            mWebViewFacebook.loadUrl(finalkey());

                        }
                    }
                });

            }

        }
    }

    private class CallbackFriend extends WebViewClient { // HERE IS THE MAIN CHANGE.

        Timer mTimer = new Timer();

        int progress = 5;

        int t = 0;

        boolean isFinished = false;

        @Override
        public void onPageStarted(final WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            Log.e("url main ",url);

            if (!isFacebookCallingStrange) {
                progress = 5;
                mTimer = new Timer();

                isFacebookCallingStrange = true;


                circularProgressStranger.setProgressValue(5);
                circularProgressStranger.setCenterTitle("5%");
                mTimer.scheduleAtFixedRate(new TimerTask() {

                    @Override
                    public void run() {

                        MainActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {

                                if (progress < 40 && !callstrangers) {
                                    progress += 3;


                                    circularProgressStranger.setProgressValue(progress);
                                    circularProgressStranger.setCenterTitle(progress + "%");
                                    view.scrollTo(0, view.getContentHeight());
                                } else {
                                    mTimer.cancel();
                                }
                            }
                        });


                    }
                }, 1000, 500);

            }

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }

        @Override
        public void onPageFinished(final WebView view, String url) {

            mTimer.cancel();

            isFinished = true;

            if (!webStranger) {
                webStranger = true;
                final Timer timer = new Timer();

                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isRunningStranger) {
                                    if (t < 12) {
                                        mScrollViewStranger.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                mScrollViewStranger.fullScroll(ScrollView.FOCUS_DOWN);
                                            }
                                        });
                                        if (progress < 50) {
                                            progress += 3;
                                            circularProgressStranger.setProgressValue(progress);
                                            circularProgressStranger.setCenterTitle(progress + "%");
                                        }

                                        t++;
                                    } else {
                                        timer.cancel();
                                        mTimer.cancel();
                                    }

                                }
                            }
                        });

                    }
                }, 0, 400);


                Timer timerdelay = new Timer();

                timerdelay.schedule(new TimerTask() {
                    @Override
                    public void run() {

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (isRunningStranger) {
                                    isRunningStranger = false;
                                    mWebViewFacebookStrangers.loadUrl(finalkey());

                                }
                            }
                        });

                    }
                }, 5000);
            }
        }
    }


    class MyJavaScriptInterface {

        boolean isprocessed = false;

        @SuppressLint("NewApi")
        @JavascriptInterface
        public void processHTML(String html) {
            if (isprocessed) return;


            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {


                }
            });

            mUploadFeed = new UploadFeed();


            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mUploadFeed.execute(html);
            } else {

                mUploadFeed.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, html);
            }


        }
    }

    class MyJavaScriptInterfaceFriend {

        boolean isprocessed = false;

        @SuppressLint("NewApi")
        @JavascriptInterface
        public void processHTML(String html) {
            if (isprocessed) return;


            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {


                    circularProgressStranger.setProgressValue(45);
                    circularProgressStranger.setCenterTitle("45%");
                }
            });

            mUploadFeedKnow = new UploadFeedKnow();


            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mUploadFeedKnow.execute(html);
            } else {

                mUploadFeedKnow.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, html);
            }


        }
    }


    public class UploadFeed extends AsyncTask<String, Void, Void> {


        boolean isLogout = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                }
            });
            isLogout = false;

        }

        @Override
        protected Void doInBackground(String... params) {
            String html = params[0];
            if (html != null) {

                isprocessed = true;

                int begin = html.indexOf(getNativeKey5());


                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        circularProgress.setProgressValue(50);
                        circularProgress.setCenterTitle("50%");

                    }
                });

                if (begin < 0) {

                    isLogout = true;

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            fbIds.clear();
                            nameList.clear();
                            if (!MainActivity.this.isFinishing())
                                logout(getString(R.string.logout_mssg));
                            return;
                        }
                    });

                }

                if (!isLogout) {
                    if (begin > 0 && (html.length() > (begin + Integer.parseInt(getstringvalue())))) {

                        String newString = html.substring(begin, begin + Integer.parseInt(getstringvalue()));
                        String s = getNativeKey6();
                        String news = "\"" + s + "\"" + ":[";


                        if (newString.contains(news)) {
                            begin = html.indexOf(news, begin);
                            int end = html.indexOf("]", begin);


                            String dump = html.substring(begin + Integer.parseInt(getstringvalue1()), end);
                            mainString = dump;


                            if (!callmethod && !isLogout) {
                                callmethod = true;
                                if(!MainActivity.this.isFinishing())
                                    extractListFromHTMLMultiple(dump);
                            }

                        } else {

                            begin = html.indexOf("{", begin);
                            begin = html.indexOf(getNativeKey7(), begin);


                            int end = html.indexOf("]", begin);


                            String dump = html.substring(begin + Integer.parseInt(getstringvalue2()), end);
                            mainString = dump;


                            if (dump.equals("")) {
                                String mString = getNativeKey8();
                                String chatstring = "\"" + mString + "\"";


                                int chatindex = html.indexOf(chatstring);
                                int endchat = html.indexOf("]", (chatindex + mString.length()) + Integer.parseInt(getstringvalue3()));
                                String dumpnew = html.substring(((chatindex + mString.length()) + Integer.parseInt(getstringvalue4())), endchat);
                                if (!callmethod) {
                                    callmethod = true;
                                    if(!MainActivity.this.isFinishing())
                                        extractListFromHTMLMultiple(dumpnew);
                                }
                            } else {
                                if (!callmethod) {
                                    callmethod = true;
                                    if(!MainActivity.this.isFinishing())
                                        extractListFromHTMLMultiple(dump);
                                }
                            }
                        }

                    } else {
                        MainActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                fbIds.clear();
                                nameList.clear();
                                if (!MainActivity.this.isFinishing() && !isLogout)
                                    logout(getString(R.string.failed_login));
                                return;
                            }
                        });
                    }

                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            if (!MainActivity.this.isFinishing() && !isDestroy) {

                if(!isTokenExpire && !isDestroy){
                    setDataList();}
                else if (!isDestroy) {
                    logout(getString(R.string.failed_login));
                    isTokenExpire=false;
                }

               // view_mainss.setVisibility(View.GONE);
               // progressFriend.setVisibility(View.GONE);


                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                if (!mSharePreferencePermanentadd.isAds()) {
                    if(!isDestroy && interstitial!=null) {
                      //  mAdView.loadAd(adRequest);
//                        interstitial.loadAd(adRequest);
                        if (mtimerAds == null && !isDestroy) {
                            mtimerAds = new CounterClass(100000000, 20000);
                        }
                        if(!isDestroy && interstitial!=null)
                            mtimerAds.start();
                    }
                }

                if(!TAGS.userList)
                {
                    progressFriend.setVisibility(View.GONE);
                    no_user_dialog.setVisibility(View.VISIBLE);
                    mImageViewRefresh.setVisibility(View.GONE);
                    mTextViewTitle.setVisibility(View.GONE);
                }

//                if (TAGS.mViewersMainScreen.size() > 0) {
//
//                    } else {
//                    progressFriend.setVisibility(View.GONE);
//                    no_user_dialog.setVisibility(View.VISIBLE);
//                    mImageViewRefresh.setVisibility(View.GONE);
//                    mTextViewTitle.setVisibility(View.GONE);
//                    }


                isRunning = true;

            }
        }
    }

    private void extractListFromHTMLMultiple(String dump) {
        if (dump != null) {

            mViewers.clear();
            TAGS.mStalkerUsers.clear();
            TAGS.mViewersMainScreen.clear();
            mHashMap.clear();


            String[] mStringUserId = dump.split(getsplit());
            int u = 0;
            i = 0;
            String tempNumberCheck = mStringUserId[0].substring(1, mStringUserId[0].length() - 3);

            if (mStringUserId[0] == null || mStringUserId[0].equals("") || !TextUtils.isDigitsOnly(tempNumberCheck)) {
                TAGS.userList = false;
            } else {
                String templistUserIds = "";
                String templistUserIds50 = "";
                String templistUserIds150 = "";
                String templistUserIds200 = "";

                for (String userFbId : mStringUserId) {
                    if (!isDestroy) {
                        if (userFbId == null || userFbId.length() < 4) continue;
                        userFbId = userFbId.substring(1, userFbId.length() - 3);
                        if (userFbId.length() > 10) {
                            if (!mHashMap.containsKey(userFbId)) {

                                mHashMap.put(userFbId, userFbId);

                                if (i < 50) {
                                    if (templistUserIds.length() == 0)
                                        templistUserIds = userFbId;
                                    else
                                        templistUserIds = templistUserIds + getsplit() + userFbId;
                                } else if (i > 49 && i < 100) {
                                    if (templistUserIds50.length() == 0)
                                        templistUserIds50 = userFbId;
                                    else
                                        templistUserIds50 = templistUserIds50 + getsplit() + userFbId;
                                } else if (i > 99 && i < 150) {
                                    if (templistUserIds150.length() == 0)
                                        templistUserIds150 = userFbId;
                                    else
                                        templistUserIds150 = templistUserIds150 + getsplit() + userFbId;
                                } else {
                                    if (templistUserIds200.length() == 0)
                                        templistUserIds200 = userFbId;
                                    else
                                        templistUserIds200 = templistUserIds200 + getsplit() + userFbId;
                                }
                                i = i + 1;

                            }
                        }
                        if (i >= userList) break;
                    } else {

                        break;
                    }
                }

                MainActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        circularProgress.setProgressValue(50);
                        circularProgress.setCenterTitle("50%");

                    }
                });
                count = 0;

                if(!MainActivity.this.isFinishing()) {
                    getUserInfo(templistUserIds, mViewers);
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            circularProgress.setProgressValue(60 + (count * 10));
                            circularProgress.setCenterTitle(60 + (count * 10) + "%");
                        }
                    });

                    getUserInfo(templistUserIds50, mViewers);
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            circularProgress.setProgressValue(70 + (count * 10));
                            circularProgress.setCenterTitle(70 + (count * 10) + "%");
                        }
                    });

//                    circularProgress.setProgressValue(70 + (count * 10));
//                    circularProgress.setCenterTitle(70 + (count * 10) + "%");

                    getUserInfo(templistUserIds150, mViewers);
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            circularProgress.setProgressValue(80 + (count * 10));
                            circularProgress.setCenterTitle(80 + (count * 10) + "%");
                        }
                    });

//                    circularProgress.setProgressValue(80 + (count * 10));
//                    circularProgress.setCenterTitle(80 + (count * 10) + "%");

                    getUserInfo(templistUserIds200, mViewers);
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            circularProgress.setProgressValue(100);
                            circularProgress.setCenterTitle(100 +  "%");
                        }
                    });

                    setShowHideUsers();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if(!MainActivity.this.isFinishing()) {

                }
            }
        });
    }

    public void getUserInfo(String mStringsIds,ArrayList<Viewers> mArraylist){

        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();

        String result = null;

        HttpGet httpGet = null;
        httpGet = new HttpGet(geturl() + mStringsIds + geturl1() + mSharePreference.getmStringFacebookToken());

        HttpResponse response = null;
        try {
            response = httpClient.execute(httpGet, localContext);
        } catch (ClientProtocolException e) {
            result = null;
            e.printStackTrace();

        } catch (IOException e) {
            result = null;
            e.printStackTrace();

        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }
        BufferedReader reader;
        try {
            reader = new BufferedReader(
                    new InputStreamReader(
                            response.getEntity().getContent()
                    )
            );
            String line = null;
            result = "";
            while ((line = reader.readLine()) != null) {
                result += line + "\n";
            }

        } catch (IllegalStateException e) {
            result = null;
            e.printStackTrace();

        } catch (IOException e) {
            result = null;
            e.printStackTrace();

        }catch(NullPointerException e){
            result = null;
            e.printStackTrace();
        }
        if(result != null)getUserInfoParsing(mStringsIds,result,mArraylist);
    }


    public void getUserInfoParsing(String mStringIds,String result,ArrayList<Viewers> mArrayList ){
        String[] mStringFbIds = mStringIds.split(getsplit());
        for (int t = 0; t < mStringFbIds.length; t++) {

            JSONObject jObject = null;
            try {
                jObject = new JSONObject(result);


                if (jObject.has(mStringFbIds[t])) {

                    JSONObject mJsonObject = jObject.getJSONObject(mStringFbIds[t]);

                    String resultanme = mJsonObject.getString(getconstant());

                    Viewers mViewersItem = new Viewers();
                    Viewers mViewersItemStalker = new Viewers();
                    Viewers mViewersMainScreen = new Viewers();

                    if (resultanme != null) {
                        mViewersItem.setName(resultanme);
                        mViewersItemStalker.setName(resultanme);
                        mViewersMainScreen.setName(resultanme);
                    } else {
                        mViewersItem.setName("");
                        mViewersItemStalker.setName("");
                        mViewersMainScreen.setName("");
                    }

                    mViewersItem.setId(mStringFbIds[t]);
                    mViewersMainScreen.setId(mStringFbIds[t]);

                    mViewersItem.setImage(getimage() + mStringFbIds[t] + getimage1());
                    mViewersMainScreen.setImage(getimage() + mStringFbIds[t] + getimage1());

                    mViewersItemStalker.setId(mStringFbIds[t]);
                    mViewersItemStalker.setImage(getimage() + mStringFbIds[t] + getimage1());

                    mArrayList.add(mViewersItem);
                    TAGS.mStalkerUsers.add(mViewersItemStalker);
                    TAGS.mViewersMainScreen.add(mViewersMainScreen);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    public void setShowHideUsers(){
        for(int i = 0; i < mViewers.size();i++){
            Viewers viewer = mViewers.get(i);
            if (i < 6) {
                viewer.setShow(true);
            }
            if (i > 5 && i < 9) {
                if (!mSharePreferencePermanentShare.isShareonFb(mSharePreference.getmStringUserName())) {
                    viewer.setImageId(R.drawable.img_purchase_facebook);
                    viewer.setShow(false);
                    viewer.setSharename(getResources().getString(R.string.share_adapter));
                } else
                    viewer.setShow(true);
            }

            if (i > 8 && i < 12) {
                if (!mSharePreferencePermanentRate.isRate(mSharePreference.getmStringUserName())) {
                    viewer.setImageId(R.drawable.img_purchase_star);
                    viewer.setShow(false);
                    viewer.setSharename(getResources().getString(R.string.rate1));
                } else
                    viewer.setShow(true);
            }

            if (i < 35) {
                if (mSharePreferencePermanentpurchasefriend.isPuchaseItem35())
                    viewer.setShow(true);
                else {
                    if (i > 11 && i < 35) {
                        viewer.setImageId(R.drawable.img_purchase_lock);
                        viewer.setShow(false);
                        viewer.setSharename(getResources().getString(R.string.unlock));
                    }
                }
            }
            if (i >= 35 && i < 50) {
                if (mSharePreferencePermanentpurchasefriend.isPuchaseItem50())
                    viewer.setShow(true);
                else {
                    viewer.setImageId(R.drawable.img_purchase_lock);
                    viewer.setShow(false);
                    viewer.setSharename(getResources().getString(R.string.unlock));
                }
            }

            if (i >= 50 &i<=99) {
                if (mSharePreferencePermanentpurchasefriend.isPuchaseItem100())
                    viewer.setShow(true);
                else {
                    viewer.setImageId(R.drawable.img_purchase_lock);
                    viewer.setShow(false);
                    viewer.setSharename(getResources().getString(R.string.unlock));
                }
            }
            if (i >= 100) {
                if (mSharePreferencePermanentpurchasefriend.isPuchaseItem200())
                    viewer.setShow(true);
                else {
                    viewer.setImageId(R.drawable.img_purchase_lock);
                    viewer.setShow(false);
                    viewer.setSharename(getResources().getString(R.string.unlock));
                }
            }
        }
    }

    public void setList(){
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mDataAdapter.notifyDataSetChanged();

        Animation animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.translate);

        mDataAdapter = new DataAdapter(MainActivity.this,mViewers);
        mRecyclerView.setAdapter(mDataAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
      //  mRecyclerView.startAnimation(animFadein);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        progressFriend.setVisibility(View.GONE);
        view_mainss.setVisibility(View.GONE);
        mRelativeLayoutMore.setVisibility(View.VISIBLE);
        mLinearLayoutButtons.setVisibility(View.VISIBLE);
        mImageViewRefresh.setVisibility(View.VISIBLE);



    }

    public void setDataList(){

        if (TAGS.userList) {

            if(mViewers.size()==0) {
                if(loadingCount<2) {
                    loadingCount++;
                 //   showAlert(getString(R.string.loading_title), getString(R.string.loading_message));
                    isRunning = true;
                    if(!isDestroy)
                        refreshfriends();

                }
                else
                {
                    if(!isDestroy)
                        showAlertClose(getString(R.string.loading_title), getString(R.string.loading_message_restart));
                }
            }
            else {
                tinydb.putListObject(STORELIST, mViewers);
                tinydb.putLong(LOCALTIME, System.currentTimeMillis());
                setList();
                if(!MainActivity.this.isFinishing()) {
                    mDialogPopup.show();
                }
            }

        } else {
            no_user_dialog.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            mLinearLayoutButtons.setVisibility(View.GONE);
            mImageViewRefresh.setVisibility(View.GONE);
            mTextViewTitle.setVisibility(View.GONE);
        }
    }

    public class UploadFeedKnow extends AsyncTask<String, Void, Void> {


        boolean isLogout = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            MainActivity.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {
            String html = params[0];
            if (html != null) {

                isprocessed = true;

                int begin = html.indexOf(getNativeKey9());

                boolean flag = false;

                if (begin < 0) {
                    isLogout = true;
                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            fbIds.clear();
                            nameList.clear();
                            if (!MainActivity.this.isFinishing())
                                logout(getResources().getString(R.string.logout_mssg));

                            return;

                        }
                    });

                }

                String tempname = "";
                String tempname50 = "";

                if (begin > 0)
                    mViewersPeople.clear();

                if (!isLogout) {
                    if (begin > 0) {

                        int count = 0;

                        Viewers viewer;
                        do {

                            if (begin > 0 && html.length() > begin) {
                                html = html.substring(begin);
                                int end1 = 0;
                                end1 = html.indexOf(getNativeKey10());
                                String id = html.substring(0, end1);

                                int start = id.indexOf(getNativeKey11());
                                String newid = id.substring(start + Integer.parseInt(getstringvalue6()), start + Integer.parseInt(getstringvalue7()));

                                start = newid.indexOf(" ");
                                newid = newid.substring(0, start - 1);

                                viewer = new Viewers();

                                String mutual = html.substring(0, end1);

                                int show = mutual.indexOf("is a mutual friend");

                                if (show > 0) {
                                    String mutualname = mutual.substring((show - 70), show);
                                    int first = mutualname.indexOf(sign1());
                                    int second = mutualname.indexOf(sign2());
                                    if (first > 0 && second > 0) {
                                        String mname = mutualname.substring(first + 1, second);
                                        viewer.setMulualname(mname);
                                        viewer.setShowmutual(true);
                                    }
                                } else {

                                    mutual = html.substring(end1);
                                    mutual = mutual.substring(0, mutual.indexOf(getNativeKey10()));

                                    show = mutual.indexOf(getNativeKey12());

                                    if (show > 0) {
                                        String mutualname = mutual.substring((show - 50), show + 5);
                                        int first = mutualname.indexOf(sign1());
                                        int second = mutualname.indexOf(sign2());
                                        if (first > 0 && second > 0) {
                                            String mname = mutualname.substring(first + 1, second);
                                            viewer.setMulualname(mname);
                                            viewer.setShowmutual(true);
                                        }
                                    }
                                }

                                viewer.setViewid(0);
                                viewer.setId(newid);

                                viewer.setImageId(R.drawable.img_purchase_lock);
                                viewer.setSharename("Unlock");

                                //
                                viewer.setName("");

                                if (count < 50) {

                                    if (newid.matches(numbers())) {
                                        if (tempname.length() == 0)
                                            tempname = newid;
                                        else
                                            tempname += getsplit() + newid;

                                    }
                                } else if (count < 100) {
                                    if (newid.matches(numbers())) {
                                        if (tempname50.length() == 0)
                                            tempname50 = newid;
                                        else
                                            tempname50 += getsplit() + newid;
                                    }
                                }

                                viewer.setImage(getimage() + newid + getimage1());

                                html = html.substring(end1);
                                begin = 0;
                                begin = html.indexOf(getNativeKey9());


                                if (count < 3)
                                    viewer.setShow(true);

                                if (mSharePreferencePermanentpurchasestranger.isPuchasePeople50()) {
                                    viewer.setShow(true);
                                }

                                if (mSharePreferencePermanentpurchasestranger.isPuchasePeople25() && count < 25) {
                                    viewer.setShow(true);
                                }

                                if (mSharePreferencePermanentpurchasestranger.isPuchasePeople() && count < 15) {
                                    viewer.setShow(true);
                                }
                                if (newid.matches(numbers())) {
                                    mViewersPeople.add(viewer);
                                    count = count + 1;
                                }
                            }
                            if (begin < 0)
                                flag = true;

                            if (count > 99)
                                flag = true;

                        } while (!flag);

                    }


                    MainActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            circularProgressStranger.setProgressValue(50);
                            circularProgressStranger.setCenterTitle("50%");
                        }
                    });

                    if (!callstrangers && !isLogout) {
                        callstrangers = true;
                        if(!MainActivity.this.isFinishing())
                            extractListpeoplemayknowMultiple(tempname, tempname50);


                        MainActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                circularProgressStranger.setProgressValue(100);
                                circularProgressStranger.setCenterTitle("100%");
                            }
                        });
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            setDataListStrangers();


        }

    }

    private void extractListpeoplemayknowMultiple(String temp, String temp50) {

        getUsersInfoStranger(temp,mViewersPeople,0);
        getUsersInfoStranger(temp50,mViewersPeople,1);
    }

    public void setStrangerList()
    {
        progressFriend.setVisibility(View.GONE);
        view_mainss.setVisibility(View.GONE);
        progressStrangers.setVisibility(View.GONE);
        mRecyclerViewStrangers.setVisibility(View.VISIBLE);

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        progressStrangers.setVisibility(View.GONE);
        view_mainss.setVisibility(View.GONE);


        mRelativeLayoutMore.setVisibility(View.VISIBLE);
        mLinearLayoutButtons.setVisibility(View.VISIBLE);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        isRunningStranger = true;

        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDataAdapterStrangers.notifyDataSetChanged();

            }
        });
    }

    public void setDataListStrangers(){

        if(mViewersPeople.size()==0) {
            if(loadingCountStranger<2) {
                loadingCountStranger++;
                showAlert(getString(R.string.loading_title), getString(R.string.loading_message));
                isRunningStranger = true;
                refreshstranger();

            }
            else
            {
                showAlertClose(getString(R.string.loading_title), getString(R.string.loading_message_restart));
            }
        }
        else {
            tinydb.putListObject(STORELIST_STRANGER, mViewersPeople);
            tinydb.putLong(LOCALTIME_STRAGNER, System.currentTimeMillis());
            setStrangerList();
            if(!MainActivity.this.isFinishing()) {
                mDialogPopup.show();
            }
        }

    }

    public void getUsersInfoStranger(String mStringsIds,ArrayList<Viewers> mArrayList,int t){


        HttpClient httpClient = new DefaultHttpClient();
        HttpContext localContext = new BasicHttpContext();

        String result = null;

        HttpGet httpGet = null;
        httpGet = new HttpGet(geturl() + mStringsIds + geturl1() + mSharePreference.getmStringFacebookToken());

        HttpResponse response = null;
        try {
            response = httpClient.execute(httpGet, localContext);
        } catch (ClientProtocolException e) {
            result = null;
            e.printStackTrace();

        } catch (IOException e) {
            result = null;
            e.printStackTrace();

        } catch (Exception e) {
            result = null;
            e.printStackTrace();
        }
        BufferedReader reader;
        if(response != null){
        try {
            reader = new BufferedReader(
                    new InputStreamReader(
                            response.getEntity().getContent()
                    )
            );
            String line = null;
            result = "";
            while ((line = reader.readLine()) != null) {
                result += line + "\n";
            }

        } catch (IllegalStateException e) {
            result = null;
            e.printStackTrace();

        } catch (IOException e) {
            result = null;
            e.printStackTrace();

        }
        }

        if (response != null) getUserInfoParsingStranger(mStringsIds, result, mArrayList,t);


    }

    public void getUserInfoParsingStranger(String mStringIds, String result, ArrayList<Viewers> mArrayList,int t1){
        String[] mStringFbIds = mStringIds.split(getsplit());
        int length =0;
        if(t1 == 1){
            length = mStringFbIds.length;
        }

        for (int t = 0; t < mStringFbIds.length; t++) {

            JSONObject jObject = null;
            try {
                jObject = new JSONObject(result);

                if (jObject.has(mStringFbIds[t])) {

                    JSONObject mJsonObject = jObject.getJSONObject(mStringFbIds[t]);

                    String resultanme = mJsonObject.getString(getconstant());


                    if (resultanme != null) {

                        if ((t+length)< mViewersPeople.size()) {
                            mViewersPeople.get(t+length).setName(resultanme);
                            if (resultanme.contains(" ") && resultanme.length() > 0)
                                mViewersPeople.get(t+length).setFirstname(resultanme.substring(0, resultanme.indexOf(" ")));
                            else
                                mViewersPeople.get(t+length).setFirstname(resultanme);
                        }
                    }
                    else
                    {
                        if ((t+length) < mViewersPeople.size()) {
                            mViewersPeople.get(t+length).setName("");
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public static Intent getOpenFacebookIntent(Context context, String id) {
        try {
            context.getPackageManager()
                    .getPackageInfo(FACEBOOK_KATANA_URL+"", 0); //Checks if FB is even installed.
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse(FACEBOOK_PROFILE + id)); //Trys to make intent with FB's URI
        } catch (Exception e) {
            return new Intent(Intent.ACTION_VIEW,
                    Uri.parse(FACEBOOK_ARKVERSE+"")); //catches and opens a url to the desired page
        }
    }


    public void showAlert(String title, String message) {

        alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setCancelable(false);
        alertDialog.setTitle(title);

        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                alertDialog.dismiss();
            }
        });
        if(!isDestroy) {
        alertDialog.show();
        }
    }

    public void showAlertClose(String title, String message) {

        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setCancelable(false);
        alertDialog.setTitle(title);

        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                alertDialog.dismiss();
                finishAffinity();
                Intent mIntent = new Intent(getApplicationContext(),MySplash.class);
                startActivity(mIntent);
            }
        });
        if(!isDestroy && !MainActivity.this.isFinishing()) {
            alertDialog.show();
        }
    }


    public void initIABSetup() {

        mHelper = new IabHelper(this, String.valueOf(PurchaseItem.base64EncodedPublicKey));
        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            @Override
            public void onIabSetupFinished(IabResult result) {

                if (!result.isSuccess()) {

                    setup_successed = false;
                    return;
                }
                setup_successed = true;
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {

        @Override
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {

            if (result.isFailure() || !setup_successed) {
                return;
            }
            List<Purchase> purchases = inventory.getAllPurchases();// new
            if (purchases != null && purchases.size() > 0) {
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.SKU_ITEM_00))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.SKU_ITEM_00)));
                    if (!mSharePreferencePermanentpurchasefriend.isPuchaseItem35()) {
                        mSharePreferencePermanentpurchasefriend.setPuchaseItem35(true);
                        mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
                        mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);
                    }
                    SHOWUSERS = 34;
                    isBoughtItem1 = true;
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.SKU_ITEM_11))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.SKU_ITEM_11)));
                    if (!mSharePreferencePermanentpurchasefriend.isPuchaseItem50()) {

                        mSharePreferencePermanentpurchasefriend.setPuchaseItem50(true);
                        mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
                        mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);

                    }
                    isBoughtItem2 = true;
                    SHOWUSERS = 49;
                }

                if (inventory.hasPurchase(String.valueOf(PurchaseItem.SKU_ITEM_12))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.SKU_ITEM_12)));
                    if (!mSharePreferencePermanentpurchasefriend.isPuchaseItem100()) {

                        mSharePreferencePermanentpurchasefriend.setPuchaseItem100(true);
                        mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
                        mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);

                    }
                    isBoughtItem3 = true;
                    SHOWUSERS = 99;
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.SKU_ITEM_13))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.SKU_ITEM_13)));
                    if (!mSharePreferencePermanentpurchasefriend.isPuchaseItem200()) {

                        mSharePreferencePermanentpurchasefriend.setPuchaseItem200(true);
                        mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
                        mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);

                    }
                    isBoughtItem3 = true;
                    SHOWUSERS = 199;
                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD)));
                    mSharePreferencePermanentadd.setAds(true);
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mAdView.setVisibility(View.GONE);
                        }
                    });
                }


                if (inventory.hasPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_15))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_15)));
                    mSharePreferencePermanentpurchasestranger.setPuchasePeople(true);
                    isBoughtPeople = true;
                    mSharePreference.setSharefbPeople();
                    mSharePreference.setRatePeople();

                }

                if (inventory.hasPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_25))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_25)));
                    mSharePreferencePermanentpurchasestranger.setPuchasePeople(true);
                    mSharePreferencePermanentpurchasestranger.setPuchasePeople25(true);
                    isBoughtPeople = true;
                    mSharePreference.setSharefbPeople();
                    mSharePreference.setRatePeople();

                }
                if (inventory.hasPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_ALL))) {
                    purchases.add(inventory.getPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_ALL)));
                    mSharePreferencePermanentpurchasestranger.setPuchasePeople(true);
                    mSharePreferencePermanentpurchasestranger.setPuchasePeople25(true);
                    mSharePreferencePermanentpurchasestranger.setPuchasePeople50(true);
                    isBoughtPeople = true;
                    mSharePreference.setSharefbPeople();
                    mSharePreference.setRatePeople();

                }

            }
            if (purchases.size() > 0) {
                isInAppBilling = true;
            }
        }
    };
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {

            if (mHelper == null)
                return;

            if (result.isFailure()) {
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.purchase_auth_failed));

                return;
            }
            if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_00))) {
                isBoughtItem1 = true;
                mSharePreferencePermanentpurchasefriend.setPuchaseItem35(true);
                mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
                mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        SHOWUSERS = 34;
                        int count = 0;
                        if (mViewers.size() > 35) {
                            count = 35;
                        } else {
                            count = mViewers.size();
                        }

                        for (int x = 0; x < count; x++) {
                            mViewers.get(x).setShow(true);
                        }

                        tinydb.putListObject(STORELIST, mViewers);

                        if (mViewers.size() > 35) {
                            mDataAdapter.notifyDataSetChanged();
                            mRecyclerView.setVisibility(View.VISIBLE);
                        } else
                            Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.restart_app));


                    }
                });
                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.thankyou_message));
            } else if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_11))) {
                mSharePreferencePermanentpurchasefriend.setPuchaseItem50(true);
                mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
                mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);


                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SHOWUSERS = 49;

                        int count = 0;
                        if (mViewers.size() > 50) {
                            count = 50;
                        } else {
                            count = mViewers.size();
                        }

                        for (int x = 0; x < count; x++) {
                            mViewers.get(x).setShow(true);
                        }
                        tinydb.putListObject(STORELIST, mViewers);
                        if (mViewers.size() > 35) {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mDataAdapter.notifyDataSetChanged();
                        } else
                            Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.restart_app));

                    }
                });

                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.thankyou_message));
            } else if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_12))) {
                mSharePreferencePermanentpurchasefriend.setPuchaseItem100(true);
                mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
                mSharePreferencePermanentRate.setRate(mSharePreference.getmStringUserName(),true);

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SHOWUSERS = 99;

                        int count = 0;
                        if(mViewers.size()>100)
                            count=100;
                        else
                            count = mViewers.size();

                        for (int x = 0; x < count / 2; x++) {
                            mViewers.get(x).setShow(true);
                        }
                        tinydb.putListObject(STORELIST, mViewers);
                        if (mViewers.size() > 50) {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mDataAdapter.notifyDataSetChanged();
                        } else
                            Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.restart_app));


                    }
                });

                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.thankyou_message));
            } else if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_13))) {

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SHOWUSERS = 99;

                        int count = mViewers.size();

                        for (int x = 0; x < count / 2; x++) {
                            mViewers.get(x).setShow(true);
                        }
                        tinydb.putListObject(STORELIST, mViewers);
                        if (mViewers.size() > 100) {
                            mRecyclerView.setVisibility(View.VISIBLE);
                            mDataAdapter.notifyDataSetChanged();
                        } else
                            Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.restart_app));


                    }
                });
            } else if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD))) {
                mSharePreferencePermanentadd.setAds(true);

                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mAdView.setVisibility(View.GONE);
                    }
                });

                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.thankyou_message));
            } else if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_15))) {

                isBoughtPeople = true;
                mSharePreferencePermanentpurchasestranger.setPuchasePeople(true);
                mSharePreference.setSharefbPeople();
                mSharePreference.setRatePeople();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        int count = 0;
                        if (mViewersPeople.size() > 15)
                            count = 15;
                        else
                            count = mViewersPeople.size();

                        for (int j = 0; j < count; j++) {
                            mViewersPeople.get(j).setShow(true);
                        }
                        tinydb.putListObject(STORELIST_STRANGER, mViewersPeople);

                        mDataAdapterStrangers.notifyDataSetChanged();

                    }
                });
                //
                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.thankyou_message));
            } else if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_25))) {

                isBoughtPeople = true;
                mSharePreferencePermanentpurchasestranger.setPuchasePeople(true);
                mSharePreferencePermanentpurchasestranger.setPuchasePeople25(true);
                mSharePreference.setSharefbPeople();
                mSharePreference.setRatePeople();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        int count = 0;
                        if (mViewersPeople.size() > 24)
                            count = 25;
                        else
                            count = mViewersPeople.size();

                        for (int j = 0; j < count; j++) {
                            mViewersPeople.get(j).setShow(true);
                        }
                        tinydb.putListObject(STORELIST_STRANGER, mViewersPeople);
                        mDataAdapterStrangers.notifyDataSetChanged();

                    }
                });
                //
                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.thankyou_message));
            } else if (purchase.getSku().equals(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_ALL))) {

                isBoughtPeople = true;
                mSharePreferencePermanentpurchasestranger.setPuchasePeople(true);
                mSharePreferencePermanentpurchasestranger.setPuchasePeople25(true);
                mSharePreferencePermanentpurchasestranger.setPuchasePeople50(true);
                mSharePreference.setSharefbPeople();
                mSharePreference.setRatePeople();
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int j = 0; j < (mViewersPeople.size() ); j++) {
                            mViewersPeople.get(j).setShow(true);
                        }
                        tinydb.putListObject(STORELIST_STRANGER, mViewersPeople);
                        mDataAdapterStrangers.notifyDataSetChanged();
                    }
                });
                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.thankyou_message));
            } else {
            }

        }
    };

    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

		/*
         * TODO: verify that the developer payload of the purchase is correct.
		 * It will be the same one that you sent when initiating the purchase.
		 *
		 * WARNING: Locally generating a random string when starting a purchase
		 * and verifying it here might seem like a good approach, but this will
		 * fail in the case where the user purchases an item on one device and
		 * then uses your app on a different device, because on the other device
		 * you will not have access to the random string you originally
		 * generated.
		 *
		 * So a good developer payload has these characteristics:
		 *
		 * 1. If two different users purchase an item, the payload is different
		 * between them, so that one user's purchase can't be replayed to
		 * another user.
		 *
		 * 2. The payload must be such that you can verify it even when the app
		 * wasn't the one who initiated the purchase flow (so that items
		 * purchased by the user on one device work on other devices owned by
		 * the user).
		 *
		 * Using your own server to store and verify developer payloads across
		 * app installations is recommended.
		 */

        return true;
    }


    public void logout(String message) {
        AlertDialog mAlertDialog = new AlertDialog.Builder(MainActivity.this).create();
        mAlertDialog.setMessage(message);

        mAlertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Editor prefEdit = sharedPreferences.edit();
                prefEdit.clear();
                mSharePreference.clearData();
                tinydb.clear();
                finish();
                Intent mIntent = new Intent(MainActivity.this, LoginPagerScreen.class);
                startActivity(mIntent);
            }
        });
        if(!isDestroy)
            mAlertDialog.show();
    }

    @Override
    public void onBackPressed() {

        boolean isconnected = isNetworkAvailable(getApplicationContext());


        if (mWebViewFacebookStrangers.getVisibility() == View.VISIBLE) {
            mWebViewFacebookStrangers.setVisibility(View.GONE);
        } else {

            if (isconnected) {
                if (progressFriend.getVisibility() != View.VISIBLE) {
                    if (sp1.getString("rate", "").contentEquals("")) {
                        showdialogback();
                    } else
                        super.onBackPressed();
                }
            } else {
                finish();
            }
        }
        if (sp1.getString("rate", "").contentEquals("")) {
            showdialogback();
        } else
            super.onBackPressed();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            mHelper.queryInventoryAsync(mGotInventoryListener);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        isOpenScreen = true;
        isfirstclick = false;
        activityDestroyed = false;
        if (mSharePreferencePermanentadd.isAds()) {
            activityDestroyed = true;
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        isOpenScreen = false;
    }

    public void dialog_purchase() {
        final Dialog mDialog = new Dialog(MainActivity.this);

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_purchase);

        final ImageView mImageView_Close = (ImageView) mDialog.findViewById(R.id.purchase_dialog_close);
        final LinearLayout mLinearLayoutShare = (LinearLayout) mDialog.findViewById(R.id.layout_share);
        final LinearLayout mLinearLayoutRate = (LinearLayout) mDialog.findViewById(R.id.layout_rate);
        final LinearLayout mLinearLayout35 = (LinearLayout) mDialog.findViewById(R.id.layout_35);
        final LinearLayout mLinearLayout50 = (LinearLayout) mDialog.findViewById(R.id.layout_50);
        final LinearLayout mLinearLayout100 = (LinearLayout) mDialog.findViewById(R.id.layout_100);
        final LinearLayout mLinearLayout200 = (LinearLayout) mDialog.findViewById(R.id.layout_200);
        final LinearLayout mLinearLayoutAds = (LinearLayout) mDialog.findViewById(R.id.layout_ads);

        final View view_share = (View) mDialog.findViewById(R.id.view_share);
        final View view_rate = (View) mDialog.findViewById(R.id.view_rate);
        final View view_50 = (View) mDialog.findViewById(R.id.view_100);
        final View view_100 = (View) mDialog.findViewById(R.id.view_200);
        final View view_ads = (View) mDialog.findViewById(R.id.view_ads);

        TextView mTextViewShare = (TextView)mDialog.findViewById(R.id.text_share);
        TextView mTextViewRate = (TextView)mDialog.findViewById(R.id.text_rate);
        TextView mTextView35 = (TextView)mDialog.findViewById(R.id.text_35);
        TextView mTextView50 = (TextView)mDialog.findViewById(R.id.text_50);
        TextView mTextView100 = (TextView)mDialog.findViewById(R.id.text_100);
        TextView mTextView200 = (TextView)mDialog.findViewById(R.id.text_200);

        mTextViewShare.setText(getPurchaseText(R.string.purchase_dialog_5_user,9));
        mTextViewRate.setText(getPurchaseText(R.string.purchase_dialog_rate,12));
        mTextView35.setText(getPurchaseText(R.string.purchase_dialog_35_users,35));
        mTextView50.setText(getPurchaseText(R.string.purchase_dialog_50_users,50));
        mTextView100.setText(getPurchaseText(R.string.purchase_dialog_100_users,100));
        mTextView200.setText(getPurchaseText(R.string.purchase_dialog_200_users,200));

        if(mViewers.size()>0 && mViewers.size() <= 6){
            mLinearLayout35.setVisibility(View.GONE);
            mLinearLayout50.setVisibility(View.GONE);
            mLinearLayout100.setVisibility(View.GONE);
            mLinearLayout200.setVisibility(View.GONE);
            mLinearLayoutRate.setVisibility(View.GONE);
            mLinearLayoutShare.setVisibility(View.GONE);
            view_100.setVisibility(View.GONE);
            view_50.setVisibility(View.GONE);
            view_rate.setVisibility(View.GONE);
            view_share.setVisibility(View.GONE);
            view_ads.setVisibility(View.GONE);
        }

        if(mViewers.size()>6 && mViewers.size()<=9){
            mLinearLayout35.setVisibility(View.GONE);
            mLinearLayout50.setVisibility(View.GONE);
            mLinearLayout100.setVisibility(View.GONE);
            mLinearLayout200.setVisibility(View.GONE);
            mLinearLayoutRate.setVisibility(View.GONE);
            view_100.setVisibility(View.GONE);
            view_50.setVisibility(View.GONE);
            view_rate.setVisibility(View.GONE);
            view_share.setVisibility(View.GONE);
            view_ads.setVisibility(View.GONE);
        }

        if(mViewers.size()>=10 && mViewers.size()<=12){
            mLinearLayout35.setVisibility(View.GONE);
            mLinearLayout50.setVisibility(View.GONE);
            mLinearLayout100.setVisibility(View.GONE);
            mLinearLayout200.setVisibility(View.GONE);
            view_100.setVisibility(View.GONE);
            view_share.setVisibility(View.GONE);
            view_50.setVisibility(View.GONE);
            view_ads.setVisibility(View.GONE);
            view_rate.setVisibility(View.GONE);
        }

        if(mViewers.size()>=13 && mViewers.size()<=34){

            mLinearLayout50.setVisibility(View.GONE);
            mLinearLayout100.setVisibility(View.GONE);
            mLinearLayout200.setVisibility(View.GONE);
            view_100.setVisibility(View.GONE);
            view_50.setVisibility(View.GONE);
            view_ads.setVisibility(View.GONE);
        }

        if(mViewers.size()>=35 && mViewers.size()<=49){
            mLinearLayout100.setVisibility(View.GONE);
            mLinearLayout200.setVisibility(View.GONE);
            view_100.setVisibility(View.GONE);
            view_50.setVisibility(View.GONE);
            view_rate.setVisibility(View.GONE);
            view_share.setVisibility(View.GONE);
            view_ads.setVisibility(View.GONE);
        }

        if(mViewers.size()>=50 && mViewers.size()<=99){
            mLinearLayout200.setVisibility(View.GONE);
            view_ads.setVisibility(View.GONE);
        }

        if (mSharePreferencePermanentShare.isShareonFb(mSharePreference.getmStringUserName())) {
            mLinearLayoutShare.setVisibility(View.GONE);
        }

        if (mSharePreferencePermanentRate.isRate(mSharePreference.getmStringUserName())) {
            mLinearLayoutRate.setVisibility(View.GONE);
        }

        if (isBoughtItem1) {

            mLinearLayout35.setVisibility(View.GONE);
        }

        if (isBoughtItem2) {

            mLinearLayout35.setVisibility(View.GONE);
            mLinearLayout50.setVisibility(View.GONE);
        }

        if (isBoughtItem3) {

            mLinearLayout35.setVisibility(View.GONE);
            mLinearLayout50.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentadd.isAds()) {
            mLinearLayoutAds.setVisibility(View.GONE);
        }
        mImageView_Close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mLinearLayoutShare.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                shareimagefb();
                mDialog.dismiss();
            }
        });

        mLinearLayoutRate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mSharePreferencePermanentShare.isShareonFb(mSharePreference.getmStringUserName())) {
                    clickShare = false;

                    AsyncRequest getPosts =
                            new AsyncRequest(MainActivity.this, AsyncRequest.POSTURLCONNECTION,RATINGLABEL);
                    getPosts.execute(register() + mSharePreference.getmStringUserEmail() + "&face_id=" + mSharePreference.getmStringUserId() + "&gcm_id=" + mSharePreference.getToken());
                    mDialog.dismiss();
                } else {
                    showAlert(getResources().getString(R.string.profile_stalker), getResources().getString(R.string.share_fb));
                }

            }
        });

        mLinearLayout35.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_00),isBoughtItem1);
                mDialog.dismiss();
            }
        });

        mLinearLayout50.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_11),isBoughtItem2);

                mDialog.dismiss();
            }
        });

        mLinearLayout100.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_12),isBoughtItem3);
                mDialog.dismiss();
            }
        });

        mLinearLayout200.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_13),mSharePreferencePermanentpurchasefriend.isPuchaseItem200());
                mDialog.dismiss();
            }
        });
        mLinearLayoutAds.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                  inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_REMOVE_AD),mSharePreferencePermanentadd.isAds());
                mDialog.dismiss();
            }
        });


        mDialog.show();

    }

    public String getPurchaseText(int res, int size)
    {
        String text = "";

        if(mViewers.size()<size)
            text = String.format(getResources().getString(res), mViewers.size());
        else
            text = String.format(getResources().getString(res), size);

        return text;
    }

    public void updateRating() {
        final String appName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_URL + appName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(PLAY_STORE + appName)));

        }
        if (!isBoughtItem1 && !isBoughtItem2 && !isBoughtItem3) {

            SHOWUSERS = 12;
            int count = 0;

            if (mViewers.size() > 12)
                count = 12;

            for (int i = 0; i < count; i++) {
                mViewers.get(i).setShow(true);
            }
            mDataAdapter.notifyDataSetChanged();
        }
    }

    public void shareimagefb() {

        if(ShareDialog.canShow(ShareLinkContent.class)){
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(PLAY_STORE + getApplicationContext().getPackageName())).build();
            shareDialog.show(linkContent);  // Show facebook ShareDialog
        }
    }

    public void dialog_purchase_stranger() {
        final Dialog mDialog = new Dialog(MainActivity.this);

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_strangers);
        final ImageView mImageView_Close = (ImageView) mDialog.findViewById(R.id.purchase_dialog_strangers_close);
        final TextView mTextView35 = (TextView) mDialog.findViewById(R.id.text_35);
        final TextView mTextView50 = (TextView) mDialog.findViewById(R.id.text_50);
        final TextView mTextView100 = (TextView) mDialog.findViewById(R.id.text_100);

        final LinearLayout mLinearLayout35 = (LinearLayout) mDialog.findViewById(R.id.layout_35);
        final LinearLayout mLinearLayout50 = (LinearLayout) mDialog.findViewById(R.id.layout_50);
        final LinearLayout mLinearLayout100 = (LinearLayout) mDialog.findViewById(R.id.layout_100);

        mTextView35.setText(getResources().getString(R.string.purchase_dialog_15_users));

        if (mViewersPeople.size() <= 25) {
            mTextView50.setText(mViewersPeople.size() + " users - 2.99$");
        } else {
            mTextView50.setText(getResources().getString(R.string.purchase_dialog_25_users));
        }

        mTextView100.setText(mViewersPeople.size() + " users - 4.99$");

        if (mViewersPeople.size() < 50) {
            mTextView100.setVisibility(View.GONE);
            mLinearLayout100.setVisibility(View.GONE);
        }
        if (mSharePreferencePermanentpurchasestranger.isPuchasePeople()) {
            mTextView35.setVisibility(View.GONE);
        }

        if (mSharePreferencePermanentpurchasestranger.isPuchasePeople25()) {
            mTextView35.setVisibility(View.GONE);
            mTextView50.setVisibility(View.GONE);
        }


        mImageView_Close.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mLinearLayout35.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_15),mSharePreferencePermanentpurchasestranger.isPuchasePeople());
                mDialog.dismiss();
            }
        });

        mLinearLayout50.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_25),mSharePreferencePermanentpurchasestranger.isPuchasePeople25());
                mDialog.dismiss();
            }
        });

        mLinearLayout100.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_ALL),mSharePreferencePermanentpurchasestranger.isPuchasePeople50());
                mDialog.dismiss();
            }
        });


        if (mViewersPeople.size() > 15)
            mDialog.show();
        else {

            inAppPurchase(String.valueOf(PurchaseItem.SKU_ITEM_PEOPLE_15),mSharePreferencePermanentpurchasestranger.isPuchasePeople());
        }

    }

    public void setNotification() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent("android.media.action.DISPLAY_NOTIFICATION");
        notificationIntent.addCategory("android.intent.category.DEFAULT");

        PendingIntent broadcast = PendingIntent.getBroadcast(this, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
    }

    public void ShowDialogPop() {

        mDialogPopup = new Dialog(MainActivity.this, R.style.PauseDialog);
        mDialogPopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialogPopup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialogPopup.setCanceledOnTouchOutside(false);
        mDialogPopup.setCancelable(false);
        mDialogPopup.setContentView(R.layout.dialog_welcome);

        ImageView mImageViewCancel = (ImageView) mDialogPopup.findViewById(R.id.done);
        TextView txtuserName = (TextView) mDialogPopup.findViewById(R.id.txtUserName);
        ImageView mImagetick = (ImageView) mDialogPopup.findViewById(R.id.image);
        txtuserName.setText(mSharePreference.getmStringUserName());
        Glide.with(MainActivity.this)
                .load("http://graph.facebook.com/" + mSharePreference.getmStringUserId() + "/picture?type=large")
                .placeholder(R.drawable.icon)
                .into(mImagetick);
        mImageViewCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                mDialogPopup.dismiss();


            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void checkInternetConnection() {

        if (br == null) {

            br = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    Bundle extras = intent.getExtras();

                    NetworkInfo info = (NetworkInfo) extras.getParcelable("networkInfo");

                    State state = info.getState();
                    Log.d("TEST Internet", info.toString() + " " + state.toString());

                    if (isNetworkAvailable(getApplicationContext())) {

                        mLinearLayoutNoInternet.setVisibility(View.GONE);

                    } else {

                        mLinearLayoutNoInternet.setVisibility(View.VISIBLE);
                    }

                }
            };

            final IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
            registerReceiver((BroadcastReceiver) br, intentFilter);
        }
    }

    public void logout(Activity mActivity) {
        final Dialog mDialog = new Dialog(mActivity, R.style.PauseDialog);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        mDialog.setContentView(R.layout.dialog_logout);
        ImageView imageViewok = (ImageView) mDialog.findViewById(R.id.img_ok_logout);
        ImageView imageViewcancel= (ImageView) mDialog.findViewById(R.id.img_ok_cancel);
        imageViewok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                activityDestroyed = true;
                tinydb.clear();
                callFacebookLogout(MainActivity.this);
                if(mSharePreference != null)
                {
                    mSharePreference.clearData();
                }

                if(mDrawerLayout != null){
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                }
                mDialog.dismiss();
            }
        });
        imageViewcancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
            }
        });
        mDialog.show();
    }

    public void showdialogback() {
        final Dialog mDialog = new Dialog(MainActivity.this, R.style.PauseDialog);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.dialog_exit);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        TextView mButtonLoveit, mButtonNeedsWork, mButtonLater;
        mButtonLater = (TextView) mDialog.findViewById(R.id.dialog_later);
        mButtonLoveit = (TextView) mDialog.findViewById(R.id.dialog_love);
        mButtonNeedsWork = (TextView) mDialog.findViewById(R.id.dialog_work);
        mButtonLoveit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Editor editor = sp1.edit();
                editor.putString("rate", "true");
                editor.commit();

                final String appName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_URL + appName)));

                } catch (android.content.ActivityNotFoundException andfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(PLAY_STORE + appName)));
                }
            }
        });
        mButtonNeedsWork.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Editor editor = sp1.edit();

                Intent intent1 = new Intent(Intent.ACTION_SEND);
                intent1.setData(Uri.parse("mailto:"));
                intent1.putExtra(Intent.EXTRA_EMAIL, new String[]{email_id});
                intent1.putExtra(Intent.EXTRA_SUBJECT, improvments);
                intent1.putExtra(Intent.EXTRA_TEXT, "hello");
                intent1.setType("message/rfc822");
                startActivity(Intent.createChooser(intent1, EMail));
                editor.putString(rate, TRUE );
                editor.commit();
            }

        });
        mButtonLater.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               if(mDialog != null)
                mDialog.dismiss();
               else{
               }
                finishAffinity();
            }
        });
        mDialog.show();
    }

    public void callFacebookLogout(Context context) {

        GraphRequest.Callback callback = new GraphRequest.Callback() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onCompleted(GraphResponse response) {

                try {

                    if(TAGS.mStalkerUsers!=null)
                        TAGS.mStalkerUsers.clear();
                    if(TAGS.mViewersMainScreen!=null)
                        TAGS.mViewersMainScreen.clear();
                    if(TAGS.mViewersNew!=null)
                        TAGS.mViewersNew.clear();

                    if (response.getError() != null) {
                        if(mSharePreference != null){
                            mSharePreference.clearData();
                        }
                        if(mViewers!=null)
                            mViewers.clear();

                        Intent mIntent = new Intent(MainActivity.this, LoginPagerScreen.class);
                        startActivity(mIntent);
                        finishAffinity();
                    } else if (response !=null && response.getJSONObject().getBoolean(SUCCESS)) {
                        LoginManager.getInstance().logOut();
                        if(mSharePreference != null){
                            mSharePreference.clearData();
                        }
                        if(mViewers!=null)
                            mViewers.clear();

                        Intent mIntent = new Intent(MainActivity.this, LoginPagerScreen.class);
                        startActivity(mIntent);
                        finishAffinity();

                    }
                } catch (JSONException ex) { /* no op */ }
                TAGS.userList = true;
            }
        };
        GraphRequest request = new GraphRequest(AccessToken.getCurrentAccessToken(),
                GRAPH_PATH, new Bundle(), HttpMethod.DELETE, callback);
        request.executeAsync();


    }

    public boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    private FacebookCallback<Sharer.Result> callback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result result) {
            mSharePreference.setSharefbPeople();
            mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
            mSharePreferencePermanentShare.setSharefb(mSharePreference.getmStringUserName(),true);
            SHOWUSERS = 9;
            int count = 0;

            if (mViewers.size() > 9)
                count = 9;

            for (int i = 0; i < count; i++) {
                mViewers.get(i).setShow(true);
            }

            mDataAdapterStrangers.notifyDataSetChanged();
            mDataAdapter.notifyDataSetChanged();

            // Write some code to do some operations when you shared content successfully.
        }

        @Override
        public void onCancel() {
            // Write some code to do some operations when you cancel sharing content.
        }

        @Override
        public void onError(FacebookException error) {
            // Write some code to do some operations when some error occurs while sharing content.
        }
    };


    public void inAppPurchase(String purchaseItem, boolean isBought) {
        if (setup_successed) {
            if (!isBought) {
                if (mHelper != null) {
                    mHelper.launchPurchaseFlow(MainActivity.this, purchaseItem, RC_REQUEST,
                            mPurchaseFinishedListener, payload);
                } else {
                    Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.purchase_error));
                }
            } else {
                Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.purchase_done_already));
            }
        } else {
            Utils.SetDiolog(MainActivity.this, getResources().getString(R.string.purchase_can_not));

        }
    }
}


// 23301268145

# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/wscn/Documents/android-sdk-macosx/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

#-keep  class MyJavaScriptInterface
#-keep  class Callback
#-keepclassmembers class com.check_who_visitedmyprofile.LoginRegisterActivity1$MyJavaScriptInterface {
#public *;
#     }
#-keepclassmembers class com.check_who_visitedmyprofile.LoginRegisterActivity1$Callback {
# public *;
# }

-keepclassmembers class com.profile.stalkers.MainActivity$MyJavaScriptInterface {
public *;
     }
-keepclassmembers class com.profile.stalkers.MainActivity$Callback {
 public *;
 }
-keepclassmembers class com.profile.stalkers.MainActivity$MyJavaScriptInterfaceFriend {
public *;
     }
-keepclassmembers class com.profile.stalkers.MainActivity$CallbackFriend {
 public *;
 }
# UploadFeed
-renamesourcefileattribute SourceFile
-keepattributes SourceFile,LineNumberTable

-keepattributes MyJavaScriptInterface
-keepattributes MyJavaScriptInterfaceFriend

-keepattributes Signature

# For using GSON @Expose annotation

-keepattributes EnclosingMethod
-keepattributes *Annotation*
# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.** { *; }



# Application classes that will be serialized/deserialized over Gson
-keep class com.profile.stalkers.model.** { *; }
-dontwarn org.apache.commons.**
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**


